import 'dart:async';
import 'dart:convert';
import 'package:wokee_platform/view/model/model.dart';
import 'package:http/http.dart' as http;
 
var valueDataStatus = "";

var valueStatusCode = "";
var valueStatusDesc = "";
var valueEktp = "";
var valueNamaLengkap = "";
var valueAgama = "";
var valuePekerjaan = "";
var valuePendidikan = "";
var valueTempatLahir = "";
var valueTanggalLahir = "";
var valueStatusKawin = "";
var valueGolonganDarah = "";
var valueJenisKelamin = "";
var valueNomorProvinsi = "";
var valueNamaProvinsi = "";
var valueNomorKabupatan = "";
var valueNamaKabupaten = "";
var valueNomorKecamatan = "";
var valueNamaKecamatan = "";
var valueNomorKelurahan = "";
var valueNamaKelurahan = "";
var valueDusun = "";
var valueAlamat = "";
var valueRt = "";
var valueRw = "";
var valueNomorKartuKeluarga = "";
var valueNamaAyah = "";
var valueNamaIbu = "";

Future dukcapilApi() async {
  print("-->User Input (Service)<--");
  print("NIK: ${txtControllerEktp.text}");

  var url = "http://10.2.62.35:8080/wokee/okoce/ektp";
  print("URL: $url");

  Map dataBody = {
    "EKTP": "${txtControllerEktp.text}",
  };

  var body = json.encode(dataBody);

  final response = await http.post(url,
      // final response = await ioClient.post(url,
      headers: {"Content-Type": "application/json"},
      body: body);

  final int statusCode = response.statusCode;

  if (statusCode == 200) {
    print("Connection Success!");
    print("Status Code: $statusCode");
    var result = json.decode(response.body);
    var result1 = result['Status']['statusCode'];
    var result2 = result['Status']['statusDesc'];
    var result3 = result['Data']['ektp'];
    var result4 = result['Data']['namaLengkap'];
    var result5 = result['Data']['agama'];
    var result6 = result['Data']['pekerjaan'];
    var result7 = result['Data']['pendidikan'];
    var result8 = result['Data']['tempatLahir'];
    var result9 = result['Data']['tglLahir'];
    var result10 = result['Data']['statusKawin'];
    var result11 = result['Data']['golDarah'];
    var result12 = result['Data']['jenisKelamin'];
    var result13 = result['Data']['propinsiNo'];
    var result14 = result['Data']['propinsiNama'];
    var result15 = result['Data']['kabNo'];
    var result16 = result['Data']['kabNama'];
    var result17 = result['Data']['kecNo'];
    var result18 = result['Data']['kecNama'];
    var result19 = result['Data']['kelNo'];
    var result20 = result['Data']['kelNama'];
    var result21 = result['Data']['dusun'];
    var result22 = result['Data']['alamat'];
    var result23 = result['Data']['rtNo'];
    var result24 = result['Data']['rwNo'];
    var result25 = result['Data']['noKK'];
    var result26 = result['Data']['namaAyah'];
    var result27 = result['Data']['namaIbu'];

    // print("Status Code: $result1");
    // print("Status Desc: $result2");
    // print("EKTP: $result3");
    // print("Nama Lengkap: $result4");
    // print("Agama: $result5");
    // print("Pekerjaan: $result6");
    // print("Pendidikan: $result7");
    // print("Tempat Lahir: $result8");
    // print("Tanggal Lahir: $result9");
    // print("Status Kawin: $result10");
    // print("Golongan Darah: $result11");
    // print("Jenis Kelamin: $result12");
    // print("Nomor Provinsi: $result13");
    // print("Nama Provinsi: $result14");
    // print("Nomor Kabupaten: $result15");
    // print("Nama Kabupaten: $result16");
    // print("Nomor Kecamatan: $result17");
    // print("Nama Kecamatan: $result18");
    // print("Nomor Kelurahan: $result19");
    // print("Nama Kelurahan: $result20");
    // print("Dusun: $result21");
    // print("Alamat: $result22");
    // print("Nomor RT: $result23");
    // print("Nama RW: $result24");
    // print("Nomor Kartu Keluarga: $result25");
    // print("Nama Ayah: $result26");
    // print("Nama Ibu: $result27");

    valueStatusCode = result1.toString();
    valueStatusDesc = result2.toString();

    valueEktp = result3.toString();
    valueNamaLengkap = result4.toString();
    valueAgama = result5.toString();
    valuePekerjaan = result6.toString();
    valuePendidikan = result7.toString();
    valueTempatLahir = result8.toString();
    valueTanggalLahir = result9.toString();
    valueStatusKawin = result10.toString();
    valueGolonganDarah = result11.toString();
    valueJenisKelamin = result12.toString();
    valueNomorProvinsi = result13.toString();
    valueNamaProvinsi = result14.toString();
    valueNomorKabupatan = result15.toString();
    valueNamaKabupaten = result16.toString();
    valueNomorKecamatan = result17.toString();
    valueNamaKecamatan = result18.toString();
    valueNomorKelurahan = result19.toString();
    valueNamaKelurahan = result20.toString();
    valueDusun = result21.toString();
    valueAlamat = result22.toString();
    valueRt = result23.toString();
    valueRw = result24.toString();
    valueNomorKartuKeluarga = result25.toString();
    valueNamaAyah = result26.toString();
    valueNamaIbu = result27.toString();

    return valueStatusCode +
        valueStatusDesc +
        valueEktp +
        valueNamaLengkap +
        valueAgama +
        valuePekerjaan +
        valuePendidikan +
        valueTempatLahir +
        valueTanggalLahir +
        valueStatusKawin +
        valueGolonganDarah +
        valueJenisKelamin +
        valueNomorProvinsi +
        valueNamaProvinsi +
        valueNomorKabupatan +
        valueNamaKabupaten +
        valueNomorKecamatan +
        valueNamaKecamatan +
        valueNomorKelurahan +
        valueNamaKelurahan +
        valueDusun +
        valueAlamat +
        valueRt +
        valueRw +
        valueNomorKartuKeluarga +
        valueNamaAyah +
        valueNamaIbu;
  } else {
    print("Connection Failed!");
    print("Status Code Failed: $statusCode");
    var result = json.decode(response.body);
    var failed = result['Data']['status'];
    valueDataStatus = failed.toString();
    return valueDataStatus;
    // throw new Exception("Error while fetching data");
  }
}
