import 'dart:async';
import 'dart:convert';
import 'package:wokee_platform/view/model/model.dart';
import 'package:flutter/services.dart';
import 'dart:io';
import 'package:http/io_client.dart';
import 'package:wokee_platform/service/generate_signature.dart';
  
var valueCode = "";
var valueMessage = "";

Future registerUser() async{
    String apiKey = await rootBundle.loadString('assets/url/apikey.txt');
    String apiSecret = await rootBundle.loadString('assets/url/apisecret.txt');
    var apiKeyTarget = "$apiKey";
    var apiSecretTarget = "$apiSecret";

    BKPSignature  signatureRegCentagate = BKPSignature ();
    // signatureRegCentagate.getSignature("POST", "{\"firstName\":\"${txtControllerNamaDepan.text}\",\"lastName\":\"${txtControllerNamaBelakang.text}\",\"userName\":\"${txtControllerEmail.text}\",\"userApp\":\"wokee\",\"userUniqid\":\"${txtControllerEmail.text}\",\"userClientid\":\"${txtControllerEmail.text}\",\"userEmail\":\"${txtControllerEmail.text}\",\"mobileNum\":\"${txtControllerCodeCountry.text+txtCOntrollerMobileNumber.text}\"}", "$apiKeyTarget", "$tempTimestamp", "$apiSecretTarget");
    // signatureRegCentagate.getSignature("POST", "{\"firstName\":\"${txtControllerNamaDepan.text}\",\"lastName\":\"${txtControllerNamaBelakang.text}\",\"userName\":\"${txtControllerEmail.text}\",\"userApp\":\"wokee\",\"userUniqid\":\"${txtControllerEmail.text}\",\"userClientid\":\"${txtControllerEmail.text}\",\"userEmail\":\"${txtControllerEmail.text}\",\"mobileNum\":\"${txtControllerNomorHandphone.text}\"}", "$apiKeyTarget", "$tempTimestamp", "$apiSecretTarget");
    // signatureRegCentagate.getSignature("POST", "{\"firstName\":\"${txtControllerNamaDepan.text}\",\"lastName\":\"${txtControllerNamaBelakang.text}\",\"userName\":\"${txtControllerEmail.text}\",\"userApp\":\"wokee\",\"userUniqid\":\"${txtControllerEmail.text}\",\"userClientid\":\"${txtControllerEmail.text}\",\"userEmail\":\"${txtControllerEmail.text}\",\"mobileNum\":\"${txtControllerNomorHandphone.text}\"}", "45fd483f-a4ac-4844-badc-7fbb43c0bd70", "$tempTimestamp", "108acb54-ce2d-476a-9b1e-dfbec548cddd");
    signatureRegCentagate.getSignature("POST", "{\"firstName\":\"${txtControllerNamaDepan.text}\",\"lastName\":\"${txtControllerNamaBelakang.text}\",\"userName\":\"${txtControllerEmail.text}\",\"userApp\":\"wokee\",\"userUniqid\":\"${txtControllerEmail.text}\",\"userClientid\":\"${txtControllerEmail.text}\",\"userEmail\":\"${txtControllerEmail.text}\",\"mobileNum\":\"${txtControllerCodeCountry.text + txtControllerMobileNumber.text}\"}", "$apiKeyTarget", "$tempTimestamp", "$apiSecretTarget");

    print("-->User Input Registration (Centagate)<--");
    print("First Name: ${txtControllerNamaDepan.text}");
    print("Last Name: ${txtControllerNamaBelakang.text}");
    print("Email: ${txtControllerEmail.text}");
    // print("Mobile Number: ${txtControllerCodeCountry.text+txtCOntrollerMobileNumber.text}");
    // print("Mobile Number: ${txtControllerNomorHandphone.text}");
    print("Mobile Number: ${txtControllerCodeCountry.text + txtControllerMobileNumber.text}");
    
    // print("Time Stamp: $tempTimestamp");
    // print("BKP-Signature: $valueBKPSignature");

    String prefixUrl = await rootBundle.loadString('assets/url/service.txt');
    var url = "${prefixUrl}T24/datapool/registerNewWokee";
    print("URL: $url");
    // var url ="http://10.2.62.33:8080/T24/datapool/registerNewWokee";

    Map dataBody = {
      "firstName":"${txtControllerNamaDepan.text}",
      "lastName":"${txtControllerNamaBelakang.text}",
      "userName":"${txtControllerEmail.text}",
      "userApp":"wokee",
      "userUniqid":"${txtControllerEmail.text}",
      "userClientid":"${txtControllerEmail.text}",
      "userEmail":"${txtControllerEmail.text}",
      // "mobileNum":"${txtControllerCodeCountry.text+txtCOntrollerMobileNumber.text}"
      // "mobileNum":"${txtControllerNomorHandphone.text}"
      "mobileNum":"${txtControllerCodeCountry.text + txtControllerMobileNumber.text}"
    };

    var body = json.encode(dataBody);

    bool trustSelfSigned = true;
    HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => trustSelfSigned);
    IOClient ioClient = new IOClient(httpClient);

    final response = await ioClient.post(
    url, 
    headers: {
      "Content-Type":"application/json",
      "BKP-ApiKey":"$apiKeyTarget",
      "BKP-Signature":"$valueBKPSignature",
      // "BKP-Timestamp": "2019-07-17T13:24:00Z"
      "BKP-Timestamp": "$tempTimestamp"
      },
    body: body);

    final int statusCode = response.statusCode;
    
    if (statusCode == 200) {
      print("Connection Success!");
      print("Status Code: $statusCode");
    } else {
      print("Connection Failed!");
      throw new Exception("Error while fetching data");
    }

    var result = json.decode(response.body);
    var result2 = result['Output']['Code'];
    var result3 = result['Output']['Message'];

    // print("Response Body: ${response.body}");
    // print("-->Connection<--");
    // print("Code: $result2");
    // print("Message: $result3");
    

    valueCode = result2.toString();
    valueMessage = result3.toString();
    print("Code: $valueCode" + ", " + "Message: $valueMessage");
    // return print("Code: $valueCode" + ", " + "Message: $valueMessage");
    return valueCode + valueMessage;
  }