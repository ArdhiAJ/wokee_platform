import 'dart:async';
import 'dart:convert';
import 'package:wokee_platform/view/model/model.dart';
import 'package:flutter/services.dart';
import 'dart:io';
import 'package:http/io_client.dart';
import 'package:wokee_platform/service/generate_signature.dart';

var valueCode = "";
var valueDesc = "";
var valueOTP = "";
// var tempOTP = "nyaa@gmail.com";

Future otpUserRequest() async{
    String apiKey = await rootBundle.loadString('assets/url/apikey.txt');
    String apiSecret = await rootBundle.loadString('assets/url/apisecret.txt');
    var apiKeyTarget = "$apiKey";
    var apiSecretTarget = "$apiSecret";

    BKPSignature signatureReqOTP = BKPSignature();
    signatureReqOTP.getSignature("POST", "{\"User_Name\":\"${txtControllerRequestOTPEmail.text}\",\"Sms_Otp\":\"1\"}", "$apiKeyTarget", "$tempTimestamp", "$apiSecretTarget");
    // signatureReqOTP.getSignature("POST", "{\"User_Name\":\"${txtControllerRequestOTPEmail.text}\",\"Sms_Otp\":\"1\"}", "45fd483f-a4ac-4844-badc-7fbb43c0bd70", "$tempTimestamp", "108acb54-ce2d-476a-9b1e-dfbec548cddd");

    print("-->User Request OTP<--");
    print("User Name: ${txtControllerRequestOTPEmail.text}");
    // print("OTP: $requestOTP");
    // print("BKP-Signature: $valueBKPSignature");

    String prefixUrl = await rootBundle.loadString('assets/url/service.txt');
    var url = "${prefixUrl}T24/datapool/OTPRequest";
    print("URL: $url");
    // var url ="http://10.2.62.33:8080/T24/datapool/OTPRequest";

    Map dataBody = {
      "User_Name":"${txtControllerRequestOTPEmail.text}",
      "Sms_Otp":"1"
    };

    var body = json.encode(dataBody);

    bool trustSelfSigned = true;
    HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => trustSelfSigned);
    IOClient ioClient = new IOClient(httpClient);

    final response = await ioClient.post(
    url, 
    headers: {
      "Content-Type":"application/json",
      "BKP-ApiKey":"$apiKeyTarget",
      "BKP-Signature":"$valueBKPSignature",
      // "BKP-Timestamp": "2019-07-17T14:00:00Z"
      "BKP-Timestamp": "$tempTimestamp"
      },
    body: body);

    final int statusCode = response.statusCode;
    
    if (statusCode == 200) {
      print("Connection Success!");
      print("Status: $statusCode");
    } else {
      print("Connection Failed!");
      throw new Exception("Error while fetching data");
    }

    var result = json.decode(response.body);
    var result2 = result['Envelope']['Body']['Output']['Code'];
    var result3 = result['Envelope']['Body']['Output']['Message'];
    var result4 = result['Envelope']['Body']['Output']['Object']['Sms_Otp'];

    // print("Response Body: ${response.body}");
    // print("Status Code: $result2");
    // print("Message: $result3");
    // print("SMS OTP: $result4");

    valueCode = result2.toString();
    valueDesc = result3.toString();
    valueOTP = result4.toString();
    // return print("Code: $valueCode" + ", " + "Message: $valueDesc");
    return valueCode + valueDesc + valueOTP;
  }
