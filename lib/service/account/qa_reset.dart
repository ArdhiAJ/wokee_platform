import 'dart:async';
import 'dart:convert';
import 'package:wokee_platform/view/model/model.dart';
import 'package:http/http.dart' as http;

var valueFailedCode = "";
var valueFailedMessage = "";

var valueStatusCodeQaReset = "";
var valueStatusMessage = "";

Future qaReset() async {
  print("-->User Input (Service)<--");
  // print("NIK: ${txtControllerEktp.text}");
  // print("Date Of Birth: ${txtControllerEktp.text}");

  var url = "http://10.2.62.35:8080/wokee/okoce/qareset";
  print("URL: $url");

  Map dataBody = {
    "clientId": "ec",
    "userId": "user",
    "password": "pass",
    "userName": "fx_zpy2000@yahoo.com",
    "answer": "123456",
    "question": "enter your pin"
  };

  var body = json.encode(dataBody);

  final response = await http.post(url,
      headers: {"Content-Type": "application/json"}, body: body);

  final int statusCode = response.statusCode;

  if (statusCode == 200) {
    print("Connection Success!");
    print("Status Code: $statusCode");
    var result = json.decode(response.body);
    var result1 = result['Data']['code'];
    var result2 = result['Data']['message'];
    valueStatusCodeQaReset = result1.toString();
    valueStatusMessage = result2.toString();

    print("Status Code: $valueStatusCodeQaReset");
    print("Status Message: $valueStatusMessage");

    return valueStatusCodeQaReset +
        valueStatusMessage;
  } else {
    print("Connection Failed!");
    print("Status Code Failed: $statusCode");
    var result = json.decode(response.body);
    var failed1 = result['Data']['code'];
    var failed2 = result['Data']['message'];
    valueFailedCode = failed1.toString();
    valueFailedMessage = failed2.toString();
    return valueFailedCode + valueFailedMessage;
    // throw new Exception("Error while fetching data");
  }
}
