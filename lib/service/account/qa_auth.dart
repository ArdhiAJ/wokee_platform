import 'dart:async';
import 'dart:convert';
import 'package:wokee_platform/view/model/model.dart';
import 'package:http/http.dart' as http;

var valueFailedCode = "";
var valueFailedMessage = "";

var valueStatusCodeQaAuth = "";
var valueStatusMessage = "";
var valueAuthToken = "";
var valueSecretCode = "";

Future qaAuth() async {
  print("-->User Input (Service)<--");
  // print("NIK: ${txtControllerEktp.text}");
  // print("Date Of Birth: ${txtControllerEktp.text}");

  var url = "http://10.2.62.35:8080/wokee/okoce/qaauth";
  print("URL: $url");

  Map dataBody = {
    "clientId": "ec",
    "userId": "user",
    "password": "pass",
    "userName": "fx_zpy2000@yahoo.com",
    "answer": "123456"
  };

  var body = json.encode(dataBody);

  final response = await http.post(url,
      headers: {"Content-Type": "application/json"}, body: body);

  final int statusCode = response.statusCode;

  if (statusCode == 200) {
    print("Connection Success!");
    print("Status Code: $statusCode");
    var result = json.decode(response.body);
    var result1 = result['Data']['code'];
    var result2 = result['Data']['message'];
    var result3 = result['Data']['authToken'];
    var result4 = result['Data']['secretCode'];
    valueStatusCodeQaAuth = result1.toString();
    valueStatusMessage = result2.toString();
    valueAuthToken = result3.toString();
    valueSecretCode = result4.toString();

    print("Status Code: $valueStatusCodeQaAuth");
    print("Status Message: $valueStatusMessage");
    print("Auth Token: $valueAuthToken");
    print("Secret Code: $valueSecretCode");

    return valueStatusCodeQaAuth + valueStatusMessage + valueAuthToken + valueSecretCode;
    
  } else {
    print("Connection Failed!");
    print("Status Code Failed: $statusCode");
    var result = json.decode(response.body);
    var failed1 = result['Data']['code'];
    var failed2 = result['Data']['message'];
    valueFailedCode = failed1.toString();
    valueFailedMessage = failed2.toString();
    return valueFailedCode + valueFailedMessage;
    // throw new Exception("Error while fetching data");
  }
}
