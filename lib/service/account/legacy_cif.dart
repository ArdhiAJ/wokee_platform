import 'dart:async';
import 'dart:convert';
import 'package:wokee_platform/service/disdukcapil/dukcapil_api.dart';
import 'package:wokee_platform/view/model/model.dart';
import 'package:http/http.dart' as http;

var valueStatus = "";
var valueStatusDesc = "";
var valueCifNoLegacy = "";
var valueMobileNoLegacy = "";

var valueStatusFailed = "";
var valueStatusDescFailed = "";
var valueCifNoLegacyFailed = "";
var valueMobileNoLegacyFailed = "";

Future legacyCif() async {
  print("-->User Input (Service)<--");
  print("NIK: $valueEktp");
  String parsingDob = "$valueTanggalLahir".replaceAll("-", "");
  print("Date Of Birth: $parsingDob");

  var url = "http://10.2.62.35:8080/wokee/okoce/legacycifquery";
  print("URL: $url");

  Map dataBody = {
    "fullName": "",
    "maidenName": "",
    "clientId": "my app",
    "user": "user",
    "password": "pass",
    "noEktp": "$valueEktp",
    "dateOfBirth": "$parsingDob"
    // "noEktp": "3302242503760006",
    // "dateOfBirth": "19760325"
  };

  var body = json.encode(dataBody);

  final response = await http.post(url,
      headers: {"Content-Type": "application/json"}, body: body);

  final int statusCode = response.statusCode;

  if (statusCode == 200) {
    print("Connection Success!");
    print("Status Code: $statusCode");
    var result = json.decode(response.body);
    var result1 = result['Response']['Common']['STATUS'];
    var result2 = result['Response']['Common']['STATUS_DESC'];
    var result3 = result['Response']['Output']['CIF_NO_LEGACY'];
    var result4 = result['Response']['Output']['MOBILE_NO_LEGACY'];

    valueStatus = result1.toString();
    valueStatusDesc = result2.toString();
    valueCifNoLegacy = result3.toString();
    valueMobileNoLegacy = result4.toString();

    print("Status: $valueStatus");
    print("Status Desc: $valueStatusDesc");
    print("CIF No Legacy: $valueCifNoLegacy");
    print("Mobile No Legacy: $valueMobileNoLegacy");

    return valueStatus +
        valueStatusDesc +
        valueCifNoLegacy +
        valueMobileNoLegacy;
  } else {
    print("Connection Failed!");
    print("Status Code Failed: $statusCode");
    var result = json.decode(response.body);
    var result5 = result['Response']['Common']['STATUS'];
    var result6 = result['Response']['Common']['STATUS_DESC'];
    var result7 = result['Response']['Output']['CIF_NO_LEGACY'];
    var result8 = result['Response']['Output']['MOBILE_NO_LEGACY'];
    valueStatusFailed = result5.toString();
    valueStatusDescFailed = result6.toString();
    valueCifNoLegacyFailed = result7.toString();
    valueMobileNoLegacyFailed = result8.toString();
    return valueStatusFailed +
        valueStatusDescFailed +
        valueCifNoLegacyFailed +
        valueMobileNoLegacyFailed;
    // throw new Exception("Error while fetching data");
  }
}
