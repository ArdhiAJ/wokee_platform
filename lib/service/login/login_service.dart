import 'dart:io';
import 'dart:async';
import 'dart:convert';
import 'package:wokee_platform/view/model/model.dart';
import 'package:flutter/services.dart';
import 'package:http/io_client.dart';
import 'package:wokee_platform/service/generate_signature.dart';

var valueCode ="";
var valueMessage= "";
var valueUserID = "";
var valueAuthToken = "";
var valueUseSystemPass = "";
var valueLastLogin = "";
var valueAppID = "";
var tempEmailLogin = "";
var tempNewPassword = "";

var valueStatusCode = "";
var valueStatusDesc = "";

Future loginUser() async{
    String apiKey = await rootBundle.loadString('assets/url/apikey.txt');
    String apiSecret = await rootBundle.loadString('assets/url/apisecret.txt');
    print("apiKey: $apiKey");
    print("apiSecret: $apiSecret");
    print("timestamp: $tempTimestamp");
    
    print("-->User Input Service<--");
    print("Email Service: ${txtControllerEmail.text}");
    print("Password Service: ${txtControllerPassword.text}");

    var apiKeyTarget = "$apiKey";
    var apiSecretTarget = "$apiSecret";

    BKPSignature signatureLogin = BKPSignature();
    signatureLogin.getSignature("POST", "{\"cid\":\"ec\",\"cAuthU\":\"user\",\"cAuthP\":\"pass\",\"userName\":\"${txtControllerEmail.text}\",\"password\":\"${txtControllerPassword.text}\"}", "$apiKeyTarget", "$tempTimestamp", "$apiSecretTarget");
    // signatureLogin.getSignature("POST", "{\"cid\":\"ec\",\"cAuthU\":\"user\",\"cAuthP\":\"pass\",\"userName\":\"${txtControllerEmail.text}\",\"password\":\"${txtControllerPassword.text}\"}", "45fd483f-a4ac-4844-badc-7fbb43c0bd70", "$tempTimestamp", "108acb54-ce2d-476a-9b1e-dfbec548cddd");

    // print("-->User Input Service<--");
    // print("Email: ${txtControllerEmail.text}");
    // print("Password: ${txtControllerPassword.text}");

    String prefixUrl = await rootBundle.loadString('assets/url/service.txt');
    var url = "${prefixUrl}centagate/loginWokee";
    print("URL: $url");
    print("Siganature: $valueBKPSignature");

    Map dataBody = {
      "cid":"ec",
      "cAuthU":"user",
      "cAuthP":"pass",
      "userName":"${txtControllerEmail.text}",
      "password":"${txtControllerPassword.text}"
    };

    var body = json.encode(dataBody);

    bool trustSelfSigned = true;
    HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => trustSelfSigned);
    IOClient ioClient = new IOClient(httpClient);

    // final Response = await http.post(
    final response = await ioClient.post(
    url, 
    headers: {
      "Content-Type":"application/json",
      "BKP-ApiKey":"$apiKeyTarget",
      "BKP-Signature":"$valueBKPSignature",
      "BKP-Timestamp": "$tempTimestamp"
    },
    body: body);

    final int statusCode = response.statusCode;
    
    if (statusCode == 200) {
      print("Connection Success!");
      print("Status Code: $statusCode");
      var result = json.decode(response.body);
      // var result1 = result['Status']['statusCode'];
      // var result2 = result['Status']['statusDesc'];
      var result1 = result['Output']['Code'];
      var result2 = result['Output']['Message'];
      var result3 = result['Output']['data']['User_Id'];
      var result4 = result['Output']['data']['Auth_Token'];
      var result5 = result['Output']['data']['App_Id'];
        // var result6 = result['Output']['data']['Secret_Code'];
      var result7 = result['Output']['data']['Use_System_Password'];
        // var result8 = result['Output']['data']['Email'];
      var result9 = result['Output']['data']['Last_Success_Login'];

      // print("Response Body: ${response.body}");
      // print("Code: $result1");
      // print("Message: $result2");
      // print("User ID: $result3");
      print("Auth_Token: $result4"); 
      print("App_Id: $result5");
      // print("Secret_Code: $result6");
      // print("Use_System_Password: $result7");
      // print("Email: $result8"); 
      print("Last_Success_Login: $result9");
      
      valueCode = result1.toString();
      valueMessage = result2.toString();
      valueUserID = result3.toString();
      valueAuthToken = result4.toString();
      valueAppID = result5.toString();
      valueUseSystemPass = result7.toString();
      valueLastLogin = result9.toString();
      // tempEmailLogin = "$email".toString();
      // tempNewPassword = "$password".toString();
      // print("Temp Email: $tempEmailLogin");
      // print("Temp New Password: $tempNewPassword");
      // return print("User ID: $valueUserID" + " , " + "Code: $result1 + ", " + "Message: $result1" + " , " + "Auth_Token: $valueAuthToken");
      // return valueCode + valueMessage + valueUserID + valueAuthToken + valueLastLogin;
      return valueCode + valueMessage + valueUserID + valueAuthToken + valueAppID + valueUseSystemPass + valueLastLogin + tempEmailLogin + tempNewPassword;
    } else {
      print("Connection Failed!");
      print("Status Code Failed: $statusCode");
      var result = json.decode(response.body);
      var failed1 = result['Status']['statusCode'];
      var failed2 = result['Status']['statusDesc'];
      valueStatusCode = failed1.toString();
      valueStatusDesc = failed2.toString();
      return valueStatusCode + valueStatusDesc;
      // throw new Exception("Error while fetching data");
    }
  }