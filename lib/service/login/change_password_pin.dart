import 'package:wokee_platform/view/model/model.dart';
import 'dart:async';
import 'dart:convert';
import 'package:flutter/services.dart';
import 'dart:io';
import 'package:http/io_client.dart';
import 'package:wokee_platform/service/generate_signature.dart';

var valueCode = "";
var valueMessage = "";
var valueNewPassword = "";

Future changePassPinUser() async{
    String apiKey = await rootBundle.loadString('assets/url/apikey.txt');
    String apiSecret = await rootBundle.loadString('assets/url/apisecret.txt');
    var apiKeyTarget = "$apiKey";
    var apiSecretTarget = "$apiSecret";

    BKPSignature signatureChangePassPin = BKPSignature();
    signatureChangePassPin.getSignature("POST", "{\"cid\":\"ec\",\"cAuthU\":\"username\",\"cAuthP\":\"password\",\"userName\":\"${txtControllerEmail.text}\",\"oldPassword\":\"${txtControllerOldPassword.text}\",\"newPassword\":\"${txtControllerNewPassword.text}\",\"oldPin\":\"${txtControllerOldPin.text}\",\"newPin\":\"${txtControllerNewPin.text}\"}", "$apiKeyTarget", "$tempTimestamp", "$apiSecretTarget");
    // signatureChangePassPin.getSignature("POST", "{\"cid\":\"ec\",\"cAuthU\":\"username\",\"cAuthP\":\"password\",\"userName\":\"${txtControllerEmail.text}\",\"oldPassword\":\"${txtControllerOldPassword.text}\",\"newPassword\":\"${txtControllerNewPassword.text}\",\"oldPin\":\"${txtControllerOldPin.text}\",\"newPin\":\"${txtControllerNewPin.text}\"}", "45fd483f-a4ac-4844-badc-7fbb43c0bd70", "$tempTimestamp", "108acb54-ce2d-476a-9b1e-dfbec548cddd");
    print("-->Input Change Password & PIN User<--");
    print("User Name: ${txtControllerEmail.text}");
    print("Sandi Lama: ${txtControllerOldPassword.text}");
    print("Sandi Baru: ${txtControllerNewPassword.text}");
    print("Konfirmasi Sandi Baru: ${txtControllerConfirmNewPassword.text}");
    print("PIN Lama: ${txtControllerOldPin.text}");
    print("PIN Baru: ${txtControllerNewPin.text}");
    print("Konfirmasi PIN Baru: ${txtControllerConfirmNewPin.text}");

    String prefixUrl = await rootBundle.loadString('assets/url/service.txt');
    var url = "${prefixUrl}T24/datapool/chgPinPass";
    print("URL: $url");

    Map dataBody = {
      "cid":"ec",
      "cAuthU":"username",
      "cAuthP":"password",
      "userName":"${txtControllerEmail.text}",
      "oldPassword":"${txtControllerOldPassword.text}",
      "newPassword":"${txtControllerNewPassword.text}",
      "oldPin":"${txtControllerOldPin.text}",
      "newPin":"${txtControllerNewPin.text}"
    };

    var body = json.encode(dataBody);

    bool trustSelfSigned = true;
    HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => trustSelfSigned);
    IOClient ioClient = new IOClient(httpClient);

    final response = await ioClient.post(
    url, 
    headers: {
      "Content-Type":"application/json",
      "BKP-ApiKey":"$apiKeyTarget",
      "BKP-Signature":"$valueBKPSignature",
      "BKP-Timestamp": "$tempTimestamp"
      },
    body: body);

    final int statusCode = response.statusCode;
    
    if (statusCode == 200) {
      print("Connection Success!");
      print("Status: $statusCode");
    } else {
      print("Connection Failed!");
      throw new Exception("Error while fetching data");
    }

    var result = json.decode(response.body);
    var result2 = result['Output']['Code'];
    var result3 = result['Output']['Message'];

    valueCode = result2.toString();
    valueMessage = result3.toString();
    // return print("Code: $valueCode" + ", " + "Message: $valueMessage");
    return valueCode + valueMessage + valueNewPassword;
  }