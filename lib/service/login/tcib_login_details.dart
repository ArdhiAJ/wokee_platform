import 'dart:async';
import 'dart:convert';
import 'package:wokee_platform/view/model/model.dart';
import 'package:flutter/services.dart';
import 'dart:io';
import 'package:http/io_client.dart';
import 'package:wokee_platform/service/generate_signature.dart';
import 'package:wokee_platform/service/login/login_service.dart';
import 'package:http/http.dart' as http;

Future userTCIBL() async {
  print("-->Hit TCIB Login Details<--");
  print("Username: $valueAppID");
  print("Password: ${txtControllerNewPassword.text}");

  var url = "http://10.2.62.35:8080/wokee/okoce/TCIBL";
  print("URL: $url");

  Map dataBody = {
    "companyId": "ID0014202",
    "userName": "$valueAppID",
    "password": "${txtControllerNewPassword.text}"
  };

  var body = json.encode(dataBody);

  final response = await http.post(url,
      headers: {"Content-Type": "application/json"}, body: body);

  final int statusCode = response.statusCode;

  if (statusCode == 200) {
    print("Connection Success!");
    print("Status: $statusCode");
    var result = json.decode(response.body);
    print("Response Body: $result");
  } else {
    print("Status: $statusCode");
    print("Connection Failed!");
    // throw new Exception("Error while fetching data");
  }
}

Future userTCIBLAfterChangePassPIn() async {
  print("-->Hit TCIB Login Details<--");
  print("Username: $valueAppID");
  print("Password: ${txtControllerPassword.text}");

  var url = "http://10.2.62.35:8080/wokee/okoce/TCIBL";
  print("URL: $url");

  Map dataBody = {
    "companyId": "ID0014202",
    "userName": "$valueAppID",
    "password": "${txtControllerPassword.text}"
  };

  var body = json.encode(dataBody);

  final response = await http.post(url,
      headers: {"Content-Type": "application/json"}, body: body);

  final int statusCode = response.statusCode;

  if (statusCode == 200) {
    print("Connection Success!");
    print("Status: $statusCode");
  } else {
    print("Status: $statusCode");
    print("Connection Failed!");
  }
}
