import 'dart:async';
import 'dart:convert';
import 'package:wokee_platform/service/login/login_service.dart';
import 'package:flutter/services.dart';
import 'dart:io';
import 'package:http/io_client.dart';
import 'package:wokee_platform/view/model/model.dart';
import 'package:wokee_platform/service/generate_signature.dart';
  
var valueCode = "";
var valueMessage = "";

Future logoutUser() async{
    String apiKey = await rootBundle.loadString('assets/url/apikey.txt');
    String apiSecret = await rootBundle.loadString('assets/url/apisecret.txt');
    var apiKeyTarget = "$apiKey";
    var apiSecretTarget = "$apiSecret";

    BKPSignature signatureLogout = BKPSignature();
    signatureLogout.getSignature("POST", "{\"appID\":\"ec\",\"deviceID\":\"user\",\"devicePassword\":\"pass\",\"userName\":\"$valueUserID\",\"sessionID\":\"$valueAuthToken\"}", "$apiKeyTarget", "$tempTimestamp", "$apiSecretTarget");
    // signatureLogout.getSignature("POST", "{\"appID\":\"ec\",\"deviceID\":\"user\",\"devicePassword\":\"pass\",\"userName\":\"$valueUserID\",\"sessionID\":\"$valueAuthToken\"}", "45fd483f-a4ac-4844-badc-7fbb43c0bd70", "$tempTimestamp", "108acb54-ce2d-476a-9b1e-dfbec548cddd");

    String prefixUrl = await rootBundle.loadString('assets/url/service.txt');
    var url = "${prefixUrl}T24/datapool/wokeeLogout";
    print("URL: $url");

    Map dataBody = {
      "appID":"ec",
      "deviceID":"user",
      "devicePassword":"pass",
      "userName":"$valueUserID",
      "sessionID":"$valueAuthToken"
    };

    var body = json.encode(dataBody);

    bool trustSelfSigned = true;
    HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => trustSelfSigned);
    IOClient ioClient = new IOClient(httpClient);

    final response = await ioClient.post(
    url, 
    headers: {
      "Content-Type":"application/json",
      "BKP-ApiKey":"$apiKeyTarget",
      "BKP-Signature":"$valueBKPSignature",
      "BKP-Timestamp": "$tempTimestamp"
      },
    body: body);

    final int statusCode = response.statusCode;
    
    if (statusCode == 200) {
      print("Connection Success!");
      print("Status: $statusCode");
    } else {
      print("Connection Failed!");
    }

    var result = json.decode(response.body);
    var result2 = result['Output']['Code'];
    var result3 = result['Output']['Message'];
    
    valueCode = result2.toString();
    valueMessage = result3.toString();

    print("User Name: $valueUserID");
    print("Auth Token: $valueAuthToken");
    // return print("Code: $valueCode" + ", " + "Message: $valueMessage");
    return valueCode + valueMessage;
  }