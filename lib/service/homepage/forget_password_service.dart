import 'dart:async';
import 'dart:convert';
import 'package:wokee_platform/view/model/model.dart';
import 'package:flutter/services.dart';
import 'dart:io';
import 'package:http/io_client.dart';
import 'package:wokee_platform/service/generate_signature.dart';

var valueCode = "";
var valueMessage = "";

Future resetPasswordUser() async{
    String apiKey = await rootBundle.loadString('assets/url/apikey.txt');
    String apiSecret = await rootBundle.loadString('assets/url/apisecret.txt');
    var apiKeyTarget = "$apiKey";
    var apiSecretTarget = "$apiSecret";

    BKPSignature signatureForgotPass = BKPSignature();
    signatureForgotPass.getSignature("POST", "{\"cid\":\"ec\",\"cAuthU\":\"user\",\"cAuthP\":\"pass\",\"userName\":\"${txtControllerUserNameForgotPass.text}\",\"newPassword\":\"${txtControlleruserPasswordForgotPass.text}\"}", "$apiKeyTarget", "$tempTimestamp", "$apiSecretTarget");
    // signatureForgotPass.getSignature("POST", "{\"cid\":\"ec\",\"cAuthU\":\"user\",\"cAuthP\":\"pass\",\"userName\":\"${txtControllerUserNameForgotPass.text}\",\"newPassword\":\"${txtControlleruserPasswordForgotPass.text}\"}", "45fd483f-a4ac-4844-badc-7fbb43c0bd70", "$tempTimestamp", "108acb54-ce2d-476a-9b1e-dfbec548cddd");

    print("-->Input Change Password & PIN User<--");
    print("User Name: ${txtControllerUserNameForgotPass.text}");
    print("Sandi Baru: ${txtControlleruserPasswordForgotPass.text}");

    String prefixUrl = await rootBundle.loadString('assets/url/service.txt');
    var url = "${prefixUrl}centagate/resetPassword";
    print("URL: $url");

    Map dataBody = {
      "cid":"ec",
      "cAuthU":"user",
      "cAuthP":"pass",
      "userName":"${txtControllerUserNameForgotPass.text}",
      "newPassword":"${txtControlleruserPasswordForgotPass.text}"
    };

    var body = json.encode(dataBody);

    bool trustSelfSigned = true;
    HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => trustSelfSigned);
    IOClient ioClient = new IOClient(httpClient);

    final response = await ioClient.post(
    url, 
    headers: {
      "Content-Type":"application/json",
      "BKP-ApiKey":"$apiKeyTarget",
      "BKP-Signature":"$valueBKPSignature",
      "BKP-Timestamp": "$tempTimestamp"
      },
    body: body);

    final int statusCode = response.statusCode;
    
    if (statusCode == 200) {
      print("Connection Success!");
      print("Status: $statusCode");
    } else {
      print("Connection Failed!");
      throw new Exception("Error while fetching data");
    }

    var result = json.decode(response.body);
    var result2 = result['Output']['Code'];
    var result3 = result['Output']['Message'];
  
    valueCode = result2.toString();
    valueMessage = result3.toString();

    // return print("Code: $valueCode" + ", " + "Message: $valueMessage");
    return valueCode + valueMessage;
  }