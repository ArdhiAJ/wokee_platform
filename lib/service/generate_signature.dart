import 'package:crypto/crypto.dart';
import 'dart:convert';

var valueBKPSignature = "";

class BKPSignature {
  String getSignature(String _xHttpVerb, String _msgbody, String _apiKey,
      String _timeStamp, String _apiSecret) {

    if (_xHttpVerb == "POST" || _xHttpVerb == "PUT") {
      _msgbody = _msgbody.replaceAll("[\n\r\t]", "");
      _msgbody = _msgbody.replaceAll(new RegExp(r"\s+\b|\b\s"), "");
    } else {
      _msgbody = "";
    }
    //get MD5
    // print("-->  Data Generate Signature  <--");
    // print("httpVerb: " + _xHttpVerb);
    // print("requestBody: " + _msgbody);
    // print("BKP-ApiKey: " + _apiKey);
    // print("BKP-Timestamp: " + _timeStamp);
    // print("ApiSecret: " + _apiSecret);
    var md5x = md5.convert(utf8.encode(_msgbody));
    var resultMd5 = md5x.toString();
    // print("-->  Proses Generate Signature  <--");
    // print("Hasil MD5: " + resultMd5);
    //getBase64
    var base64x = base64.encode(md5x.bytes);
    // print("Hasil Base64_Encode(MD5): " + base64x);
    //create string for Signature
    var stringtoSignature =
        _xHttpVerb + ":" + base64x + ":" + _apiKey + ":" + _timeStamp;
    // print("String Signature: " + stringtoSignature);
    //HMAC SHA-256
    var hmacKey = utf8.encode(_apiSecret);
    var bodyBytes = utf8.encode(stringtoSignature);
    var hmacked = new Hmac(sha256, hmacKey);
    var stringDigest = hmacked.convert(bodyBytes);
    // print("HMAC: " + stringDigest.toString());
    //base64 HMAC
    var base64Hmacked = base64.encode(stringDigest.bytes);
    // print("Base64_Encode(HMAC): " + base64Hmacked);
    //combine with apikey
    var base64Combined = _apiKey + ":" + base64Hmacked.toString();
    var bkpSignature = base64.encode(utf8.encode(base64Combined));
    // print("BKP-Signature: " + bkpSignature);
    valueBKPSignature = bkpSignature.toString();
    // print("BKP Signature: $valueBKPSignature");
    return valueBKPSignature;
    // return bkpSignature;
  }
}
