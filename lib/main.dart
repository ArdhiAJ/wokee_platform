import 'package:flutter/material.dart';
import 'package:wokee_platform/view/login/login_page.dart';
import 'package:wokee_platform/view/onboarding/camera_example.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Wokee Platform',
      theme: ThemeData(
        textTheme: TextTheme(
          body1: TextStyle(color: Color(0xff2E943E))
        ),
        primarySwatch: Colors.green,
      ),
      home: LoginPage(),
    );
  }
}
