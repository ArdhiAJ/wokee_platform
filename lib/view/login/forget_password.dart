import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:wokee_platform/service/homepage/forget_password_service.dart';
import 'package:wokee_platform/view/login/change_password&PIN.dart';
import 'package:wokee_platform/view/login/login_page.dart';
import 'package:wokee_platform/view/model/color_loader_2.dart';
import 'package:wokee_platform/view/model/model.dart';
// import 'package:wokee_platform/view/login/login.dart';

class ForgetPasswordPage extends StatefulWidget {
  @override
  _ForgetPasswordPageState createState() => _ForgetPasswordPageState();
}

class _ForgetPasswordPageState extends State<ForgetPasswordPage> {
  bool newPasswordVisible = true;
  bool oldPasswordVisible = true;
  final formKey = GlobalKey<FormState>();

  final FocusNode _firstInput = new FocusNode();
  final FocusNode _secondInput = new FocusNode();
  final FocusNode _thirdInput = new FocusNode();

  void _toggleVisibilityPwd() {
    setState(() {
      newPasswordVisible = !newPasswordVisible;
    });
  }

  void _toggleVisibilityConfirmPwd() {
    setState(() {
      oldPasswordVisible = !oldPasswordVisible;
    });
  }

  void _submit() async {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      if (userPasswordForgotPass != confirmUserPasswordForgotPass) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                'Confirm Password',
                textAlign: TextAlign.center,
              ),
              content: const Text(
                  'Confirm password must be match with the password'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  onPressed: () {
                    Navigator.of(context).pop();
                    txtControlleruserPasswordForgotPass.clear();
                    txtControllerConfirmUserPasswordForgotPass.clear();
                  },
                ),
              ],
            );
          },
        );
      } else {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return Center(
                child: ColorLoader2(),
              );
            });
        new Future.delayed(new Duration(seconds: 0), () async {
          //dismiss loading
          await resetPasswordUser();
          _navigateToLogin();
        });
      }
    }
  }

  @override
  void initState() {
    super.initState();
    setState(() {   
     txtControlleruserPasswordForgotPass.clear();
     txtControllerConfirmUserPasswordForgotPass.clear(); 
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      appBar: new PreferredSize(
        preferredSize: Size(double.infinity, 50),
        child: new ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25)),
          child: new AppBar(
            backgroundColor: Color(0xff007022),
            title: new Center(
                  child: new Text(
                    "Reset Password",
                    style: TextStyle(color: Colors.white, fontSize: 25),
                  ),
                ),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8),
                child: new Image.asset('assets/images/logo_wokee.png')
              ),
            ],
          ),
        ),
      ),
      body: new Form(
        key: formKey,
        child: new ListView(
          padding: const EdgeInsets.fromLTRB(15, 15, 15, 0),
          children: <Widget>[
            new Text(
              "Change Your Password?",
              style: TextStyle(fontSize: 18),
              textAlign: TextAlign.center,
            ),
            new SizedBox(
              height: 15,
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 5),
              child: new Text(
                "Email Address",
                style: TextStyle(fontSize: 14),
              ),
            ),
            new TextFormField(
              controller: txtControllerUserNameForgotPass,
              keyboardType: TextInputType.emailAddress,
              focusNode: _firstInput,
              decoration: InputDecoration(
                labelText: 'Enter your email here',
                prefixIcon: Icon(Icons.email),
                contentPadding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30.0)),
              ),
              onEditingComplete: () => //perubahan terbaru
                  FocusScope.of(context).requestFocus(_secondInput),
              validator: validateEmail,
            ),
            new SizedBox(
              height: 15,
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 5),
              child: new Text(
                "New Password",
                style: TextStyle(fontSize: 14),
              ),
            ),
            new TextFormField(
              controller: txtControlleruserPasswordForgotPass,
              keyboardType: TextInputType.text,
              focusNode: _secondInput,
              obscureText: newPasswordVisible,
              decoration: InputDecoration(
                labelText: 'Enter your new password here',
                prefixIcon: Icon(Icons.vpn_key),
                suffixIcon: IconButton(
                    onPressed: _toggleVisibilityPwd,
                    icon: newPasswordVisible
                        ? Icon(Icons.visibility_off)
                        : Icon(Icons.visibility)),
                contentPadding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30.0)),
              ),
              onEditingComplete: () => //perubahan terbaru
                  FocusScope.of(context).requestFocus(_thirdInput),
              onSaved: (val) => userPasswordForgotPass = val,
              validator: validatePassword,
            ),
            new SizedBox(
              height: 15,
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 5),
              child: new Text(
                "Confirm your new password",
                style: TextStyle(fontSize: 14),
              ),
            ),
            new TextFormField(
              controller: txtControllerConfirmUserPasswordForgotPass,
              keyboardType: TextInputType.text,
              focusNode: _thirdInput,
              obscureText: oldPasswordVisible,
              decoration: InputDecoration(
                labelText: 'Enter confirm your new password here',
                prefixIcon: Icon(Icons.vpn_key),
                suffixIcon: IconButton(
                  onPressed: _toggleVisibilityConfirmPwd,
                  icon: oldPasswordVisible
                      ? Icon(Icons.visibility_off)
                      : Icon(Icons.visibility),
                ),
                contentPadding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30.0)),
              ),
              onSaved: (val) => confirmUserPasswordForgotPass = val,
              validator: validatePassword,
            ),
            new SizedBox(
              height: 15,
            ),
            new RaisedButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              color: Color(0xff007022),
              onPressed: _submit,
              // onPressed: () {
              //   Navigator.push(
              //       context,
              //       new MaterialPageRoute(
              //           builder: (context) => new ChangePasswordPINPage()));
              // },
              child: new Text(
                "Reset Password",
                style: TextStyle(color: Colors.white, fontSize: 17, fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _navigateToLogin() async {
    await new Future.delayed(const Duration(seconds: 5));
    if (valueCode == '0') {
      print("Forgot Password Success!");
      Navigator.pop(context); //dismiss loader
      Fluttertoast.showToast(
          msg: "Reset Password Successful",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 1,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);
      Navigator.pushReplacement(context,
          new MaterialPageRoute(builder: (context) => new LoginPage()));
      txtControllerPassword.clear();
    } else {
      print("Reset Password Failed");
      Navigator.pop(context); //dismiss loader
      _showdialog("Message: $valueMessage");
    }
  }

  _showdialog(String msg) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: new AlertDialog(
          // title: Text("Pesan"),
          content: new Text(
            msg,
            style: new TextStyle(fontSize: 16.0),
          ),
          actions: <Widget>[
            new FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                  txtControllerUserNameForgotPass.clear();
                  txtControlleruserPasswordForgotPass.clear();
                  txtControllerConfirmUserPasswordForgotPass.clear();
                },
                child: new Text("OK"))
          ],
        ));
  }
}
