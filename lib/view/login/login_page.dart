import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/io_client.dart';
import 'package:local_auth/auth_strings.dart';
import 'package:wokee_platform/service/generate_signature.dart';
import 'package:wokee_platform/service/login/login_service.dart';
import 'package:wokee_platform/service/login/tcib_login_details.dart';
import 'package:wokee_platform/view/home/input_pin.dart';
// import 'package:wokee_platform/view/home/home_page.dart';
import 'package:wokee_platform/view/home/main_menu.dart';
import 'package:wokee_platform/view/login/change_password&PIN.dart';
import 'package:wokee_platform/view/onboarding/aboutyou2.dart';
import 'package:wokee_platform/view/onboarding/camera_example.dart';
import 'package:wokee_platform/view/onboarding/home_onboarding.dart';
import 'package:wokee_platform/view/onboarding/onboarding_page.dart';
import 'package:wokee_platform/view/register/register.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:local_auth/local_auth.dart';
import 'package:wokee_platform/view/model/model.dart';
import 'package:wokee_platform/view/model/color_loader_2.dart';
import 'package:http/http.dart' as http;

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool checkboxEmail = false;
  bool checkboxBiometric = false;
  bool passwordVisible = true;
  bool _canCheckBiometric = false;
  bool _isTextFormFieldEmail = true;
  bool _isEnabledUsr = true;
  bool _isEnabledPwd = true;

  // String _txtStatusOnboarding = "$valueStatusOnboarding";
  // String _txtStatusOnboarding = "";

  final formKey = GlobalKey<FormState>();

  SharedPreferences sharedPreferences;

  final FocusNode _firstInput = new FocusNode();
  final FocusNode _secondInput = new FocusNode();
  final LocalAuthentication _localAuthentication = LocalAuthentication();
  List<BiometricType> _availableBiometricTypes = List<BiometricType>();

  void _toggleVisibility() {
    setState(() {
      passwordVisible = !passwordVisible;
    });
  }

  void _submit() async {
    //pakai async
    print("Save Usr: ${txtControllerEmail.text}");
    print("Save Pwd: ${txtControllerPassword.text}");
    if (formKey.currentState.validate()) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return Center(
              child: ColorLoader2(),
            );
          });

      new Future.delayed(new Duration(seconds: 0), () async {
        if (checkboxEmail) {
          print("Ini Fitur Simpan Alamat Email");
          await loginUser();
          _navigateToChangePassPIN();
          // Navigator.push(
          //     context,
          //     new MaterialPageRoute(
          //         builder: (context) => new HomeOnboarding()));
        } else if (checkboxBiometric) {
          print("Ini Fitur Simpan Biometric (Fingerprint/FaceID)");
          _checkBiometric();
        } else {
          print("Ini Fitur Login Biasa");
          await loginUser();
          _navigateToChangePassPIN();

          // CustomerIdHomeOnBoarding customerIdLogin =
          //     new CustomerIdHomeOnBoarding("$valueCustomerID");
          // setState(() async {
          //   if (_txtStatusOnboarding == 'null') {
          //     Navigator.push(
          //         context,
          //         new MaterialPageRoute(
          //             builder: (context) => new HomeOnboarding(
          //                 customerIdHomeOnBoarding: customerIdLogin)));

          //     print("Onboarding Pending");
          //   } else {
          //     Navigator.push(
          //         context,
          //         new MaterialPageRoute(
          //             builder: (context) => new MainMenuController()));
          //     print("Onboarding Sukses");
          //   }
          // });
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    double sbarHeight = MediaQuery.of(context).padding.top;
    txtControllerMaskingEmail.text = "$valueUsrMasked";

    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      body: new Form(
        key: formKey,
        child: new ListView(
          padding: EdgeInsets.only(left: 30, right: 30, top: sbarHeight),
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 17),
              child: new Image.asset(
                'assets/images/logo_wokee.png',
                height: 90,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 5),
              child: new Text(
                "Email",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
              ),
            ),
            _isTextFormFieldEmail
                ? new TextFormField(
                    controller: txtControllerEmail,
                    enabled: _isEnabledUsr,
                    focusNode: _firstInput,
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                      hintText: 'Please enter your email here',
                      prefixIcon: Icon(Icons.email),
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20.0)),
                    ),
                    onEditingComplete: () =>
                        FocusScope.of(context).requestFocus(_secondInput),
                    validator: validateEmail,
                  )
                : new TextField(
                    controller: txtControllerMaskingEmail,
                    enabled: false,
                    focusNode: _firstInput,
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.email),
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20.0)),
                    ),
                  ),
            // new Center(
            //     child: new Text('$valueUsrMasked',
            //         style: TextStyle(fontSize: 18.0, color: Colors.black87),
            //         textAlign: TextAlign.center),
            //   ),
            Padding(
              padding: const EdgeInsets.only(top: 7),
              child: new Text(
                "Password",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
              ),
            ),
            new TextFormField(
              controller: txtControllerPassword,
              enabled: _isEnabledPwd,
              focusNode: _secondInput,
              obscureText: passwordVisible,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                hintText: 'Please enter your password here',
                prefixIcon: Icon(Icons.vpn_key),
                suffixIcon: IconButton(
                    onPressed: _toggleVisibility,
                    icon: passwordVisible
                        ? Icon(Icons.visibility_off)
                        : Icon(Icons.visibility)),
                contentPadding:
                    EdgeInsets.symmetric(vertical: 0, horizontal: 0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20.0)),
              ),
              validator: validatePassword,
            ),
            new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 10, bottom: 7),
                  child: new ButtonTheme(
                    // minWidth: 10.0,
                    child: new RaisedButton(
                      color: Color(0xff2E943E),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(7),
                      ),
                      onPressed: () {
                        _submit();
                        // Navigator.pushReplacement(context, new MaterialPageRoute(builder: (context) => new ExCamPage()));
                        // Navigator.push(
                        //     context,
                        //     new MaterialPageRoute(
                        //         builder: (context) => new HomeOnboarding()));
                      },
                      child: new Text(
                        "Login",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 17),
                      ),
                    ),
                  ),
                ),
                new InkWell(
                  onTap: () {},
                  child: new Text(
                    "Forgot Password?",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                  ),
                ),
              ],
            ),
            // new SizedBox(
            //   height: 30,
            //   child: new CheckboxListTile(
            //     controlAffinity: ListTileControlAffinity.leading,
            //     value: checkboxEmail,
            //     onChanged: (bool valueEmail) {
            //       setState(() {
            //         checkboxEmail = valueEmail;
            //       });
            //     },
            //     title: new Text(
            //       "Ingatkan Email Saya",
            //       style: TextStyle(color: Color(0xff2E943E), fontSize: 14),
            //     ),
            //   ),
            // ),
            new SizedBox(
              height: 30,
              child: new CheckboxListTile(
                controlAffinity: ListTileControlAffinity.leading,
                value: checkboxEmail,
                onChanged: _onChange,
                secondary: const Icon(
                  Icons.mail_outline,
                  color: Color(0xff007022),
                ),
                title: new Text(
                  "Remember My Email",
                  style: TextStyle(color: Color(0xff2E943E), fontSize: 14),
                ),
                // onChanged: (bool value) {
                //   setState(() {
                //     checkboxEmail = value;
                //   });
                // },
              ),
            ),
            // new CheckboxListTile(
            //   controlAffinity: ListTileControlAffinity.leading,
            //   value: checkboxBiometric,
            //   onChanged: (bool valueBiometric) {
            //     setState(() {
            //       checkboxBiometric = valueBiometric;
            //     });
            //   },
            //   title: new Text(
            //     "Masuk dengan Biometric",
            //     style: TextStyle(color: Color(0xff2E943E), fontSize: 14),
            //   ),
            // ),
            new CheckboxListTile(
              controlAffinity: ListTileControlAffinity.leading,
              value: checkboxBiometric,
              onChanged: _onChange2,
              secondary: const Icon(
                Icons.fingerprint,
                color: Color(0xff007022),
              ),
              title: new Text(
                "Login With Fingerprint",
                style: TextStyle(color: Color(0xff2E943E), fontSize: 14),
              ),
              // onChanged: (bool value) {
              //   setState(() {
              //     checkboxBiometric = value;
              //   });
              // },
            ),
            new GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (context) => new RegisterPage()));
                        
              },
              child: new Card(
                margin: const EdgeInsets.only(bottom: 5),
                elevation: 2,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                  child: new Text(
                    "Register Now",
                    style: TextStyle(fontSize: 15),
                  ),
                ),
              ),
            ),
            new Card(
              margin: const EdgeInsets.only(bottom: 5),
              elevation: 2,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15),
              ),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                child: new Text(
                  "Get My Bank",
                  style: TextStyle(fontSize: 15),
                ),
              ),
            ),
            new Card(
              margin: const EdgeInsets.only(bottom: 5),
              elevation: 2,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15),
              ),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                child: new Text(
                  "Currency Exchange Rates",
                  style: TextStyle(fontSize: 15),
                ),
              ),
            ),
            new Card(
              margin: const EdgeInsets.only(bottom: 5),
              elevation: 2,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15),
              ),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                child: new Text(
                  "Change Language",
                  style: TextStyle(fontSize: 15),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 7),
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Text(
                    ": Powered by :",
                    style: TextStyle(color: Colors.black, fontSize: 12),
                  ),
                  new Image.asset(
                    'assets/images/logo_bbkp.png',
                    height: 30,
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  var valueLastTimeLogin = "";
  static var valueStatusOnboarding = "";
  var valueCustomerID = "";
  var valueSavingAC = "";
  var valueName = "";

  Future atiQueryEdgeConnectUsername() async {
    print("-->User Input ATIEC<--");
    print("Email: ${txtControllerEmail.text}");
    print("Password: ${txtControllerPassword.text}");

    // print("-->User Input<--");
    print("User ID Login: $valueUserID");

    var url = "http://10.2.62.35:8080/wokee/okoce/atiqueryusername";
    print("URL: $url");

    Map dataBody = {
      "company": "",
      "username": "DFW17009",
      "password": "123123",
      "columnName": "@ID",
      "criteriaValue": "$valueUserID",
      "operand": "EQ"
    };

    var body = json.encode(dataBody);

    final response = await http.post(url,
        headers: {"Content-Type": "application/json"}, body: body);

    final int statusCode = response.statusCode;

    if (statusCode == 200) {
      print("Connection Success!");
      print("Status Code: $statusCode");
      var result = json.decode(response.body);
      var result4 = result['ATIQueryEdgeConnectUsernameResponse']
              ['ATIENECUSERNAMEType']['gATIENECUSERNAMEDetailType']
          ['mATIENECUSERNAMEDetailType']['LASTTIMELOGIN'];
      var result5 = result['ATIQueryEdgeConnectUsernameResponse']
              ['ATIENECUSERNAMEType']['gATIENECUSERNAMEDetailType']
          ['mATIENECUSERNAMEDetailType']['CUSTONBOARDSTATUS'];
      var result6 = result['ATIQueryEdgeConnectUsernameResponse']
              ['ATIENECUSERNAMEType']['gATIENECUSERNAMEDetailType']
          ['mATIENECUSERNAMEDetailType']['CUSTOMER'];
      var result7 = result['ATIQueryEdgeConnectUsernameResponse']
              ['ATIENECUSERNAMEType']['gATIENECUSERNAMEDetailType']
          ['mATIENECUSERNAMEDetailType']['SAVINGAC'];
      var result8 = result['ATIQueryEdgeConnectUsernameResponse']
              ['ATIENECUSERNAMEType']['gATIENECUSERNAMEDetailType']
          ['mATIENECUSERNAMEDetailType']['NAME'];
      print("Last Time Login: $result4");
      print("Status OnBoarding: $result5");
      print("Customer ID: $result6");
      print("Saving Account: $result7");
      print("Name Account: $result8");

      setState(() {
        valueLastTimeLogin = result4.toString();
        valueStatusOnboarding = result5.toString();
        valueCustomerID = result6.toString();
        valueSavingAC = result7.toString();
        valueName = result8.toString();
        return valueLastTimeLogin +
            valueStatusOnboarding +
            valueCustomerID +
            valueSavingAC +
            valueName;
      });
    } else if (statusCode != 200) {
      print("Connection Failed!");
      _showdialog("Can't Get Last Time Login!");
    }
  }

//old atiqueryedgeconnectusername (hendra)
  // // var valueExternalUser = "";
  // // var valuePassword = "";
  // // var valuePIN = "";
  // var valueLastTimeLogin = "";
  // static var valueStatusOnboarding = "";

  // Future atiQueryEdgeConnectUsername() async {
  //   print("-->User Input ATIEC<--");
  //   print("Email: ${txtControllerEmail.text}");
  //   print("Password: ${txtControllerPassword.text}");
  //   String apiKey = await rootBundle.loadString('assets/url/apikey.txt');
  //   String apiSecret = await rootBundle.loadString('assets/url/apisecret.txt');
  //   var apiKeyTarget = "$apiKey";
  //   var apiSecretTarget = "$apiSecret";
  //   print("ATIEC");
  //   print("apiKey: $apiKey");
  //   print("apiSecret: $apiSecret");
  //   print("timestamp: $tempTimestamp");

  //   BKPSignature signatureATIQueryUsername = BKPSignature();
  //   signatureATIQueryUsername.getSignature(
  //       "POST",
  //       "{\"company\":\"\",\"username\":\"INTF01\",\"password\":\"123123\",\"columnName\":\"@ID\",\"criteriaValue\":\"$valueUserID\",\"operand\":\"EQ\"}",
  //       "$apiKeyTarget",
  //       "$tempTimestamp",
  //       "$apiSecretTarget");
  //   // signatureATIQueryUsername.getSignature("POST", "{\"company\":\"\",\"username\":\"INTF01\",\"password\":\"123123\",\"columnName\":\"@ID\",\"criteriaValue\":\"$valueUserID\",\"operand\":\"EQ\"}", "45fd483f-a4ac-4844-badc-7fbb43c0bd70", "$merge", "108acb54-ce2d-476a-9b1e-dfbec548cddd");

  //   // print("-->User Input<--");
  //   print("User ID Login: $valueUserID");

  //   String prefixUrl = await rootBundle.loadString('assets/url/service.txt');
  //   var url = "${prefixUrl}T24/datapool/ATIQueryEdgeConectUsername";
  //   print("URL: $url");

  //   Map dataBody = {
  //     "company": "",
  //     "username": "INTF01",
  //     "password": "123123",
  //     "columnName": "@ID",
  //     "criteriaValue": "$valueUserID",
  //     "operand": "EQ"
  //   };

  //   var body = json.encode(dataBody);

  //   bool trustSelfSigned = true;
  //   HttpClient httpClient = new HttpClient()
  //     ..badCertificateCallback =
  //         ((X509Certificate cert, String host, int port) => trustSelfSigned);
  //   IOClient ioClient = new IOClient(httpClient);

  //   final response = await ioClient.post(url,
  //       headers: {
  //         "Content-Type": "application/json",
  //         "BKP-ApiKey": "$apiKeyTarget",
  //         "BKP-Signature": "$valueBKPSignature",
  //         "BKP-Timestamp": "$merge"
  //       },
  //       body: body);

  //   final int statusCode = response.statusCode;

  //   if (statusCode == 200) {
  //     print("Connection Success!");
  //     print("Status Code: $statusCode");
  //     var result = json.decode(response.body);
  //     var result4 = result['Envelope']['Body']
  //                 ['ATIQueryEdgeConnectUsernameResponse']['ATIENECUSERNAMEType']
  //             ['gATIENECUSERNAMEDetailType']['mATIENECUSERNAMEDetailType']
  //         ['LASTTIMELOGIN'];
  //     var result5 = result['Envelope']['Body']
  //                 ['ATIQueryEdgeConnectUsernameResponse']['ATIENECUSERNAMEType']
  //             ['gATIENECUSERNAMEDetailType']['mATIENECUSERNAMEDetailType']
  //         ['CUSTONBOARDSTATUS'];
  //     print("Last Time Login: $result4");
  //     print("Status OnBoarding: $result5");

  //     setState(() {
  //       valueLastTimeLogin = result4.toString();
  //       valueStatusOnboarding = result5.toString();
  //       return
  //           // valueExternalUser +
  //           //     valuePassword +
  //           //     valuePIN +
  //           valueLastTimeLogin + valueStatusOnboarding;
  //     });
  //   } else if (statusCode != 200) {
  //     print("Connection Failed!");
  //     _showdialog("Can't Get Last Time Login!");
  //   }
  // }

  var loginTime = "";
  _navigateToChangePassPIN() async {
    // print("-->User Input Front End<--");
    // print("Email: ${txtControllerEmail.text}");
    // print("Password: ${txtControllerPassword.text}");
    await new Future.delayed(const Duration(seconds: 3));
    var route = new MaterialPageRoute(
      builder: (BuildContext context) => new ChangePasswordPINPage(),
    );
    if (valueCode == '0' && valueMessage == 'Success') {
      String ceklastLogin = await atiQueryEdgeConnectUsername();
      loginTime = "$valueLastTimeLogin";
      print("Last Login(Return): $loginTime");
      if (loginTime == 'null') {
        print("LTL: $loginTime");
        Navigator.pop(context);
        print("Must be change password & pin!");
        Navigator.of(context).push(route);
      } else if (loginTime != 'null') {
        print("Login Success");
        print("LTL: $loginTime");
        String _txtStatusOnboarding = "$valueStatusOnboarding";
        // String _txtStatusOnboarding = "done";
        print("SOB: $_txtStatusOnboarding");
        var tcibLogin = await userTCIBLAfterChangePassPIn();
        if (_txtStatusOnboarding == 'null') {
          Navigator.pop(context);
          Fluttertoast.showToast(
              msg: "Login Successful",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIos: 1,
              backgroundColor: Colors.green,
              textColor: Colors.white,
              fontSize: 16.0);
          CustomerIdHomeOnBoarding customerIdLogin =
              new CustomerIdHomeOnBoarding("$valueCustomerID");
          Navigator.pushReplacement(
              context,
              new MaterialPageRoute(
                  builder: (context) => new HomeOnboarding(
                        customerIdHomeOnBoarding: customerIdLogin,
                      )));
        } else {
          Navigator.pop(context); //yg sebelumnya
          Fluttertoast.showToast(
              msg: "Login Successful",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIos: 1,
              backgroundColor: Colors.green,
              textColor: Colors.white,
              fontSize: 16.0);
          // SavingAccount savingAccount = new SavingAccount("$valueSavingAC", "$valueLastTimeLogin");
          // Navigator.pushReplacement(
          //     context,
          //     new MaterialPageRoute(
          //         builder: (context) =>
          //             new MainMenuController(savingAC: savingAccount)));

          Navigator.pop(context);
          CustomerIdHomeOnBoarding customerIdLogin =
              new CustomerIdHomeOnBoarding("$valueCustomerID");
          Navigator.pushReplacement(
              context,
              new MaterialPageRoute(
                  builder: (context) => new HomeOnboarding(
                        customerIdHomeOnBoarding: customerIdLogin,
                      )));

        } //update last time login terbaru

      }
    } else {
      print("Login Failed");
      Navigator.pop(context);
      if (valueMessage == "Invalid credentials") {
        // Navigator.pushReplacement(context,
        //     new MaterialPageRoute(builder: (context) => new AboutYouPage2()));

        _showdialog("User email dan password \ntidak valid");
        _showdialog("Invalid Credentials");
        _isEnabledPwd = true;
        print("Temp Password: ${txtControllerPassword.text}");
      } else if (valueMessage == "User in locked status") {
        // _showdialog("User dalam status terkunci");
        _showdialog("User in Locked Status");
      } else {
        // _showdialog("Koneksi Gagal");
        _showdialog("Koneksi Failed");
      }
    }
  }

  _showdialog(String msg) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: new AlertDialog(
          // title: Text("Pesan"),
          content: new Text(
            msg,
            style: new TextStyle(fontSize: 16.0),
          ),
          actions: <Widget>[
            new FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                  setState(() {
                    _isEnabledUsr = true;
                    //  _isEnabledPwd = true;
                  });
                  _isEnabledPwd = true;
                  // txtControllerEmail.clear();
                  txtControllerPassword.clear();
                },
                child: new Text("OK"))
          ],
        ));
  }

  @override
  void initState() {
    super.initState();
    getCredentialEmail();
    getCredentialBiometric();
  }

  _showdialogCheckboxEmail(String msg) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: new AlertDialog(
          title: Text("Warning"),
          content: new Text(
            msg,
            style: new TextStyle(fontSize: 16.0),
          ),
          actions: <Widget>[
            new FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                  sharedPreferences.clear();
                  setState(() {
                    checkboxEmail = false;
                    checkboxBiometric = false;
                    _isEnabledUsr = true;
                    _isEnabledPwd = true;
                  });
                },
                child: new Text("OK"))
          ],
        ));
  }

  var valueUsrMasked = "";
  masking() {
    if (txtControllerEmail.text != "") {
      String tempUsr = txtControllerEmail.text;
      int indexAt = tempUsr.indexOf("@");
      int prefix, suffix;
      prefix = indexAt + 3;
      suffix = indexAt - 3;
      String selectText = tempUsr.substring(suffix, prefix);
      String replaceText = tempUsr.replaceAll("$selectText", '******');
      valueUsrMasked = replaceText.toString();
      print("Username(Masking): $valueUsrMasked");
      return valueUsrMasked;
    } else if (txtControllerEmail.text == "" &&
        txtControllerPassword.text == "") {
      _isTextFormFieldEmail = false;
      print("User Masking Gagal!");
      // _showdialogCheckboxEmail("User Email dan Password tidak boleh kosong");
      _showdialogCheckboxEmail("User Email and Password Can't be Empty");
    }
  }

  _onChange(bool value) async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      checkboxEmail = value;
      // checkboxBiometric = false;
      sharedPreferences.setBool('checkEmail', checkboxEmail);
      sharedPreferences.setString('rememberEmail', txtControllerEmail.text);
      sharedPreferences.commit();
      getCredentialEmail();
    });
  }

  getCredentialEmail() async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      checkboxEmail = sharedPreferences.getBool('checkEmail');
      if (checkboxEmail != null) {
        if (checkboxEmail) {
          txtControllerEmail.text =
              sharedPreferences.getString('rememberEmail');
          setState(() {
            masking();
            // _checkBiometric(); //ini yang harus dihapuskan
            // print("User Email: ${txtControllerEmail.text}");
            // print("User Password: ${txtControllerPassword.text}");
            _isTextFormFieldEmail = !_isTextFormFieldEmail;
            _isEnabledUsr = false;
            _isEnabledPwd = true; //lock textformfield
          });
        } else {
          txtControllerEmail.clear();
          txtControllerPassword.clear();
          sharedPreferences.clear();
          setState(() {
            _isTextFormFieldEmail = true;
            _isEnabledUsr = true;
            _isEnabledPwd = true;
          });
        }
      } else {
        checkboxEmail = false;
      }
    });
  }

  _onChange2(bool value2) async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      checkboxBiometric = value2;
      sharedPreferences.setBool('checkBiometric', checkboxBiometric);
      sharedPreferences.setString('rememberEmail', txtControllerEmail.text);
      sharedPreferences.setString(
          'rememberPassword', txtControllerPassword.text);
      sharedPreferences.commit();
      getCredentialBiometric();
    });
  }

  getCredentialBiometric() async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      checkboxBiometric = sharedPreferences.getBool('checkBiometric');
      if (checkboxBiometric != null) {
        if (checkboxBiometric) {
          txtControllerEmail.text =
              sharedPreferences.getString('rememberEmail');
          txtControllerPassword.text =
              sharedPreferences.getString('rememberPassword');
          setState(() {
            masking();
            // _checkBiometric(); //ini yang harus dihapuskan
            print("User Email: ${txtControllerEmail.text}");
            print("User Password: ${txtControllerPassword.text}");
            _isTextFormFieldEmail = !_isTextFormFieldEmail;
            _isEnabledUsr = false;
            _isEnabledPwd = false; //lock textformfield
          });
        } else {
          txtControllerEmail.clear();
          txtControllerPassword.clear();
          sharedPreferences.clear();
          setState(() {
            _isTextFormFieldEmail = true;
            _isEnabledUsr = true;
            _isEnabledPwd = true;
          });
        }
      } else {
        checkboxBiometric = false;
      }
    });
  }

  _showdialogCekBiometrik(String msg) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: new AlertDialog(
          title: Text("Check Biometric"),
          content: new Text(
            msg,
            style: new TextStyle(fontSize: 16.0),
          ),
          actions: <Widget>[
            new FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: new Text("OK"))
          ],
        ));
  }

  Future<void> _checkBiometric() async {
    bool canCheckBiometric = false;
    try {
      canCheckBiometric = await _localAuthentication.canCheckBiometrics;
    } on PlatformException catch (e) {
      print(e);
    }

    if (!mounted) return;

    setState(() {
      _canCheckBiometric = canCheckBiometric;
      if (_canCheckBiometric == true) {
        print("Check Biometric: $_canCheckBiometric");
        _getListOfBiometricTypes();
        _authorizeNow();
      } else {
        // _showdialogCekBiometrik("Maaf device Anda belum mendukung Biometric");
        _showdialogCekBiometrik("Sorry Your Device Doesn't Support Biometric");
      }
    });
  }

  Future<void> _getListOfBiometricTypes() async {
    List<BiometricType> listofBiometrics;
    try {
      listofBiometrics = await _localAuthentication.getAvailableBiometrics();
    } on PlatformException catch (e) {
      print(e);
    }

    if (!mounted) return;

    setState(() {
      _availableBiometricTypes = listofBiometrics;
      print("Available Biometric: $_availableBiometricTypes");
    });
  }

  Future<void> _authorizeNow() async {
    bool isAuthorized = false;

    const androidString = const AndroidAuthMessages(
      cancelButton: "Batalkan",
      goToSettingsButton: "Pengaturan",
      signInTitle: "Autentikasi",
      fingerprintHint: "Silahkan Tempel Sidik Jari di Sensor Device Anda",
      fingerprintNotRecognized: "Fingerprint Anda Tidak Dikenali/Diakui",
      fingerprintSuccess: "Fingerprint Anda Telah Dikenali/Diakui",
      goToSettingsDescription: "Tolong Konfigurasikan Sidik Jari Anda",
    );

    try {
      isAuthorized = await _localAuthentication.authenticateWithBiometrics(
        localizedReason: "Please Authenticate to Complete this Process",
        useErrorDialogs: true,
        stickyAuth: true,
        // androidAuthStrings: androidString,
      );
    } on PlatformException catch (e) {
      print(e);
    }

    if (!mounted) return;

    setState(() {
      if (isAuthorized) {
        loginUser();
        _navigateToChangePassPIN();
        // Navigator.push(
        //     context,
        //     new MaterialPageRoute(
        //         builder: (context) => new MainMenuController()));
        // Navigator.push(
        //     context,
        //     new MaterialPageRoute(
        //         builder: (context) => new HomeOnboarding()));
      } else {}
    });
  }
}
