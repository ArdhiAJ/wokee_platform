import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:wokee_platform/service/login/tcib_login_details.dart';
import 'package:wokee_platform/view/model/color_loader_2.dart';
import 'package:wokee_platform/view/model/model.dart';
import 'package:wokee_platform/service/login/change_password_pin.dart';
import 'package:wokee_platform/view/login/login_page.dart';

class ChangePasswordPINPage extends StatefulWidget {
  @override
  _ChangePasswordPINPageState createState() => _ChangePasswordPINPageState();
}

class _ChangePasswordPINPageState extends State<ChangePasswordPINPage> {
  bool newPasswordVisible = true;
  bool oldPasswordVisible = true;
  bool confirmPasswordVisible = true;

  bool newPinVisible = true;
  bool oldPinVisible = true;
  bool confirmPinVisible = true;
  final formKey = GlobalKey<FormState>();

  final FocusNode _firstInput = new FocusNode();
  final FocusNode _secondInput = new FocusNode();
  final FocusNode _thirdInput = new FocusNode();
  final FocusNode _fourInput = new FocusNode();
  final FocusNode _fiveInput = new FocusNode();
  final FocusNode _sixInput = new FocusNode();

  void _toggleVisibilityNewPwd() {
    setState(() {
      newPasswordVisible = !newPasswordVisible;
    });
  }

  void _toggleVisibilityOldPwd() {
    setState(() {
      oldPasswordVisible = !oldPasswordVisible;
    });
  }

  void _toggleVisibilityConfirmPwd() {
    setState(() {
      confirmPasswordVisible = !confirmPasswordVisible;
    });
  }

  void _toggleVisibilityNewPin() {
    setState(() {
      newPinVisible = !newPinVisible;
    });
  }

  void _toggleVisibilityOldPin() {
    setState(() {
      oldPinVisible = !oldPinVisible;
    });
  }

  void _toggleVisibilityConfirmPin() {
    setState(() {
      confirmPinVisible = !confirmPinVisible;
    });
  }

  void _submit() async {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      if (newPassword != confirmNewPassword) {
        //|| (newPIN != confirmNewPin)
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                'Confirm Password',
                textAlign: TextAlign.center,
              ),
              content: const Text(
                  'Confirm password must be match with the password'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  onPressed: () {
                    Navigator.of(context).pop();
                    txtControllerNewPassword.clear();
                    txtControllerConfirmNewPassword.clear();
                  },
                ),
              ],
            );
          },
        );
      }
      if (newPIN != confirmNewPin) {
        //|| (newPIN != confirmNewPin)
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                'Confirm PIN',
                textAlign: TextAlign.center,
              ),
              content: const Text('Confirm PIN must be match with the PIN'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Oke'),
                  onPressed: () {
                    Navigator.of(context).pop();
                    txtControllerNewPin.clear();
                    txtControllerConfirmNewPin.clear();
                  },
                ),
              ],
            );
          },
        );
      } else {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return Center(
                child: ColorLoader2(),
              );
            });
        new Future.delayed(new Duration(seconds: 0), () async {
          //dismiss loader
          await changePassPinUser();
          _navigateToLogin();
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      appBar: new PreferredSize(
        preferredSize: Size(double.infinity, 50),
        child: new ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25)),
          child: new AppBar(
            backgroundColor: Color(0xff007022),
            title: new Center(
                  child: new Text(
                    "Change Password & PIN",
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                ),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8),
                child: new Image.asset('assets/images/logo_wokee.png')
              ),
            ],
          ),
        ),
      ),
      body: new Form(
        key: formKey,
        child: new ListView(
          padding: const EdgeInsets.fromLTRB(15, 15, 15, 0),
          children: <Widget>[
            new Text(
              "Change Password",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            new SizedBox(
              height: 5,
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 5),
              child: new Text(
                "Old Password",
                style: TextStyle(fontSize: 14),
              ),
            ),
            new TextFormField(
              controller: txtControllerOldPassword,
              keyboardType: TextInputType.text,
              focusNode: _firstInput,
              obscureText: oldPasswordVisible,
              maxLength: 16,
              decoration: InputDecoration(
                labelText: 'Enter your old password here',
                prefixIcon: Icon(Icons.code),
                suffixIcon: IconButton(
                  onPressed: _toggleVisibilityOldPwd,
                  icon: oldPasswordVisible
                      ? Icon(Icons.visibility_off)
                      : Icon(Icons.visibility),
                ),
                contentPadding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30.0)),
              ),
              onEditingComplete: () => //perubahan terbaru
                  FocusScope.of(context).requestFocus(_secondInput),
              validator: validatePassword,
            ),
            // new SizedBox(
            //   height: 15,
            // ),
            Padding(
              padding: const EdgeInsets.only(bottom: 5),
              child: new Text(
                "New Password",
                style: TextStyle(fontSize: 14),
              ),
            ),
            new TextFormField(
              controller: txtControllerNewPassword,
              keyboardType: TextInputType.text,
              focusNode: _secondInput,
              obscureText: newPasswordVisible,
              maxLength: 16,
              decoration: InputDecoration(
                labelText: 'Enter your new password here',
                prefixIcon: Icon(Icons.code),
                suffixIcon: IconButton(
                  onPressed: _toggleVisibilityNewPwd,
                  icon: newPasswordVisible
                      ? Icon(Icons.visibility_off)
                      : Icon(Icons.visibility),
                ),
                contentPadding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30.0)),
              ),
              onEditingComplete: () => //perubahan terbaru
                  FocusScope.of(context).requestFocus(_thirdInput),
              validator: validatePassword,
              onSaved: (val) => newPassword = val,
            ),
            // new SizedBox(
            //   height: 15,
            // ),
            Padding(
              padding: const EdgeInsets.only(bottom: 5),
              child: new Text(
                "Confirm your new password",
                style: TextStyle(fontSize: 14),
              ),
            ),
            new TextFormField(
              controller: txtControllerConfirmNewPassword,
              keyboardType: TextInputType.text,
              focusNode: _thirdInput,
              obscureText: confirmPasswordVisible,
              maxLength: 16,
              decoration: InputDecoration(
                labelText: 'Enter confirm your new password here',
                prefixIcon: Icon(Icons.code),
                suffixIcon: IconButton(
                  onPressed: _toggleVisibilityConfirmPwd,
                  icon: confirmPasswordVisible
                      ? Icon(Icons.visibility_off)
                      : Icon(Icons.visibility),
                ),
                contentPadding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30.0)),
              ),
              onEditingComplete: () => //perubahan terbaru
                  FocusScope.of(context).requestFocus(_fourInput),
              validator: validatePassword,
              onSaved: (val) => confirmNewPassword = val,
            ),
            // new SizedBox(
            //   height: 15,
            // ),
            new Text(
              "Change PIN",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            new SizedBox(
              height: 5,
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 5),
              child: new Text(
                "Old PIN",
                style: TextStyle(fontSize: 14),
              ),
            ),
            new TextFormField(
              controller: txtControllerOldPin,
              keyboardType: TextInputType.number,
              inputFormatters: <TextInputFormatter>[
                WhitelistingTextInputFormatter.digitsOnly,
              ], //
              focusNode: _fourInput,
              obscureText: oldPinVisible,
              maxLength: 6,
              decoration: InputDecoration(
                labelText: 'Enter your old pin here',
                prefixIcon: Icon(Icons.security),
                suffixIcon: IconButton(
                  onPressed: _toggleVisibilityOldPin,
                  icon: oldPinVisible
                      ? Icon(Icons.visibility_off)
                      : Icon(Icons.visibility),
                ),
                contentPadding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30.0)),
              ),
              onEditingComplete: () => //perubahan terbaru
                  FocusScope.of(context).requestFocus(_fiveInput),
              validator: validatePIN,
            ),
            // new SizedBox(
            //   height: 15,
            // ),
            Padding(
              padding: const EdgeInsets.only(bottom: 5),
              child: new Text(
                "New PIN",
                style: TextStyle(fontSize: 14),
              ),
            ),
            new TextFormField(
              controller: txtControllerNewPin,
              keyboardType: TextInputType.number,
              inputFormatters: <TextInputFormatter>[
                WhitelistingTextInputFormatter.digitsOnly,
              ], //
              focusNode: _fiveInput,
              obscureText: newPinVisible,
              maxLength: 6,
              decoration: InputDecoration(
                labelText: 'Enter your new pin here',
                prefixIcon: Icon(Icons.security),
                suffixIcon: IconButton(
                  onPressed: _toggleVisibilityNewPin,
                  icon: newPinVisible
                      ? Icon(Icons.visibility_off)
                      : Icon(Icons.visibility),
                ),
                contentPadding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30.0)),
              ),
              onEditingComplete: () => //perubahan terbaru
                  FocusScope.of(context).requestFocus(_sixInput),
              validator: validatePIN,
              onSaved: (val) => newPIN = val,
            ),
            // new SizedBox(
            //   height: 15,
            // ),
            Padding(
              padding: const EdgeInsets.only(bottom: 5),
              child: new Text(
                "Confirm New PIN",
                style: TextStyle(fontSize: 14),
              ),
            ),
            new TextFormField(
              controller: txtControllerConfirmNewPin,
              keyboardType: TextInputType.number,
              inputFormatters: <TextInputFormatter>[
                WhitelistingTextInputFormatter.digitsOnly,
              ], //
              focusNode: _sixInput,
              obscureText: confirmPinVisible,
              maxLength: 6,
              decoration: InputDecoration(
                labelText: 'Enter confirm your new pin here',
                prefixIcon: Icon(Icons.security),
                suffixIcon: IconButton(
                  onPressed: _toggleVisibilityConfirmPin,
                  icon: confirmPinVisible
                      ? Icon(Icons.visibility_off)
                      : Icon(Icons.visibility),
                ),
                contentPadding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30.0)),
              ),
              validator: validatePIN,
              onSaved: (val) => confirmNewPin = val,
            ),
            // new SizedBox(
            //   height: 15,
            // ),
            new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Text('Keterangan:',
                    style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold),
                   ),
                new Text('* Maksimal kata sandi baru adalah 16 karakter',
                    style: TextStyle(fontSize: 14.0)),
                new Text(
                    '* Minimal kata sandi baru harus sebanyak 6 atau lebih',
                    style: TextStyle(fontSize: 14.0)),
                new Text('* Kata sandi baru harus berisi huruf dan angka',
                    style: TextStyle(fontSize: 14.0)),
                new Text('* PIN baru harus berisikan angka saja',
                    style: TextStyle(fontSize: 14.0)),
                new Text('* PIN baru harus terdiri dari 6 (enam) digit',
                    style: TextStyle(fontSize: 14.0)),
              ],
            ),
            new SizedBox(
              height: 15,
            ),
            new RaisedButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              color: Color(0xff007022),
              onPressed: _submit,
              // onPressed: () {
              //   Navigator.push(
              //       context,
              //       new MaterialPageRoute(
              //           builder: (context) => new LoginPage()));
              // },
              child: new Text(
                "Send",
                style: TextStyle(color: Colors.white, fontSize: 17, fontWeight: FontWeight.bold),
              ),
            ),
            new SizedBox(
              height: 15,
            ),
          ],
        ),
      ),
    );
  }

  _navigateToLogin() async {
    await new Future.delayed(const Duration(seconds: 3));
    if (valueCode == '0') {
      print("Change Password & PIN Success");
      var tciblChangePassPIN =
          await userTCIBL(); // update first last time login
      Navigator.pop(context); //dismiss loader
      Fluttertoast.showToast(
          msg: "Change Password & PIN Successful",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 1,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);
      Navigator.pushReplacement(context,
          new MaterialPageRoute(builder: (context) => new LoginPage()));
      txtControllerPassword.clear();
    } else {
      print("Login Failed");
      Navigator.pop(context); //dismiss loader
      _showdialog("Status: $valueMessage");
    }
  }

  _showdialog(String msg) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: new AlertDialog(
          // title: Text("Pesan"),
          content: new Text(
            msg,
            style: new TextStyle(fontSize: 16.0),
          ),
          actions: <Widget>[
            new FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: new Text("OK"))
          ],
        ));
  }
}
