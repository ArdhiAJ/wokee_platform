import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:wokee_platform/view/home/purchase/mobile_invoice.dart';
import 'package:wokee_platform/view/home/withdrawal/withdrawal_result.dart';

class WithdrawalProcessPage extends StatefulWidget {
  @override
  _WithdrawalProcessPageState createState() => _WithdrawalProcessPageState();
}

class _WithdrawalProcessPageState extends State<WithdrawalProcessPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      // backgroundColor: Colors.red,
      appBar: new PreferredSize(
        preferredSize: Size(double.infinity, 50),
        child: new ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25)),
          child: new AppBar(
            backgroundColor: Color(0xff007022),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: new Center(
                  child: new Text(
                    "Withdrawal",
                    style: TextStyle(color: Colors.white, fontSize: 30),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      body: new ListView(
        children: <Widget>[
          new Card(
            color: Color(0xffF6F6F6),
            elevation: 5,
            margin: const EdgeInsets.fromLTRB(30, 60, 30, 40),
            shape: new RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12)),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 35, 20, 45),
              child: new Column(
                children: <Widget>[
                  new Container(
                    alignment: Alignment.topLeft,
                    child: new Text(
                      "Dari",
                      style: TextStyle(fontSize: 15),
                    ),
                  ),
                  new Card(
                    color: Color(0xffEAEAEA),
                    shape: new RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text(
                            "Muhammad Hamdani",
                            style: TextStyle(fontSize: 13),
                          ),
                          new Text(
                            "890000025395",
                            style: TextStyle(fontSize: 13),
                          ),
                          new Container(
                            alignment: Alignment.topRight,
                            child: new Text(
                              "Rp. 11.050.000",
                              style: TextStyle(fontSize: 22),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                new Text(
                                  "Active Balance",
                                  style: TextStyle(fontSize: 9),
                                ),
                                new Text(
                                  "Rp. 11.050.000",
                                  style: TextStyle(fontSize: 10),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: new Container(
                      alignment: Alignment.topLeft,
                      child: new Text(
                        "Jumlah",
                        style: TextStyle(fontSize: 15),
                      ),
                    ),
                  ),
                  new Container(
                    alignment: Alignment.topRight,
                    child: new Card(
                      color: Color(0xffEAEAEA),
                      shape: new RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(130, 10, 20, 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            new Text(
                              "Rp. 300.000",
                              style: TextStyle(fontSize: 22),
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                  // new Card(
                  //   color: Color(0xffEAEAEA),
                  //   shape: new RoundedRectangleBorder(
                  //       borderRadius: BorderRadius.circular(10)),
                  //   child: Padding(
                  //     padding: const EdgeInsets.fromLTRB(75, 10, 75, 10),
                  //     child: Column(
                  //       crossAxisAlignment: CrossAxisAlignment.end,
                  //       children: <Widget>[
                  //         new Text(
                  //           "Rp. 300.000",
                  //           style: TextStyle(fontSize: 22),
                  //         ),
                  //       ],
                  //     ),
                  //   ),
                  // ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 30, right: 30),
            child: new ButtonTheme(
              height: 40,
              child: new RaisedButton(
                color: Color(0xff2E943E),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(7),
                ),
                onPressed: () {
                  alertPinWithdrawal(context);
                  // Navigator.pushReplacement(
                  //     context,
                  //     new MaterialPageRoute(
                  //         builder: (context) => new WithdrawalResultPage()));
                },
                child: new Text(
                  "Process",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

Future<void> alertPinWithdrawal(BuildContext context) {
  return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(10)),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                new Text("Please enter your PIN",
                    style: TextStyle(color: Color(0xff2E943E), fontSize: 18)),
                new InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: new Icon(Icons.close),
                )
              ],
            ),
            content: new Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new Row(
                  children: <Widget>[
                    new Container(
                      width: 150,
                      child: new TextFormField(
                        style: new TextStyle(fontSize: 20),
                        maxLength: 6,
                        keyboardType: TextInputType.number,
                        inputFormatters: <TextInputFormatter>[
                          WhitelistingTextInputFormatter.digitsOnly,
                        ],
                        decoration: InputDecoration(
                          hintText: 'enter PIN here',
                          contentPadding:
                              EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(15.0)),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 15),
                      child: new ButtonTheme(
                        height: 40,
                        minWidth: 30,
                        child: new RaisedButton(
                          shape: new RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          color: Color(0xff007022),
                          onPressed: () {
                            Navigator.pushReplacement(
                                context,
                                new MaterialPageRoute(
                                    builder: (context) =>
                                        new WithdrawalResultPage()));
                          },
                          child: new Icon(
                            Icons.arrow_forward,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ));
      });
}
