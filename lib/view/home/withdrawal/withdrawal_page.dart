import 'package:flutter/material.dart';
import 'package:wokee_platform/view/home/withdrawal/withdrawal_process.dart';

class WithdrawalPage extends StatefulWidget {
  @override
  _WithdrawalPageState createState() => _WithdrawalPageState();
}

class _WithdrawalPageState extends State<WithdrawalPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      // backgroundColor: Colors.red,
      appBar: new PreferredSize(
        preferredSize: Size(double.infinity, 50),
        child: new ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25)),
          child: new AppBar(
            backgroundColor: Color(0xff007022),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: new Center(
                  child: new Text(
                    "Withdrawal",
                    style: TextStyle(color: Colors.white, fontSize: 30),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      body: new ListView(
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          new Card(
            margin: const EdgeInsets.fromLTRB(35, 10, 35, 10),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            elevation: 5,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text(
                    "Muhammad Hamdani",
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                  ),
                  new Text(
                    "890000025395",
                    style: TextStyle(fontSize: 11),
                  ),
                  new Container(
                      alignment: Alignment.topRight,
                      child: new Text(
                        "Rp. 11.050.000",
                        style: TextStyle(fontSize: 28),
                      )),
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Text(
                        "Active Balance",
                        style: TextStyle(fontSize: 9),
                      ),
                      new Text(
                        "Rp. 11.050.000",
                        style: TextStyle(fontSize: 14),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          new Divider(
            color: Color(0xff007022),
            thickness: 3,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10, bottom: 10),
            child: new Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      new Card(
                        elevation: 5,
                        shape: new RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(20, 15, 20, 15),
                          child: new Text(
                            "Rp. 50.000",
                            style: TextStyle(fontSize: 22),
                          ),
                        ),
                      ),
                      new Card(
                        elevation: 5,
                        shape: new RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(20, 15, 20, 15),
                          child: new Text(
                            "Rp. 100.000",
                            style: TextStyle(fontSize: 22),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      new Card(
                        elevation: 5,
                        shape: new RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(16, 15, 16, 15),
                          child: new Text(
                            "Rp. 300.000",
                            style: TextStyle(fontSize: 22),
                          ),
                        ),
                      ),
                      new Card(
                        elevation: 5,
                        shape: new RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(18, 15, 18, 15),
                          child: new Text(
                            "Rp. 500.000",
                            style: TextStyle(fontSize: 22),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    new Card(
                      elevation: 5,
                      shape: new RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(13, 15, 13, 15),
                        child: new Text(
                          "Rp. 1.000.000",
                          style: TextStyle(fontSize: 22),
                        ),
                      ),
                    ),
                    new GestureDetector(
                      onTap: () {
                        Navigator.push(context, new MaterialPageRoute(builder: (context) => new WithdrawalProcessPage()));
                      },
                      child: new Card(
                        elevation: 5,
                        shape: new RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(13, 17, 13, 17),
                          child: new Text(
                            "Nominal Lainnya",
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
          new Container(
            color: Color(0xff007022),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 3, 20, 3),
              child: new Text(
                "Penarikan belum diambil",
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
            ),
          ),
          new Container(
            height: 180,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
              child: new ListView(
                physics: AlwaysScrollableScrollPhysics(),
                children: <Widget>[
                  new Card(
                    shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text(
                                "6 Februari 2020",
                                style: TextStyle(fontSize: 15),
                              ),
                              new Text(
                                "Kode Penarikan: 571305",
                                style: TextStyle(fontSize: 15),
                              )
                            ],
                          ),
                          new Text(
                            "Rp. 50.000",
                            style: TextStyle(fontSize: 15),
                          )
                        ],
                      ),
                    ),
                  ),
                  new Card(
                    shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text(
                                "6 Februari 2020",
                                style: TextStyle(fontSize: 15),
                              ),
                              new Text(
                                "Kode Penarikan: 500215",
                                style: TextStyle(fontSize: 15),
                              )
                            ],
                          ),
                          new Text(
                            "Rp. 100.000",
                            style: TextStyle(fontSize: 15),
                          )
                        ],
                      ),
                    ),
                  ),
                  new Card(
                    shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text(
                                "6 Februari 2020",
                                style: TextStyle(fontSize: 15),
                              ),
                              new Text(
                                "Kode Penarikan: 398122",
                                style: TextStyle(fontSize: 15),
                              )
                            ],
                          ),
                          new Text(
                            "Rp. 300.000",
                            style: TextStyle(fontSize: 15),
                          )
                        ],
                      ),
                    ),
                  ),
                  new Card(
                    shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text(
                                "6 Februari 2020",
                                style: TextStyle(fontSize: 15),
                              ),
                              new Text(
                                "Kode Penarikan: 398122",
                                style: TextStyle(fontSize: 15),
                              )
                            ],
                          ),
                          new Text(
                            "Rp. 300.000",
                            style: TextStyle(fontSize: 15),
                          )
                        ],
                      ),
                    ),
                  ),
                  new Card(
                    shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text(
                                "6 Februari 2020",
                                style: TextStyle(fontSize: 15),
                              ),
                              new Text(
                                "Kode Penarikan: 398122",
                                style: TextStyle(fontSize: 15),
                              )
                            ],
                          ),
                          new Text(
                            "Rp. 300.000",
                            style: TextStyle(fontSize: 15),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
