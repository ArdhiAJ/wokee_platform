import 'package:flutter/material.dart';
import 'package:wokee_platform/view/home/main_menu.dart';

class WithdrawalResultPage extends StatefulWidget {
  @override
  _WithdrawalResultPageState createState() => _WithdrawalResultPageState();
}

class _WithdrawalResultPageState extends State<WithdrawalResultPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      // backgroundColor: Colors.red,
      appBar: new PreferredSize(
        preferredSize: Size(double.infinity, 50),
        child: new ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25)),
          child: new AppBar(
            backgroundColor: Color(0xff007022),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: new Center(
                  child: new Text(
                    "Withdrawal_Result",
                    style: TextStyle(color: Colors.white, fontSize: 30),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      body: new ListView(
        children: <Widget>[
          new Card(
            color: Color(0xffF6F6F6),
            elevation: 5,
            margin: const EdgeInsets.fromLTRB(30, 20, 30, 20),
            shape: new RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12)),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 5, 20, 45),
              child: new Column(
                children: <Widget>[
                  new Image.asset(
                    "assets/images/struk.png",
                    height: 120,
                    width: 120,
                  ),
                  new Text("Uang Siap Diambil", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                  new Container(
                    alignment: Alignment.topLeft,
                    child: new Text(
                      "Dari",
                      style: TextStyle(fontSize: 15),
                    ),
                  ),
                  new Container(
                    alignment: Alignment.topLeft,
                    child: new Card(
                      color: Color(0xffEAEAEA),
                      shape: new RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(20, 10, 120, 10),
                        child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Text(
                              "Muhammad Hamdani",
                              style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold),
                            ),
                            new Text(
                              "890000025395",
                              style: TextStyle(fontSize: 13),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: new Container(
                      alignment: Alignment.topLeft,
                      child: new Text(
                        "Jumlah",
                        style: TextStyle(fontSize: 15),
                      ),
                    ),
                  ),
                  new Container(
                    alignment: Alignment.topRight,
                    child: new Card(
                      color: Color(0xffEAEAEA),
                      shape: new RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(130, 10, 20, 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            new Text(
                              "Rp. 300.000",
                              style: TextStyle(fontSize: 22),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: new Container(
                      alignment: Alignment.topLeft,
                      child: new Text(
                        "Kode Penarikan",
                        style: TextStyle(fontSize: 15),
                      ),
                    ),
                  ),
                  new Container(
                    alignment: Alignment.topRight,
                    child: new Card(
                      color: Color(0xffEAEAEA),
                      shape: new RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(160, 10, 20, 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            new Text(
                              "571305",
                              style: TextStyle(fontSize: 22),
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 30, right: 30, bottom: 5),
            child: new ButtonTheme(
              height: 40,
              child: new RaisedButton(
                color: Color(0xff2E943E),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(7),
                ),
                onPressed: () {
                  // Navigator.pushReplacement(
                  //     context,
                  //     new MaterialPageRoute(
                  //         builder: (context) => new WithdrawalResultPage()));
                },
                child: new Text(
                  "Process",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 30, right: 30),
            child: new ButtonTheme(
              height: 40,
              child: new RaisedButton(
                color: Color(0xff2E943E),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(7),
                ),
                onPressed: () {
                  Navigator.pushReplacement(
                      context,
                      new MaterialPageRoute(
                          builder: (context) => new MainMenuController()));
                },
                child: new Text(
                  "Selesai",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
