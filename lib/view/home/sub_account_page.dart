import 'package:flutter/material.dart';

class SubAccountPage extends StatefulWidget {
  @override
  _SubAccountPageState createState() => _SubAccountPageState();
}

class _SubAccountPageState extends State<SubAccountPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      // backgroundColor: Colors.red,
      appBar: new PreferredSize(
        preferredSize: Size(double.infinity, 50),
        child: new ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25)),
          child: new AppBar(
            backgroundColor: Color(0xff007022),
            title: new Center(
              child: new Text(
                "Sub Account",
                style: TextStyle(color: Colors.white, fontSize: 30),
              ),
            ),
          ),
        ),
      ),
      body: new ListView(
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          new Card(
            margin: const EdgeInsets.fromLTRB(30, 15, 30, 15),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            elevation: 5,
            child: Padding(
              padding: const EdgeInsets.only(top: 15, bottom: 10),
              child: new Column(
                children: <Widget>[
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: new Image.asset(
                          'assets/images/logo_wokee_home.png',
                          height: 60,
                        ),
                      ),
                      new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text(
                            "Hi, Muhammad Hamdani",
                            style: TextStyle(
                                fontSize: 15, fontWeight: FontWeight.bold),
                          ),
                          new Text(
                            "Terakhir Masuk 14:54 (WIB), 01 Feb 2020",
                            style: TextStyle(fontSize: 12),
                          ),
                          new Text(
                            "Rp. 11.050.000",
                            style: TextStyle(
                                fontSize: 30, fontWeight: FontWeight.bold),
                          )
                        ],
                      )
                    ],
                  ),
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Text(
                        "Active Balance",
                        style: TextStyle(fontSize: 10),
                      ),
                      new Text(
                        "Rp. 11.050.000",
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.bold),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
          new Divider(
            color: Color(0xff007022),
            thickness: 3,
            height: 1,
          ),
          new Container(
            height: 340,
            child: new ListView(
              physics: AlwaysScrollableScrollPhysics(),
              children: <Widget>[
                new Card(
                  elevation: 5,
                  margin: const EdgeInsets.fromLTRB(0, 25, 55, 0),
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(20),
                          bottomRight: Radius.circular(20))),
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Text(
                          "Muhammad Hamdani A",
                          style: TextStyle(
                              fontSize: 15, fontWeight: FontWeight.bold),
                        ),
                        new Text(
                          "890000025395",
                          style: TextStyle(fontSize: 15),
                        ),
                        new Container(
                          alignment: Alignment.bottomRight,
                          child: new Text(
                            "Rp. 11.050.000",
                            style: TextStyle(
                                fontSize: 25, fontWeight: FontWeight.bold),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                new Card(
                  elevation: 5,
                  margin: const EdgeInsets.fromLTRB(0, 15, 55, 10),
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(20),
                          bottomRight: Radius.circular(20))),
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Text(
                          "Hobi",
                          style: TextStyle(
                              fontSize: 15, fontWeight: FontWeight.bold),
                        ),
                        new Text(
                          "890000025396",
                          style: TextStyle(fontSize: 15),
                        ),
                        new Container(
                          alignment: Alignment.bottomRight,
                          child: new Text(
                            "Rp. 1.020,000",
                            style: TextStyle(
                                fontSize: 25, fontWeight: FontWeight.bold),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                new Card(
                  elevation: 5,
                  margin: const EdgeInsets.fromLTRB(0, 15, 55, 10),
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(20),
                          bottomRight: Radius.circular(20))),
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Text(
                          "Pendidikan",
                          style: TextStyle(
                              fontSize: 15, fontWeight: FontWeight.bold),
                        ),
                        new Text(
                          "890000025397",
                          style: TextStyle(fontSize: 15),
                        ),
                        new Container(
                          alignment: Alignment.bottomRight,
                          child: new Text(
                            "Rp. 2.220.000",
                            style: TextStyle(
                                fontSize: 25, fontWeight: FontWeight.bold),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                new Card(
                  elevation: 5,
                  margin: const EdgeInsets.fromLTRB(0, 15, 55, 10),
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(20),
                          bottomRight: Radius.circular(20))),
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Text(
                          "Gadget",
                          style: TextStyle(
                              fontSize: 15, fontWeight: FontWeight.bold),
                        ),
                        new Text(
                          "890000025398",
                          style: TextStyle(fontSize: 15),
                        ),
                        new Container(
                          alignment: Alignment.bottomRight,
                          child: new Text(
                            "Rp. 3.310.000",
                            style: TextStyle(
                                fontSize: 25, fontWeight: FontWeight.bold),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                new Card(
                  elevation: 5,
                  margin: const EdgeInsets.fromLTRB(0, 15, 250, 10),
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(20),
                          bottomRight: Radius.circular(20))),
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Image.asset('assets/images/Add-icon.png', color: Color(0xff007022),)
                      ],
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
