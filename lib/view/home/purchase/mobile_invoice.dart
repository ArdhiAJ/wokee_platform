import 'package:flutter/material.dart';
import 'package:wokee_platform/view/home/main_menu.dart';

class MobileInvoicePage extends StatefulWidget {
  @override
  _MobileInvoicePageState createState() => _MobileInvoicePageState();
}

class _MobileInvoicePageState extends State<MobileInvoicePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      // backgroundColor: Colors.red,
      appBar: new PreferredSize(
        preferredSize: Size(double.infinity, 50),
        child: new ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25)),
          child: new AppBar(
            backgroundColor: Color(0xff007022),
            title: new Center(
              child: new Text(
                "INVOICE",
                style: TextStyle(color: Colors.white, fontSize: 30),
              ),
            ),
          ),
        ),
      ),
      body: new Column(
        children: <Widget>[
          new Expanded(
            child: new Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 25, bottom: 25),
                  child: new Image.asset(
                    'assets/images/checklist.png',
                    height: 100,
                  ),
                ),
                new Container(
                    alignment: Alignment.center,
                    child: new Text(
                      "TRANSACTION SUCCESS",
                      style:
                          TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                    )),
                Padding(
                  padding: const EdgeInsets.only(top: 10, bottom: 10),
                  child: new Container(
                      alignment: Alignment.center,
                      child: new Text(
                        "30 January 2020 1:22 pm",
                        style: TextStyle(fontSize: 15),
                      )),
                ),
                new Container(
                    alignment: Alignment.center,
                    child: new Text(
                      "Transaction ID: 949535h355395395h953",
                      style: TextStyle(fontSize: 15),
                    )),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 20),
            child: new ButtonTheme(
              minWidth: 300.0,
              height: 40,
              child: new RaisedButton(
                color: Color(0xff2E943E),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(7),
                ),
                onPressed: () {
                  Navigator.pushReplacement(
                      context,
                      new MaterialPageRoute(
                          builder: (context) => new MainMenuController()));
                },
                child: new Text(
                  "OK",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
            ),
          ),
          
        ],
      ),
    );
  }
}
