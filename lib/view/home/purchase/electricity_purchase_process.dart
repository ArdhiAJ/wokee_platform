import 'package:flutter/material.dart';
import 'package:wokee_platform/view/home/purchase/electricity_purchase_checkout.dart';

class ElectricityPurchaseProcess extends StatefulWidget {
  @override
  _ElectricityPurchaseProcessState createState() =>
      _ElectricityPurchaseProcessState();
}

class _ElectricityPurchaseProcessState
    extends State<ElectricityPurchaseProcess> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      // backgroundColor: Colors.red,
      appBar: new PreferredSize(
        preferredSize: Size(double.infinity, 50),
        child: new ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25)),
          child: new AppBar(
            backgroundColor: Color(0xff007022),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: new Center(
                  child: new Text(
                    "Electricity",
                    style: TextStyle(color: Colors.white, fontSize: 30),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      body: new ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
            child: new Column(
              children: <Widget>[
                new Card(
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                      side: BorderSide(color: Colors.grey)),
                  child: Padding(
                    padding: const EdgeInsets.all(20),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        new Text(
                          "Muhammad Hamdani",
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                        new Text(
                          "081310661230",
                          style: TextStyle(fontSize: 24),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 25),
            child: new Divider(
              color: Color(0xff007022),
              height: 1,
            ),
          ),
          new Container(
            color: Color(0xffE8E8E8),
            child: new Container(
                alignment: Alignment.center,
                child: Padding(
                  padding: const EdgeInsets.only(top: 5, bottom: 5),
                  child: new Text(
                    "PLN Prepaid",
                    style: TextStyle(fontSize: 23, color: Colors.black),
                  ),
                )),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 25),
            child: new Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      new OutlineButton(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
                          child: new Row(
                            children: <Widget>[
                              new Text(
                                "25",
                                style: TextStyle(
                                    fontSize: 29, fontWeight: FontWeight.bold),
                              ),
                              new Text(
                                ".000",
                                style: TextStyle(
                                    fontSize: 19, fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                        onPressed: () {},
                        shape: new RoundedRectangleBorder(),
                      ),
                      new OutlineButton(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
                          child: new Row(
                            children: <Widget>[
                              new Text(
                                "50",
                                style: TextStyle(
                                    fontSize: 29, fontWeight: FontWeight.bold),
                              ),
                              new Text(
                                ".000",
                                style: TextStyle(
                                    fontSize: 19, fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                        onPressed: () {},
                        shape: new RoundedRectangleBorder(),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      new OutlineButton(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                          child: new Row(
                            children: <Widget>[
                              new Text(
                                "100",
                                style: TextStyle(
                                    fontSize: 29, fontWeight: FontWeight.bold),
                              ),
                              new Text(
                                ".000",
                                style: TextStyle(
                                    fontSize: 19, fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                        onPressed: () {},
                        shape: new RoundedRectangleBorder(),
                      ),
                      new OutlineButton(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                          child: new Row(
                            children: <Widget>[
                              new Text(
                                "150",
                                style: TextStyle(
                                    fontSize: 29, fontWeight: FontWeight.bold),
                              ),
                              new Text(
                                ".000",
                                style: TextStyle(
                                    fontSize: 19, fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                        onPressed: () {},
                        shape: new RoundedRectangleBorder(),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      new OutlineButton(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                          child: new Row(
                            children: <Widget>[
                              new Text(
                                "200",
                                style: TextStyle(
                                    fontSize: 29, fontWeight: FontWeight.bold),
                              ),
                              new Text(
                                ".000",
                                style: TextStyle(
                                    fontSize: 19, fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                        onPressed: () {},
                        shape: new RoundedRectangleBorder(),
                      ),
                      new OutlineButton(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                          child: new Row(
                            children: <Widget>[
                              new Text(
                                "300",
                                style: TextStyle(
                                    fontSize: 29, fontWeight: FontWeight.bold),
                              ),
                              new Text(
                                ".000",
                                style: TextStyle(
                                    fontSize: 19, fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                        onPressed: () {},
                        shape: new RoundedRectangleBorder(),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      new OutlineButton(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                          child: new Row(
                            children: <Widget>[
                              new Text(
                                "500",
                                style: TextStyle(
                                    fontSize: 29, fontWeight: FontWeight.bold),
                              ),
                              new Text(
                                ".000",
                                style: TextStyle(
                                    fontSize: 19, fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                        onPressed: () {},
                        shape: new RoundedRectangleBorder(),
                      ),
                      new OutlineButton(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                          child: new Row(
                            children: <Widget>[
                              new Text(
                                "1.000",
                                style: TextStyle(
                                    fontSize: 29, fontWeight: FontWeight.bold),
                              ),
                              new Text(
                                ".000",
                                style: TextStyle(
                                    fontSize: 19, fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                        onPressed: () {},
                        shape: new RoundedRectangleBorder(),
                      )
                    ],
                  ),
                ),
                new Divider(
                  color: Color(0xff007022),
                  thickness: 3,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 15, bottom: 7),
                  child: new ButtonTheme(
                    minWidth: 200.0,
                    child: new RaisedButton(
                      color: Color(0xff2E943E),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(7),
                      ),
                      onPressed: () {
                        Navigator.push(
                              context,
                              new MaterialPageRoute(
                                  builder: (context) => new ElectricityPurchaseCheckout()));
                      },
                      child: new Text(
                        "Checkout",
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
