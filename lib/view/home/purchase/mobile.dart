import 'package:flutter/material.dart';
import 'package:wokee_platform/view/home/purchase/mobile_payment.dart';

class MobilePage extends StatefulWidget {
  @override
  _MobilePageState createState() => _MobilePageState();
}

class _MobilePageState extends State<MobilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      // backgroundColor: Colors.red,
      appBar: new PreferredSize(
        preferredSize: Size(double.infinity, 50),
        child: new ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25)),
          child: new AppBar(
            backgroundColor: Color(0xff007022),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: new Center(
                  child: new Text(
                    "Mobile",
                    style: TextStyle(color: Colors.white, fontSize: 30),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      body: new ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: new Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 20, bottom: 7),
                  child: new Container(
                    alignment: Alignment.topLeft,
                    child: new Text(
                      "Phone Number",
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                ),
                new Row(
                  children: <Widget>[
                    new Container(
                      width: 250,
                      child: new TextFormField(
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          hintText: '',
                          contentPadding:
                              EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(15.0)),
                        ),
                      ),
                    ),
                    new SizedBox(
                      width: 5,
                    ),
                    new IconButton(
                        iconSize: 30,
                        alignment: Alignment.topCenter,
                        onPressed: () {
                          Navigator.push(
                              context,
                              new MaterialPageRoute(
                                  builder: (context) => new MobilePaymentPage()));
                        },
                        icon: new Image.asset('assets/images/pickcontact.png')),
                  ],
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 25),
            child: new Divider(
              color: Color(0xff007022),
            ),
          )
        ],
      ),
    );
  }
}
