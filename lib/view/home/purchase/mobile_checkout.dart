import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:wokee_platform/view/home/purchase/mobile_invoice.dart';

class MobileCheckoutPage extends StatefulWidget {
  @override
  _MobileCheckoutPageState createState() => _MobileCheckoutPageState();
}

class _MobileCheckoutPageState extends State<MobileCheckoutPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      // backgroundColor: Colors.red,
      appBar: new PreferredSize(
        preferredSize: Size(double.infinity, 50),
        child: new ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25)),
          child: new AppBar(
            backgroundColor: Color(0xff007022),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: new Center(
                  child: new Text(
                    "Checkout",
                    style: TextStyle(color: Colors.white, fontSize: 30),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      body: new ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(20, 35, 20, 35),
            child: new Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15)
              ),
              elevation: 5,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(25, 30, 25, 30),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(bottom: 15),
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text("Phone Number", style: TextStyle(fontSize: 25, color: Colors.black),),
                          new Text("081214994572", style: TextStyle(fontSize: 25, color: Colors.black),)
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 15),
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text("Nominal", style: TextStyle(fontSize: 25, color: Colors.black),),
                          new Text("Rp. 50,000", style: TextStyle(fontSize: 25, color: Colors.black),)
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 15),
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text("Administrasi", style: TextStyle(fontSize: 25, color: Colors.black),),
                          new Text("Rp1,500", style: TextStyle(fontSize: 25, color: Colors.black),)
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 15),
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text("Total", style: TextStyle(fontSize: 25, color: Colors.black),),
                          new Text("Rp. 51,500", style: TextStyle(fontSize: 25, color: Colors.black),)
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 25, right: 25),
            child: new ButtonTheme(
              height: 45,
              child: new RaisedButton(
                color: Color(0xff2E943E),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(7),
                ),
                onPressed: () {
                  alertPinMobile(context);
                  // Navigator.push(
                  //       context,
                  //       new MaterialPageRoute(
                  //           builder: (context) => new MobileInvoicePage()));
                },
                child: new Text(
                  "BAYAR",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

Future<void> alertPinMobile(BuildContext context) {
  return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(10)),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                new Text("Please enter your PIN",
                    style: TextStyle(color: Color(0xff2E943E), fontSize: 18)),
                new InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: new Icon(Icons.close),
                )
              ],
            ),
            content: new Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new Row(
                  children: <Widget>[
                    new Container(
                      width: 150,
                      child: new TextFormField(
                        style: new TextStyle(fontSize: 20),
                        maxLength: 6,
                        keyboardType: TextInputType.number,
                        inputFormatters: <TextInputFormatter>[
                          WhitelistingTextInputFormatter.digitsOnly,
                        ],
                        decoration: InputDecoration(
                          hintText: 'enter PIN here',
                          contentPadding:
                              EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(15.0)),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 15),
                      child: new ButtonTheme(
                        height: 40,
                        minWidth: 30,
                        child: new RaisedButton(
                          shape: new RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          color: Color(0xff007022),
                          onPressed: () {
                            Navigator.pushReplacement(
                                context,
                                new MaterialPageRoute(
                                    builder: (context) =>
                                        new MobileInvoicePage()));
                          },
                          child: new Icon(
                            Icons.arrow_forward,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ));
      });
}