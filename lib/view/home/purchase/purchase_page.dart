import 'package:flutter/material.dart';
import 'package:wokee_platform/view/home/home_page.dart' as home_page;
import 'package:wokee_platform/view/home/purchase/electricity_purchase_page.dart';
import 'package:wokee_platform/view/home/purchase/gopay_page.dart';
import 'package:wokee_platform/view/home/purchase/linkaja_page.dart';
import 'package:wokee_platform/view/home/purchase/mobile.dart';
import 'package:wokee_platform/view/home/purchase/ovo_page.dart';
import 'package:wokee_platform/view/home/sub_account_page.dart'
    as sub_account_page;
import 'package:wokee_platform/view/home/transaction.dart' as transaction_page;
import 'package:wokee_platform/view/home/profile_page.dart' as profile_page;

class PurchasePage extends StatefulWidget {
  @override
  _PurchasePageState createState() => _PurchasePageState();
}

class _PurchasePageState extends State<PurchasePage>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    _tabController = new TabController(vsync: this, length: 4);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      // floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      // floatingActionButton: new SizedBox(
      //   width: 65,
      //   height: 65,
      //   child: new FloatingActionButton(
      //     heroTag: "fab",
      //     backgroundColor: Color(0xff007022),
      //     onPressed: () {},
      //     tooltip: 'FAB',
      //     child: new Column(
      //       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      //       children: <Widget>[
      //         new Image.asset(
      //           'assets/images/QrCode.png',
      //           height: 25,
      //         ),
      //         new Text(
      //           "Pay by\nQR",
      //           style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold),
      //           textAlign: TextAlign.center,
      //         )
      //       ],
      //     ),
      //   ),
      // ),
      appBar: new PreferredSize(
        preferredSize: Size(double.infinity, 50),
        child: new ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25)),
          child: new AppBar(
            backgroundColor: Color(0xff007022),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: new Center(
                  child: new Text(
                    "Purchase",
                    style: TextStyle(color: Colors.white, fontSize: 30),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      body: new ListView(
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 15, bottom: 15),
            child: new Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      new GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              new MaterialPageRoute(
                                  builder: (context) => new MobilePage()));
                        },
                        child: new Card(
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15)),
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(20, 8, 20, 8),
                            child: new Column(
                              children: <Widget>[
                                new Image.asset(
                                  'assets/images/mobile.png',
                                  height: 50,
                                ),
                                new Text(
                                  'Mobile',
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      new GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              new MaterialPageRoute(
                                  builder: (context) => new ElectricityPurchasePage()));
                        },
                        child: new Card(
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15)),
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(15, 8, 15, 8),
                            child: new Column(
                              children: <Widget>[
                                new Image.asset(
                                  'assets/images/pln.png',
                                  height: 50,
                                ),
                                new Text(
                                  'Electricity',
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      new GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              new MaterialPageRoute(
                                  builder: (context) => new GopayPage()));
                        },
                        child: new Card(
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15)),
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(18, 8, 18, 8),
                            child: new Column(
                              children: <Widget>[
                                new Image.asset(
                                  'assets/images/gopay.png',
                                  height: 50,
                                ),
                                new Text(
                                  'Go-Pay',
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                new Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 15),
                      child: new GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              new MaterialPageRoute(
                                  builder: (context) => new OvoPage()));
                        },
                        child: new Card(
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15)),
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(20, 8, 20, 8),
                            child: new Column(
                              children: <Widget>[
                                new Image.asset(
                                  'assets/images/ovo.png',
                                  height: 50,
                                ),
                                new Text(
                                  'Ovo',
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: new GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              new MaterialPageRoute(
                                  builder: (context) => new LinkAjaPage()));
                        },
                        child: new Card(
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15)),
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(20, 8, 20, 8),
                            child: new Column(
                              children: <Widget>[
                                new Image.asset(
                                  'assets/images/linkaja.png',
                                  height: 50,
                                ),
                                new Text(
                                  'LinkAja',
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold),
                                  textAlign: TextAlign.center,
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 10),
            child: new Container(
              color: Color(0xff007022),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                child: new Text(
                  "Recent Transactions",
                  style: TextStyle(
                      fontSize: 20,
                      color: Colors.white,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ),
          new Container(
            height: 250,
            child: new ListView(
              physics: AlwaysScrollableScrollPhysics(),
              children: <Widget>[
                new Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Padding(
                      padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text(
                                "Pulsa Telkomsel",
                                style: TextStyle(fontSize: 15),
                              ),
                              new Text(
                                "081214994572",
                                style: TextStyle(fontSize: 15),
                              )
                            ],
                          ),
                          new Text(
                            "Rp. 25.000",
                            style: TextStyle(fontSize: 15),
                          )
                        ],
                      )),
                ),
                new Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Padding(
                      padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text(
                                "Listrik",
                                style: TextStyle(fontSize: 15),
                              ),
                              new Text(
                                "14265483876",
                                style: TextStyle(fontSize: 15),
                              )
                            ],
                          ),
                          new Text(
                            "Rp. 50.000",
                            style: TextStyle(fontSize: 15),
                          )
                        ],
                      )),
                ),
                new Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Padding(
                      padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text(
                                "Gopay",
                                style: TextStyle(fontSize: 15),
                              ),
                              new Text(
                                "081310661230",
                                style: TextStyle(fontSize: 15),
                              )
                            ],
                          ),
                          new Text(
                            "Rp. 20.000",
                            style: TextStyle(fontSize: 15),
                          )
                        ],
                      )),
                ),
                new Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Padding(
                      padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text(
                                "Ovo",
                                style: TextStyle(fontSize: 15),
                              ),
                              new Text(
                                "081310661230",
                                style: TextStyle(fontSize: 15),
                              )
                            ],
                          ),
                          new Text(
                            "Rp. 20.000",
                            style: TextStyle(fontSize: 15),
                          )
                        ],
                      )),
                ),
              ],
            ),
          ),
          new Divider(
            color: Color(0xff007022),
            thickness: 2,
          ),
          // new TabBarView(
          //   controller: _tabController,
          //   children: <Widget>[
          //     new home_page.HomePage(),
          //     new sub_account_page.SubAccountPage(),
          //     new transaction_page.TransactionPage(),
          //     new profile_page.ProfilePage(),
          //   ],
          // ),
        ],
      ),
      // bottomNavigationBar: new ClipRRect(
      //   borderRadius: BorderRadius.only(
      //       topLeft: Radius.circular(20), topRight: Radius.circular(20)),
      //   child: new Material(
      //     color: Color(0xff007022),
      //     child: new Container(
      //       child: new TabBar(
      //         labelStyle:
      //             new TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
      //         controller: _tabController,
      //         tabs: <Widget>[
      //           new Tab(
      //             icon: new Image.asset(
      //               'assets/images/home.png',
      //               height: 35,
      //             ),
      //             text: "Home",
      //           ),
      //           new Tab(
      //             icon: new Image.asset(
      //               'assets/images/subaccount.png',
      //               height: 35,
      //             ),
      //             text: "SubAccount",
      //           ),
      //           new Tab(
      //             icon: new Image.asset(
      //               'assets/images/transaction.png',
      //               height: 35,
      //             ),
      //             text: "Transaction",
      //           ),
      //           new Tab(
      //               icon: new Image.asset(
      //                 'assets/images/profile.png',
      //                 height: 35,
      //               ),
      //               text: "Profile"),
      //         ],
      //       ),
      //     ),
      //   ),
      // ),
    );
  }
}
