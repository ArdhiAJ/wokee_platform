import 'package:flutter/material.dart';
import 'package:wokee_platform/view/home/purchase/gopay_process.dart';

class GopayPage extends StatefulWidget {
  @override
  _GopayPageState createState() => _GopayPageState();
}

class _GopayPageState extends State<GopayPage>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      // backgroundColor: Colors.red,
      appBar: new PreferredSize(
        preferredSize: Size(double.infinity, 50),
        child: new ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25)),
          child: new AppBar(
            backgroundColor: Color(0xff007022),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: new Center(
                  child: new Text(
                    "Top up Gopay",
                    style: TextStyle(color: Colors.white, fontSize: 30),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      body: new ListView(
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          new SizedBox(
            height: 35,
          ),
          new Divider(
            color: Color(0xff007022),
            thickness: 2,
            height: 1,
          ),
          new SizedBox(
            height: 35,
            child: new Container(
              child: new TabBar(
                labelColor: Colors.black,
                labelStyle: TextStyle(fontSize: 20),
                indicatorColor: Color(0xff007022),
                // indicatorSize: TabBarIndicatorSize.tab,
                // unselectedLabelColor: Colors.black54,
                controller: _tabController,
                tabs: [
                  new Tab(
                    text: "Input",
                  ),
                  new Tab(
                    text: "Favorite",
                  )
                ],
              ),
            ),
          ),
          new Container(
            height: 450,
            // height: 350,
            child: new TabBarView(
              controller: _tabController,
              children: <Widget>[
                new InputPage(),
                new FavoritePage(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class InputPage extends StatefulWidget {
  @override
  _InputPageState createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {
  bool _checkFavorite = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(25, 35, 25, 35),
        child: new ListView(
          children: <Widget>[
            new Text(
              "Nomor Telepon",
              style: TextStyle(color: Colors.black, fontSize: 25),
            ),
            new TextFormField(
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                hintText: '',
                contentPadding:
                    EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0)),
              ),
            ),
            new CheckboxListTile(
              value: _checkFavorite,
              onChanged: (bool value) {
                setState(() {
                  _checkFavorite = value;
                });
              },
              controlAffinity: ListTileControlAffinity.leading,
              title: new Text(
                "Tambahkan ke Favorite",
                style: TextStyle(color: Colors.black, fontSize: 15),
              ),
            ),
            new RaisedButton(
                shape: new RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(7),
                ),
                color: Color(0xff007022),
                onPressed: () {
                  Navigator.push(context, new MaterialPageRoute(builder: (context) => GopayProcessPage()));
                },
                child: new Text(
                  "Selanjutnya",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                )),
          ],
        ),
      ),
    );
  }
}

class FavoritePage extends StatefulWidget {
  @override
  _FavoritePageState createState() => _FavoritePageState();
}

class _FavoritePageState extends State<FavoritePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      body: new ListView(
        children: <Widget>[
          new Card(
            margin: const EdgeInsets.only(top: 15, bottom: 10),
            shape: new RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
              side: BorderSide(color: Colors.grey)
            ),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text(
                    "Muhammad Hamdani",
                    style: TextStyle(color: Colors.black, fontSize: 15),
                  ),
                  new Text(
                    "081310661230",
                    style: TextStyle(color: Colors.black, fontSize: 15),
                  ),
                ],
              ),
            ),
          ),
          new Card(
            margin: const EdgeInsets.only(bottom: 10),
            shape: new RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
              side: BorderSide(color: Colors.grey)
            ),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text(
                    "Muhammad Hamdani",
                    style: TextStyle(color: Colors.black, fontSize: 15),
                  ),
                  new Text(
                    "081310661230",
                    style: TextStyle(color: Colors.black, fontSize: 15),
                  ),
                ],
              ),
            ),
          ),
          new Card(
            margin: const EdgeInsets.only(bottom: 10),
            shape: new RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
              side: BorderSide(color: Colors.grey)
            ),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text(
                    "Muhammad Hamdani",
                    style: TextStyle(color: Colors.black, fontSize: 15),
                  ),
                  new Text(
                    "081310661230",
                    style: TextStyle(color: Colors.black, fontSize: 15),
                  ),
                ],
              ),
            ),
          ),
          new Card(
            margin: const EdgeInsets.only(bottom: 10),
            shape: new RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
              side: BorderSide(color: Colors.grey)
            ),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text(
                    "Muhammad Hamdani",
                    style: TextStyle(color: Colors.black, fontSize: 15),
                  ),
                  new Text(
                    "081310661230",
                    style: TextStyle(color: Colors.black, fontSize: 15),
                  ),
                ],
              ),
            ),
          ),
          new Card(
            margin: const EdgeInsets.only(bottom: 10),
            shape: new RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
              side: BorderSide(color: Colors.grey)
            ),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text(
                    "Muhammad Hamdani",
                    style: TextStyle(color: Colors.black, fontSize: 15),
                  ),
                  new Text(
                    "081310661230",
                    style: TextStyle(color: Colors.black, fontSize: 15),
                  ),
                ],
              ),
            ),
          ),
          new Card(
            margin: const EdgeInsets.only(bottom: 10),
            shape: new RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
              side: BorderSide(color: Colors.grey)
            ),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text(
                    "Muhammad Hamdani",
                    style: TextStyle(color: Colors.black, fontSize: 15),
                  ),
                  new Text(
                    "081310661230",
                    style: TextStyle(color: Colors.black, fontSize: 15),
                  ),
                ],
              ),
            ),
          ),
          new Card(
            margin: const EdgeInsets.only(bottom: 10),
            shape: new RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
              side: BorderSide(color: Colors.grey)
            ),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text(
                    "Muhammad Hamdani",
                    style: TextStyle(color: Colors.black, fontSize: 15),
                  ),
                  new Text(
                    "081310661230",
                    style: TextStyle(color: Colors.black, fontSize: 15),
                  ),
                ],
              ),
            ),
          ),
          new Card(
            margin: const EdgeInsets.only(bottom: 10),
            shape: new RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
              side: BorderSide(color: Colors.grey)
            ),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text(
                    "Muhammad Hamdani",
                    style: TextStyle(color: Colors.black, fontSize: 15),
                  ),
                  new Text(
                    "081310661230",
                    style: TextStyle(color: Colors.black, fontSize: 15),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
