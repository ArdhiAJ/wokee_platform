import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:wokee_platform/service/homepage/logout_service.dart';
import 'package:wokee_platform/view/login/change_password&PIN.dart';
import 'package:wokee_platform/view/login/forget_password.dart';
import 'package:wokee_platform/view/login/login_page.dart';
import 'package:wokee_platform/view/model/color_loader_2.dart';
import 'package:wokee_platform/view/model/model.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      appBar: new PreferredSize(
        preferredSize: Size(double.infinity, 50),
        child: new ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25)),
          child: new AppBar(
            backgroundColor: Color(0xff007022),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: new Center(
                  child: new Text(
                    "Profile",
                    style: TextStyle(color: Colors.white, fontSize: 30),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      body: new ListView(
        children: <Widget>[
          new ListTile(
            leading: new Icon(Icons.home),
            title: new Text("Home"),
          ),
          new ListTile(
            leading: new Icon(Icons.attach_money),
            title: new Text("Account Information"),
          ),
          new ListTile(
            leading: new Icon(Icons.transfer_within_a_station),
            title: new Text("Transfer"),
          ),
          new ListTile(
            leading: new Icon(Icons.add_shopping_cart),
            title: new Text("Payment"),
          ),
          new ListTile(
            leading: new Icon(Icons.shopping_cart),
            title: new Text("Purchase"),
          ),
          new ListTile(
            leading: new Icon(Icons.scanner),
            title: new Text("QR Pay"),
          ),
          new ListTile(
            leading: new Icon(Icons.book),
            title: new Text("Account Opening"),
          ),
          new ListTile(
            leading: new Icon(Icons.message),
            title: new Text("Inbox"),
          ),
          new Divider(
            color: Colors.black45,
            indent: 16,
          ),
          new ListTile(
            leading: new Icon(Icons.home),
            title: new Text("Home"),
          ),
          new ListTile(
            leading: new Icon(Icons.attach_money),
            title: new Text("Account Information"),
          ),
          new ListTile(
            leading: new Icon(Icons.transfer_within_a_station),
            title: new Text("Transfer"),
          ),
          new ListTile(
            leading: new Icon(Icons.add_shopping_cart),
            title: new Text("Payment"),
          ),
          new ListTile(
            leading: new Icon(Icons.shopping_cart),
            title: new Text("Purchase"),
          ),
          new GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new ChangePasswordPINPage()));
            },
            child: new ListTile(
              leading: new Icon(Icons.scanner),
              title: new Text("Change Password & PIN"),
            ),
          ),
          new GestureDetector(
            onTap: () {
              Navigator.push (
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new ForgetPasswordPage()));
            },
            child: new ListTile(
              leading: new Icon(Icons.book),
              title: new Text("Reset Password"),
            ),
          ),
          new GestureDetector(
            onTap: () async {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return Center(
                      child: ColorLoader2(),
                    );
                  });
              new Future.delayed(new Duration(seconds: 0), () async {
                await logoutUser();
                _nextLogout();
                // Navigator.pushReplacement(context,
                // new MaterialPageRoute(builder: (context) => new MyLoginPage(new BasicAppPresenter())));
              });
            },
            child: new ListTile(
              leading: new Icon(Icons.message),
              title: new Text("Exit Application"),
            ),
          ),
          new Divider(
            color: Colors.black45,
            indent: 16,
          ),
        ],
      ),
    );
  }

  _nextLogout() async {
    await new Future.delayed(const Duration(seconds: 3));
    if (valueCode == '0') {
      print("Logout User Success!");
      Navigator.pop(context);
      Fluttertoast.showToast(
          msg: "Logout Successfull",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 1,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);
      // txtControllerPassword.clear();
      Navigator.pushReplacement(context,
          new MaterialPageRoute(builder: (context) => new LoginPage()));
      txtControllerPassword.clear();
    } else {
      print("Logout User Failed!");
      Navigator.pop(context);
      _showdialog("Message: $valueMessage");
    }
  }

  _showdialog(String msg) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: new AlertDialog(
          // title: Text("Pesan"),
          content: new Text(
            msg,
            style: new TextStyle(fontSize: 16.0),
          ),
          actions: <Widget>[
            new FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                  txtControllerEmail.clear();
                  txtControllerPassword.clear();
                },
                child: new Text("OK"))
          ],
        ));
  }
}
