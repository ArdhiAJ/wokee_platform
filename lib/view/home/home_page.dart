import 'package:flutter/material.dart';
import 'package:wokee_platform/view/home/move_money/move_money_page.dart';
import 'package:wokee_platform/view/home/payment/payment_page.dart';
import 'package:wokee_platform/view/home/purchase/purchase_page.dart';
import 'package:wokee_platform/view/home/send_money/send_money_page.dart';
import 'package:wokee_platform/view/home/split_bill/split_bill_page.dart';
import 'package:wokee_platform/view/home/withdrawal/withdrawal_page.dart';
import 'package:wokee_platform/view/model/model.dart';
import 'package:wokee_platform/service/account/query_acc_details.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

class HomePage extends StatefulWidget {
  SavingAccountHomepage svAcc;

  HomePage({Key key, this.svAcc});

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  @override
  void initState() {
    super.initState();
    setState(() {
      queryAccDetails();
    });
  }

  @override
  Widget build(BuildContext context) {
    // var idrCurrentBalance = NumberFormat.simpleCurrency(locale: 'id')
    //     .format(double.parse(valueCurrentBalance));
    // var idrAvailableBalance = NumberFormat.simpleCurrency(locale: 'id')
    //     .format(double.parse(valueAvailableBalance));
    // print("Current Balance: $idrCurrentBalance");
    // print("Available Balance: $idrAvailableBalance");
    // print(accAvailableBalance);
    // print(accCurrentBalance);
    print("Saving Account(Menu Homepage) :${widget.svAcc.savingAc}");
    print("Saving Account(Menu Homepage) :${widget.svAcc.lastTimeLogin}");
    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      // backgroundColor: Colors.red,
      appBar: new PreferredSize(
        preferredSize: Size(double.infinity, 50),
        child: new ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25)),
          child: new AppBar(
            backgroundColor: Color(0xff007022),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: new Center(
                  child: new Text(
                    "Home",
                    style: TextStyle(color: Colors.white, fontSize: 30),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      // drawer: new Theme(
      //   data: Theme.of(context).copyWith(
      //     canvasColor: Color(0xFF219233),
      //   ),
      //   child: Padding(
      //     padding: const EdgeInsets.only(right: 110),
      //     child: new Drawer(
      //       child: new ListView(
      //         children: <Widget>[
      //           new SizedBox(
      //             height: 30,
      //           ),
      //           new Row(
      //             children: <Widget>[
      //               Padding(
      //                 padding: const EdgeInsets.only(bottom: 15),
      //                 child: new Image.asset(
      //                   'assets/images/logo_wokee.png',
      //                   height: 90,
      //                 ),
      //               ),
      //               new InkWell(
      //                 onTap: () {
      //                   Navigator.pop(context);
      //                 },
      //                 child: new Icon(
      //                   Icons.navigate_before,
      //                   color: Colors.white,
      //                   size: 40,
      //                 ),
      //               ),
      //             ],
      //           ),
      //           Padding(
      //             padding: const EdgeInsets.only(bottom: 5),
      //             child: new Card(
      //               margin: const EdgeInsets.only(right: 50),
      //               shape: RoundedRectangleBorder(
      //                   borderRadius: BorderRadius.only(
      //                       topRight: Radius.circular(5),
      //                       bottomRight: Radius.circular(5))),
      //               child: Padding(
      //                 padding: const EdgeInsets.fromLTRB(50, 5, 30, 5),
      //                 child: new Text(
      //                   "Pengguna",
      //                   style: TextStyle(
      //                       fontSize: 22, fontWeight: FontWeight.bold),
      //                 ),
      //               ),
      //             ),
      //           ),
      //           Padding(
      //             padding: const EdgeInsets.only(bottom: 5),
      //             child: new Card(
      //               margin: const EdgeInsets.only(right: 50),
      //               shape: RoundedRectangleBorder(
      //                   borderRadius: BorderRadius.only(
      //                       topRight: Radius.circular(5),
      //                       bottomRight: Radius.circular(5))),
      //               child: Padding(
      //                 padding: const EdgeInsets.fromLTRB(50, 5, 30, 5),
      //                 child: new Text(
      //                   "Pesan",
      //                   style: TextStyle(
      //                       fontSize: 22, fontWeight: FontWeight.bold),
      //                 ),
      //               ),
      //             ),
      //           ),
      //           Padding(
      //             padding: const EdgeInsets.only(bottom: 5),
      //             child: new Card(
      //               margin: const EdgeInsets.only(right: 50),
      //               shape: RoundedRectangleBorder(
      //                   borderRadius: BorderRadius.only(
      //                       topRight: Radius.circular(5),
      //                       bottomRight: Radius.circular(5))),
      //               child: Padding(
      //                 padding: const EdgeInsets.fromLTRB(50, 5, 30, 5),
      //                 child: new Text(
      //                   "Bantuan",
      //                   style: TextStyle(
      //                       fontSize: 22, fontWeight: FontWeight.bold),
      //                 ),
      //               ),
      //             ),
      //           ),
      //           Padding(
      //             padding: const EdgeInsets.only(bottom: 5),
      //             child: new Card(
      //               margin: const EdgeInsets.only(right: 50),
      //               shape: RoundedRectangleBorder(
      //                   borderRadius: BorderRadius.only(
      //                       topRight: Radius.circular(5),
      //                       bottomRight: Radius.circular(5))),
      //               child: Padding(
      //                 padding: const EdgeInsets.fromLTRB(50, 5, 30, 5),
      //                 child: new Text(
      //                   "Link Cepat",
      //                   style: TextStyle(
      //                       fontSize: 22, fontWeight: FontWeight.bold),
      //                 ),
      //               ),
      //             ),
      //           ),
      //           Padding(
      //             padding: const EdgeInsets.only(bottom: 5),
      //             child: new Card(
      //               margin: const EdgeInsets.only(right: 50),
      //               shape: RoundedRectangleBorder(
      //                   borderRadius: BorderRadius.only(
      //                       topRight: Radius.circular(5),
      //                       bottomRight: Radius.circular(5))),
      //               child: Padding(
      //                 padding: const EdgeInsets.fromLTRB(50, 5, 30, 5),
      //                 child: new Text(
      //                   "Pengaturan",
      //                   style: TextStyle(
      //                       fontSize: 22, fontWeight: FontWeight.bold),
      //                 ),
      //               ),
      //             ),
      //           ),
      //         ],
      //       ),
      //     ),
      //   ),
      // ),
      body: new ListView(
        children: <Widget>[
          new Card(
            margin: const EdgeInsets.fromLTRB(30, 15, 30, 15),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            elevation: 5,
            child: Padding(
              padding: const EdgeInsets.only(top: 15, bottom: 10),
              child: new Column(
                children: <Widget>[
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: new Image.asset(
                          'assets/images/logo_wokee_home.png',
                          height: 60,
                        ),
                      ),
                      new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text(
                            "Hi, $valueNickname",
                            style: TextStyle(
                                fontSize: 15, fontWeight: FontWeight.bold),
                          ),
                          new Text(
                            "Last Time Login: ${widget.svAcc.lastTimeLogin}",
                            style: TextStyle(fontSize: 12),
                          ),
                          new Text(
                            // "$idrCurrentBalance",
                            "Rp $valueCurrentBalance",
                            style: TextStyle(
                                fontSize: 30, fontWeight: FontWeight.bold),
                          )
                        ],
                      )
                    ],
                  ),
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Text(
                        "Active Balance ",
                        style: TextStyle(fontSize: 10),
                      ),
                      new Text(
                        // "$idrAvailableBalance",
                        "Rp $valueAvailableBalance",
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.bold),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
          new Container(
            color: Color(0xff007022),
            child: Padding(
              padding: const EdgeInsets.only(top: 10, bottom: 10),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  new GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (context) => new MoveMoneyPage()));
                    },
                    child: new Card(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15)),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: new Column(
                          children: <Widget>[
                            new Image.asset(
                              'assets/images/movemoney.png',
                              height: 50,
                            ),
                            new Text(
                              'Move Money',
                              style: TextStyle(
                                  fontSize: 12, fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  new GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (context) => new WithdrawalPage()));
                    },
                    child: new Card(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15)),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(9, 8, 10, 9),
                        child: new Column(
                          children: <Widget>[
                            new Image.asset(
                              'assets/images/withdrawal.png',
                              height: 60,
                            ),
                            new Text(
                              'Withdrawal',
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  new GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            new MaterialPageRoute(
                                builder: (context) => new SendMoneyPage()));
                      },
                      child: new Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15)),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: new Column(
                            children: <Widget>[
                              new Image.asset(
                                'assets/images/sendmoney.png',
                                height: 50,
                              ),
                              new Text(
                                'Send Money',
                                style: TextStyle(
                                    fontSize: 12, fontWeight: FontWeight.bold),
                              )
                            ],
                          ),
                        ),
                      ))
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 15),
            child: new Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(bottom: 15),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      new GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              new MaterialPageRoute(
                                  builder: (context) => new PurchasePage()));
                        },
                        child: new Card(
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15)),
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(20, 8, 20, 8),
                            child: new Column(
                              children: <Widget>[
                                new Image.asset(
                                  'assets/images/purchase.png',
                                  height: 50,
                                ),
                                new Text(
                                  'Purchase',
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      new GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              new MaterialPageRoute(
                                  builder: (context) => new PaymentPage()));
                        },
                        child: new Card(
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15)),
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(20, 8, 20, 8),
                            child: new Column(
                              children: <Widget>[
                                new Image.asset(
                                  'assets/images/payment.png',
                                  height: 50,
                                ),
                                new Text(
                                  'Payment',
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      new Card(
                        elevation: 5,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15)),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: new Column(
                            children: <Widget>[
                              new Image.asset(
                                'assets/images/paycontact.png',
                                height: 50,
                              ),
                              new Text(
                                'Pay Contacts',
                                style: TextStyle(
                                    fontSize: 15, fontWeight: FontWeight.bold),
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                new Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 15),
                      child: new GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              new MaterialPageRoute(
                                  builder: (context) => new SplitBillPage()));
                        },
                        child: new Card(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15)),
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(20, 8, 20, 8),
                            child: new Column(
                              children: <Widget>[
                                new Image.asset(
                                  'assets/images/splitbill.png',
                                  height: 50,
                                ),
                                new Text(
                                  'Split Bill',
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 15),
                      child: new Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15)),
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                          child: new Column(
                            children: <Widget>[
                              new Image.asset(
                                'assets/images/managepayee.png',
                                height: 50,
                              ),
                              new Text(
                                'Manage\nPayee',
                                style: TextStyle(
                                    fontSize: 15, fontWeight: FontWeight.bold),
                                textAlign: TextAlign.center,
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  var valueStatus = "";
  var valueNickname = "";
  var valueAcctNumber = "";
  var valueCurrentBalance = "";
  var valueAvailableBalance = "";

  var valueStatusFailed = "";

  Future queryAccDetails() async {
    print("-->User Input (Service)<--");
    // print("NIK: ${txtControllerEktp.text}");
    // print("Date Of Birth: ${txtControllerEktp.text}");

    var url = "http://10.2.62.35:8080/wokee/okoce/queryaccountdetail";
    print("URL: $url");

    Map dataBody = {
      "company": "",
      "username": "DFW17009",
      "password": "123123",
      "columnName": "@ID",
      "criteriaValue": "${widget.svAcc.savingAc}",
      // "criteriaValue": "890000000591",
      "operand": "EQ"
    };

    var body = json.encode(dataBody);

    final response = await http.post(url,
        headers: {"Content-Type": "application/json"}, body: body);

    final int statusCode = response.statusCode;

    if (statusCode == 200) {
      print("Connection Success!");
      print("Status Code: $statusCode");
      var result = json.decode(response.body);
      var result1 = result['ATIQueryAccountDetailsResponse']['Status']
          ['successIndicator'];
      var result2 = result['ATIQueryAccountDetailsResponse']
              ['ATIENECACCTDETAILSType']['gATIENECACCTDETAILSDetailType']
          ['mATIENECACCTDETAILSDetailType']['AcctNumber'];
      var result3 = result['ATIQueryAccountDetailsResponse']
              ['ATIENECACCTDETAILSType']['gATIENECACCTDETAILSDetailType']
          ['mATIENECACCTDETAILSDetailType']['Nickname'];
      var result4 = result['ATIQueryAccountDetailsResponse']
              ['ATIENECACCTDETAILSType']['gATIENECACCTDETAILSDetailType']
          ['mATIENECACCTDETAILSDetailType']['CurrentBalance'];
      var result5 = result['ATIQueryAccountDetailsResponse']
              ['ATIENECACCTDETAILSType']['gATIENECACCTDETAILSDetailType']
          ['mATIENECACCTDETAILSDetailType']['AvailableBalance'];

      final formatter = new NumberFormat("#,##0.00", "id");
      var accAvailableBalance = formatter.format(result4);
      var accCurrentBalance = formatter.format(result5);

      setState(() {
        valueStatus = result1.toString();
        valueAcctNumber = result2.toString();
        valueNickname = result3.toString();
        // valueCurrentBalance = result4.toString();
        // valueAvailableBalance = result5.toString();
        valueCurrentBalance = accAvailableBalance.toString();
        valueAvailableBalance = accCurrentBalance.toString();
      });

      print("Status: $valueStatus");
      print("Account Number : $valueAcctNumber");
      print("Nickname: $valueNickname");
      print("Current Balance: $valueCurrentBalance");
      print("Available Balance: $valueAvailableBalance");

      return valueStatus +
          valueAcctNumber +
          valueNickname +
          valueCurrentBalance +
          valueAvailableBalance;
    } else {
      print("Connection Failed!");
      print("Status Code Failed: $statusCode");
      var result = json.decode(response.body);
      var failed1 = result['ATIQueryAccountDetailsResponse']['Status']
          ['successIndicator'];
      valueStatusFailed = failed1.toString();
      return valueStatusFailed;
      // throw new Exception("Error while fetching data");
    }
  }
}
