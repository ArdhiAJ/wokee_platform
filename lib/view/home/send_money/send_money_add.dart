import 'package:flutter/material.dart';
import 'package:wokee_platform/view/home/send_money/send_money_page.dart';
import 'package:wokee_platform/view/home/send_money/send_money_process.dart';

class SendMoneyAddPage extends StatefulWidget {
  @override
  _SendMoneyAddPageState createState() => _SendMoneyAddPageState();
}

class _SendMoneyAddPageState extends State<SendMoneyAddPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      appBar: new PreferredSize(
        preferredSize: Size(double.infinity, 50),
        child: new ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25)),
          child: new AppBar(
            backgroundColor: Color(0xff007022),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: new Center(
                  child: new Text(
                    "Pilih Penerima",
                    style: TextStyle(color: Colors.white, fontSize: 30),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      body: new ListView(
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(80, 15, 80, 15),
            child: new RaisedButton(
                shape: new RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(7),
                ),
                color: Color(0xff007022),
                onPressed: () {
                  // Navigator.push(
                  //     context,
                  //     new MaterialPageRoute(
                  //         builder: (context) => SendMoneyAddPage()));
                },
                child: new Text(
                  "Tambah Penerima",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                )),
          ),
          new Divider(
            color: Color(0xff007022),
            thickness: 3,
            height: 1,
          ),
          new Container(
            height: 450,
            child: new ListView(
              physics: AlwaysScrollableScrollPhysics(),
              children: <Widget>[
                new GestureDetector(
                  onTap: () {
                    Navigator.push(context, new MaterialPageRoute(builder: (context) => new SendMoneyProcessPage()));
                  },
                  child: new Card(
                    margin: const EdgeInsets.all(15),
                    elevation: 5,
                    shape: new RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12)),
                    child: Padding(
                      padding: const EdgeInsets.all(15),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          new Text(
                            "Muhammad Hamdani",
                            style: TextStyle(
                                fontSize: 17, fontWeight: FontWeight.bold),
                          ),
                          new Text(
                            "4202015190",
                            style: TextStyle(fontSize: 17),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                new Card(
                  margin:
                      const EdgeInsets.only(left: 15, right: 15, bottom: 15),
                  elevation: 5,
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Text(
                          "Muhammad Hamdani",
                          style: TextStyle(
                              fontSize: 17, fontWeight: FontWeight.bold),
                        ),
                        new Text(
                          "4202015190",
                          style: TextStyle(fontSize: 17),
                        )
                      ],
                    ),
                  ),
                ),
                new Card(
                  margin:
                      const EdgeInsets.only(left: 15, right: 15, bottom: 15),
                  elevation: 5,
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Text(
                          "Muhammad Hamdani",
                          style: TextStyle(
                              fontSize: 17, fontWeight: FontWeight.bold),
                        ),
                        new Text(
                          "4202015190",
                          style: TextStyle(fontSize: 17),
                        )
                      ],
                    ),
                  ),
                ),
                new Card(
                  margin:
                      const EdgeInsets.only(left: 15, right: 15, bottom: 15),
                  elevation: 5,
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Text(
                          "Muhammad Hamdani",
                          style: TextStyle(
                              fontSize: 17, fontWeight: FontWeight.bold),
                        ),
                        new Text(
                          "4202015190",
                          style: TextStyle(fontSize: 17),
                        )
                      ],
                    ),
                  ),
                ),
                new Card(
                  margin:
                      const EdgeInsets.only(left: 15, right: 15, bottom: 15),
                  elevation: 5,
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Text(
                          "Muhammad Hamdani",
                          style: TextStyle(
                              fontSize: 17, fontWeight: FontWeight.bold),
                        ),
                        new Text(
                          "4202015190",
                          style: TextStyle(fontSize: 17),
                        )
                      ],
                    ),
                  ),
                ),
                new Card(
                  margin:
                      const EdgeInsets.only(left: 15, right: 15, bottom: 15),
                  elevation: 5,
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Text(
                          "Muhammad Hamdani",
                          style: TextStyle(
                              fontSize: 17, fontWeight: FontWeight.bold),
                        ),
                        new Text(
                          "4202015190",
                          style: TextStyle(fontSize: 17),
                        )
                      ],
                    ),
                  ),
                ),
                new Card(
                  margin:
                      const EdgeInsets.only(left: 15, right: 15, bottom: 15),
                  elevation: 5,
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Text(
                          "Muhammad Hamdani",
                          style: TextStyle(
                              fontSize: 17, fontWeight: FontWeight.bold),
                        ),
                        new Text(
                          "4202015190",
                          style: TextStyle(fontSize: 17),
                        )
                      ],
                    ),
                  ),
                ),
                new Card(
                  margin:
                      const EdgeInsets.only(left: 15, right: 15, bottom: 15),
                  elevation: 5,
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Text(
                          "Muhammad Hamdani",
                          style: TextStyle(
                              fontSize: 17, fontWeight: FontWeight.bold),
                        ),
                        new Text(
                          "4202015190",
                          style: TextStyle(fontSize: 17),
                        )
                      ],
                    ),
                  ),
                ),
                new Card(
                  margin:
                      const EdgeInsets.only(left: 15, right: 15, bottom: 15),
                  elevation: 5,
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Text(
                          "Muhammad Hamdani",
                          style: TextStyle(
                              fontSize: 17, fontWeight: FontWeight.bold),
                        ),
                        new Text(
                          "4202015190",
                          style: TextStyle(fontSize: 17),
                        )
                      ],
                    ),
                  ),
                ),
                new Card(
                  margin:
                      const EdgeInsets.only(left: 15, right: 15, bottom: 15),
                  elevation: 5,
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Text(
                          "Muhammad Hamdani",
                          style: TextStyle(
                              fontSize: 17, fontWeight: FontWeight.bold),
                        ),
                        new Text(
                          "4202015190",
                          style: TextStyle(fontSize: 17),
                        )
                      ],
                    ),
                  ),
                ),
                new Card(
                  margin:
                      const EdgeInsets.only(left: 15, right: 15, bottom: 15),
                  elevation: 5,
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Text(
                          "Muhammad Hamdani",
                          style: TextStyle(
                              fontSize: 17, fontWeight: FontWeight.bold),
                        ),
                        new Text(
                          "4202015190",
                          style: TextStyle(fontSize: 17),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          new Divider(
            color: Color(0xff007022),
            thickness: 3,
            height: 1,
          ),
        ],
      ),
    );
  }
}
