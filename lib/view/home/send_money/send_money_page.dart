import 'dart:math';

import 'package:flutter/material.dart';
import 'package:wokee_platform/view/home/send_money/send_money_add.dart';

class SendMoneyPage extends StatefulWidget {
  @override
  _SendMoneyPageState createState() => _SendMoneyPageState();
}

class _SendMoneyPageState extends State<SendMoneyPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      appBar: new PreferredSize(
        preferredSize: Size(double.infinity, 50),
        child: new ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25)),
          child: new AppBar(
            backgroundColor: Color(0xff007022),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: new Center(
                  child: new Text(
                    "Send Money",
                    style: TextStyle(color: Colors.white, fontSize: 30),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      body: new ListView(
        children: <Widget>[
          new Card(
            elevation: 5,
            margin: const EdgeInsets.fromLTRB(30, 15, 30, 15),
            shape: new RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Padding(
              padding: const EdgeInsets.only(top: 15, bottom: 15),
              child: new Column(
                children: <Widget>[
                  new Text(
                    "Muhammad Hamdani",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 10),
                    child: new Text(
                      "890000025395",
                      style: TextStyle(fontSize: 17),
                    ),
                  ),
                  new Text(
                    "Rp. 11.050.000",
                    style: TextStyle(fontSize: 30),
                  )
                ],
              ),
            ),
          ),
          new Transform.rotate(
              angle: pi / 2,
              child: new Icon(
                Icons.navigate_next,
                size: 50,
              )),
          new Divider(
            color: Color(0xff007022),
            thickness: 3,
            height: 1,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15, top: 15, bottom: 15),
            child: new Text(
              "Penerima",
              style: TextStyle(fontSize: 20),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 80, right: 80, bottom: 20),
            child: new RaisedButton(
                shape: new RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(7),
                ),
                color: Color(0xff007022),
                onPressed: () {
                  Navigator.push(context, new MaterialPageRoute(builder: (context) => SendMoneyAddPage()));
                },
                child: new Text(
                  "Pilih Penerima",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                )),
          ),
          new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 15),
                child: new Text(
                  "Jumlah",
                  style: TextStyle(fontSize: 20),
                ),
              ),
              new Row(
                children: <Widget>[
                  new Text(
                    "Rp.",
                    style: TextStyle(fontSize: 20),
                  ),
                  new SizedBox(
                    width: 5,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 30),
                    child: new Container(
                      width: 120,
                      child: new TextFormField(
                        keyboardType: TextInputType.number,
                        style: new TextStyle(fontSize: 25),
                        decoration: InputDecoration(
                          hintText: '40.000',
                          contentPadding: new EdgeInsets.symmetric(
                              horizontal: 10.0, vertical: 2),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15, top: 10, bottom: 50),
            child: new Row(
              children: <Widget>[
                new Text(
                  "Keterangan",
                  style: TextStyle(fontSize: 20),
                ),
                new SizedBox(
                  width: 30,
                ),
                new Container(
                  width: 200,
                  child: new TextFormField(
                    keyboardType: TextInputType.number,
                    style: new TextStyle(fontSize: 25),
                    decoration: InputDecoration(
                      hintText: '',
                      contentPadding: new EdgeInsets.symmetric(
                          horizontal: 10.0, vertical: 2),
                    ),
                  ),
                ),
              ],
            ),
          ),
          new Divider(
            color: Color(0xff007022),
            thickness: 3,
            height: 1,
          ),
        ],
      ),
    );
  }
}
