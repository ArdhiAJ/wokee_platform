import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:wokee_platform/view/home/payment/mobile_checkout2.dart';
import 'package:wokee_platform/view/home/purchase/mobile_invoice.dart';

class SendMoneyProcessPage extends StatefulWidget {
  @override
  _SendMoneyProcessPageState createState() => _SendMoneyProcessPageState();
}

class _SendMoneyProcessPageState extends State<SendMoneyProcessPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      appBar: new PreferredSize(
        preferredSize: Size(double.infinity, 50),
        child: new ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25)),
          child: new AppBar(
            backgroundColor: Color(0xff007022),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: new Center(
                  child: new Text(
                    "Send Money",
                    style: TextStyle(color: Colors.white, fontSize: 30),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      body: new ListView(
        children: <Widget>[
          new Card(
            elevation: 5,
            margin: const EdgeInsets.fromLTRB(30, 15, 30, 15),
            shape: new RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Padding(
              padding: const EdgeInsets.only(top: 15, bottom: 15),
              child: new Column(
                children: <Widget>[
                  new Text(
                    "Muhammad Hamdani",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 10),
                    child: new Text(
                      "890000025395",
                      style: TextStyle(fontSize: 17),
                    ),
                  ),
                  new Text(
                    "Rp. 11.050.000",
                    style: TextStyle(fontSize: 30),
                  )
                ],
              ),
            ),
          ),
          new Transform.rotate(
              angle: pi / 2,
              child: new Icon(
                Icons.navigate_next,
                size: 50,
              )),
          new Divider(
            color: Color(0xff007022),
            thickness: 3,
            height: 1,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15, top: 15, bottom: 15),
            child: new Text(
              "Penerima",
              style: TextStyle(fontSize: 20),
            ),
          ),
          new Card(
            margin: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            elevation: 5,
            shape: new RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12)),
            child: Padding(
              padding: const EdgeInsets.all(15),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Text(
                    "Muhammad Hamdani",
                    style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
                  ),
                  new Text(
                    "4202015190",
                    style: TextStyle(fontSize: 17),
                  )
                ],
              ),
            ),
          ),
          // Padding(
          //   padding: const EdgeInsets.only(left: 80, right: 80, bottom: 20),
          //   child: new RaisedButton(
          //       shape: new RoundedRectangleBorder(
          //         borderRadius: BorderRadius.circular(7),
          //       ),
          //       color: Color(0xff007022),
          //       onPressed: () {
          //         Navigator.push(
          //             context,
          //             new MaterialPageRoute(
          //                 builder: (context) => SendMoneyAddPage()));
          //       },
          //       child: new Text(
          //         "Pilih Penerima",
          //         style: TextStyle(color: Colors.white, fontSize: 20),
          //       )),
          // ),
          new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 15),
                child: new Text(
                  "Jumlah",
                  style: TextStyle(fontSize: 20),
                ),
              ),
              new Row(
                children: <Widget>[
                  new Text(
                    "Rp.",
                    style: TextStyle(fontSize: 20),
                  ),
                  new SizedBox(
                    width: 5,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 30),
                    child: new Container(
                      width: 120,
                      child: new TextFormField(
                        keyboardType: TextInputType.number,
                        style: new TextStyle(fontSize: 25),
                        decoration: InputDecoration(
                          hintText: '40.000',
                          contentPadding: new EdgeInsets.symmetric(
                              horizontal: 10.0, vertical: 2),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15, top: 10, bottom: 50),
            child: new Row(
              children: <Widget>[
                new Text(
                  "Keterangan",
                  style: TextStyle(fontSize: 20),
                ),
                new SizedBox(
                  width: 30,
                ),
                new Container(
                  width: 200,
                  child: new TextFormField(
                    keyboardType: TextInputType.number,
                    style: new TextStyle(fontSize: 25),
                    decoration: InputDecoration(
                      hintText: '',
                      contentPadding: new EdgeInsets.symmetric(
                          horizontal: 10.0, vertical: 2),
                    ),
                  ),
                ),
              ],
            ),
          ),
          new Divider(
            color: Color(0xff007022),
            thickness: 3,
            height: 1,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 40, right: 40, top: 20),
            child: new RaisedButton(
                shape: new RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(7),
                ),
                color: Color(0xff007022),
                onPressed: () {
                  alertPinSendMoney(context);
                  // Navigator.push(context, new MaterialPageRoute(builder: (context) => MobileInvoicePage()));
                },
                child: new Text(
                  "Process",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                )),
          ),
        ],
      ),
    );
  }
}

Future<void> alertPinSendMoney(BuildContext context) {
  return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(10)),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                new Text("Please enter your PIN",
                    style: TextStyle(color: Color(0xff2E943E), fontSize: 18)),
                new InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: new Icon(Icons.close),
                )
              ],
            ),
            content: new Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new Row(
                  children: <Widget>[
                    new Container(
                      width: 150,
                      child: new TextFormField(
                        style: new TextStyle(fontSize: 20),
                        maxLength: 6,
                        keyboardType: TextInputType.number,
                        inputFormatters: <TextInputFormatter>[
                          WhitelistingTextInputFormatter.digitsOnly,
                        ],
                        decoration: InputDecoration(
                          hintText: 'enter PIN here',
                          contentPadding:
                              EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(15.0)),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 15),
                      child: new ButtonTheme(
                        height: 40,
                        minWidth: 30,
                        child: new RaisedButton(
                          shape: new RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          color: Color(0xff007022),
                          onPressed: () {
                            Navigator.pushReplacement(
                                context,
                                new MaterialPageRoute(
                                    builder: (context) =>
                                        new MobileInvoicePage()));
                          },
                          child: new Icon(
                            Icons.arrow_forward,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ));
      });
}