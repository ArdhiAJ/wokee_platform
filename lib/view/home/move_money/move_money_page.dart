import 'dart:math';

import 'package:flutter/material.dart';
import 'package:wokee_platform/view/home/move_money/move_money_confirm.dart';

class MoveMoneyPage extends StatefulWidget {
  @override
  _MoveMoneyPageState createState() => _MoveMoneyPageState();
}

class _MoveMoneyPageState extends State<MoveMoneyPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      appBar: new PreferredSize(
        preferredSize: Size(double.infinity, 50),
        child: new ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25)),
          child: new AppBar(
            backgroundColor: Color(0xff007022),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: new Center(
                  child: new Text(
                    "Move Money",
                    style: TextStyle(color: Colors.white, fontSize: 30),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      body: new ListView(
        children: <Widget>[
          new Card(
            elevation: 5,
            margin: const EdgeInsets.fromLTRB(70, 15, 70, 15),
            shape: new RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Padding(
              padding: const EdgeInsets.only(top: 15, bottom: 15),
              child: new Column(
                children: <Widget>[
                  new Text(
                    "Muhammad Hamdani",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  new Text(
                    "890000025395",
                    style: TextStyle(fontSize: 17),
                  ),
                  new Text(
                    "Rp. 11.050.000",
                    style: TextStyle(fontSize: 30),
                  )
                ],
              ),
            ),
          ),
          new Transform.rotate(
              angle: pi / 2,
              child: new Icon(
                Icons.navigate_next,
                size: 50,
              )),
          new Container(
            // alignment: Alignment.center,
            width: 400,
            height: 130,
            child: new ListView(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                new Card(
                  elevation: 5,
                  margin: const EdgeInsets.all(15),
                  shape: new RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: new Column(
                      children: <Widget>[
                        new Text(
                          "Gadget",
                          style: TextStyle(
                              fontSize: 19, fontWeight: FontWeight.bold),
                        ),
                        new Text(
                          "890000025398",
                          style: TextStyle(fontSize: 15),
                        ),
                        new Text(
                          "Rp. 3.310.000",
                          style: TextStyle(fontSize: 29),
                        )
                      ],
                    ),
                  ),
                ),
                new Card(
                  elevation: 5,
                  margin: const EdgeInsets.all(15),
                  shape: new RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: new Column(
                      children: <Widget>[
                        new Text(
                          "Pendidikan",
                          style: TextStyle(
                              fontSize: 19, fontWeight: FontWeight.bold),
                        ),
                        new Text(
                          "890000025397",
                          style: TextStyle(fontSize: 15),
                        ),
                        new Text(
                          "Rp. 2.220.000",
                          style: TextStyle(fontSize: 29),
                        )
                      ],
                    ),
                  ),
                ),
                new Card(
                  elevation: 5,
                  margin: const EdgeInsets.all(15),
                  shape: new RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: new Column(
                      children: <Widget>[
                        new Text(
                          "Hobi",
                          style: TextStyle(
                              fontSize: 19, fontWeight: FontWeight.bold),
                        ),
                        new Text(
                          "890000025396",
                          style: TextStyle(fontSize: 15),
                        ),
                        new Text(
                          "Rp. 1.020.000",
                          style: TextStyle(fontSize: 29),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          new Divider(
            color: Color(0xff007022),
            thickness: 3,
            height: 1,
          ),
          new Container(
            color: Color(0xffE8E8E8),
            child: new Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 20, top: 20),
                  child: new Container(
                      alignment: Alignment.topLeft,
                      child: new Text(
                        "Amount",
                        style: TextStyle(fontSize: 20),
                      )),
                ),
                new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Text(
                      "Rp.",
                      style: TextStyle(fontSize: 20),
                    ),
                    new SizedBox(
                      width: 5,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 10),
                      child: new Container(
                        width: 200,
                        child: new TextFormField(
                          keyboardType: TextInputType.number,
                          style: new TextStyle(fontSize: 40),
                          decoration: InputDecoration(
                            hintText: '40.000',
                            contentPadding: new EdgeInsets.symmetric(
                                horizontal: 10.0, vertical: 2),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          new Divider(
            color: Color(0xff007022),
            thickness: 3,
            height: 1,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 35, left: 35, right: 35),
            child: new ButtonTheme(
              height: 45,
              child: new RaisedButton(
                color: Color(0xff2E943E),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(7),
                ),
                onPressed: () {
                  Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => new MoveMoneyConfirmPage()));
                },
                child: new Text(
                  "Berikutnya",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
