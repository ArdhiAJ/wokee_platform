import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:wokee_platform/view/home/purchase/mobile_invoice.dart';

class MoveMoneyConfirmPage extends StatefulWidget {
  @override
  _MoveMoneyConfirmPageState createState() => _MoveMoneyConfirmPageState();
}

class _MoveMoneyConfirmPageState extends State<MoveMoneyConfirmPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      appBar: new PreferredSize(
        preferredSize: Size(double.infinity, 50),
        child: new ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25)),
          child: new AppBar(
            backgroundColor: Color(0xff007022),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: new Center(
                  child: new Text(
                    "Move Money",
                    style: TextStyle(color: Colors.white, fontSize: 30),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      body: new ListView(
        children: <Widget>[
          new Card(
            elevation: 5,
            margin: const EdgeInsets.fromLTRB(70, 15, 70, 15),
            shape: new RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Padding(
              padding: const EdgeInsets.only(top: 15, bottom: 15),
              child: new Column(
                children: <Widget>[
                  new Text(
                    "Muhammad Hamdani",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  new Text(
                    "890000025395",
                    style: TextStyle(fontSize: 17),
                  ),
                  new Text(
                    "Rp. 11.050.000",
                    style: TextStyle(fontSize: 30),
                  )
                ],
              ),
            ),
          ),
          new Transform.rotate(
              angle: pi / 2,
              child: new Icon(
                Icons.navigate_next,
                size: 50,
              )),
          new Card(
            elevation: 5,
            margin: const EdgeInsets.fromLTRB(70, 15, 70, 15),
            shape: new RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Padding(
              padding: const EdgeInsets.all(15),
              child: new Column(
                children: <Widget>[
                  new Text(
                    "Gadget",
                    style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold),
                  ),
                  new Text(
                    "890000025398",
                    style: TextStyle(fontSize: 15),
                  ),
                  new Text(
                    "Rp. 3.310.000",
                    style: TextStyle(fontSize: 29),
                  )
                ],
              ),
            ),
          ),
          new Divider(
            color: Color(0xff007022),
            thickness: 3,
            height: 1,
          ),
          new Container(
            color: Color(0xffE8E8E8),
            child: new Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 20, top: 20),
                  child: new Container(
                      alignment: Alignment.topLeft,
                      child: new Text(
                        "Amount",
                        style: TextStyle(fontSize: 20),
                      )),
                ),
                new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Text(
                      "Rp.",
                      style: TextStyle(fontSize: 20),
                    ),
                    new SizedBox(
                      width: 5,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 10),
                      child: new Container(
                        width: 200,
                        child: new TextFormField(
                          keyboardType: TextInputType.number,
                          style: new TextStyle(fontSize: 40),
                          decoration: InputDecoration(
                            hintText: '40.000',
                            contentPadding: new EdgeInsets.symmetric(
                                horizontal: 10.0, vertical: 2),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          new Divider(
            color: Color(0xff007022),
            thickness: 3,
            height: 1,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 35, left: 35, right: 35),
            child: new ButtonTheme(
              height: 45,
              child: new RaisedButton(
                color: Color(0xff2E943E),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(7),
                ),
                onPressed: () {
                  alertPinMoveMoney(context);
                  // Navigator.push(
                  //       context,
                  //       new MaterialPageRoute(
                  //           builder: (context) => new MobileInvoicePage()));
                },
                child: new Text(
                  "Proses",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

Future<void> alertPinMoveMoney(BuildContext context) {
  return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(10)),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                new Text("Please enter your PIN",
                    style: TextStyle(color: Color(0xff2E943E), fontSize: 18)),
                new InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: new Icon(Icons.close),
                )
              ],
            ),
            content: new Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new Row(
                  children: <Widget>[
                    new Container(
                      width: 150,
                      child: new TextFormField(
                        style: new TextStyle(fontSize: 20),
                        maxLength: 6,
                        keyboardType: TextInputType.number,
                        inputFormatters: <TextInputFormatter>[
                          WhitelistingTextInputFormatter.digitsOnly,
                        ],
                        decoration: InputDecoration(
                          hintText: 'enter PIN here',
                          contentPadding:
                              EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(15.0)),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 15),
                      child: new ButtonTheme(
                        height: 40,
                        minWidth: 30,
                        child: new RaisedButton(
                          shape: new RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          color: Color(0xff007022),
                          onPressed: () {
                            Navigator.pushReplacement(
                                context,
                                new MaterialPageRoute(
                                    builder: (context) =>
                                        new MobileInvoicePage()));
                          },
                          child: new Icon(
                            Icons.arrow_forward,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ));
      });
}
