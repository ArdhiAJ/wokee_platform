import 'package:flutter/material.dart';
import 'package:wokee_platform/view/home/payment/electricity_page.dart';
import 'package:wokee_platform/view/home/payment/mobile_payment2.dart';
import 'package:wokee_platform/view/home/purchase/mobile_payment.dart';

class PaymentPage extends StatefulWidget {
  @override
  _PaymentPageState createState() => _PaymentPageState();
}

class _PaymentPageState extends State<PaymentPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      appBar: new PreferredSize(
        preferredSize: Size(double.infinity, 50),
        child: new ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25)),
          child: new AppBar(
            backgroundColor: Color(0xff007022),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: new Center(
                  child: new Text(
                    "Payment",
                    style: TextStyle(color: Colors.white, fontSize: 30),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      body: new ListView(
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 15, bottom: 100),
            child: new Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      new GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              new MaterialPageRoute(
                                  builder: (context) =>
                                      new MobilePaymentPage2()));
                        },
                        child: new Card(
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15)),
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(20, 8, 20, 8),
                            child: new Column(
                              children: <Widget>[
                                new Image.asset(
                                  'assets/images/mobile.png',
                                  height: 50,
                                ),
                                new Text(
                                  'Mobile',
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      new GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              new MaterialPageRoute(
                                  builder: (context) => new ElectricityPage()));
                        },
                        child: new Card(
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15)),
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(15, 8, 15, 8),
                            child: new Column(
                              children: <Widget>[
                                new Image.asset(
                                  'assets/images/pln.png',
                                  height: 50,
                                ),
                                new Text(
                                  'Electricity',
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 10),
            child: new Container(
              color: Color(0xff007022),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                child: new Text(
                  "Recent Transactions",
                  style: TextStyle(
                      fontSize: 20,
                      color: Colors.white,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ),
          new Container(
            height: 250,
            child: new ListView(
              physics: AlwaysScrollableScrollPhysics(),
              children: <Widget>[
                new Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Padding(
                      padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text(
                                "Telkomsel Halo",
                                style: TextStyle(fontSize: 15),
                              ),
                              new Text(
                                "081214994572",
                                style: TextStyle(fontSize: 15),
                              )
                            ],
                          ),
                          new Text(
                            "Rp. 165.000",
                            style: TextStyle(fontSize: 15),
                          )
                        ],
                      )),
                ),
                new Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Padding(
                      padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text(
                                "Listrik",
                                style: TextStyle(fontSize: 15),
                              ),
                              new Text(
                                "14265483876",
                                style: TextStyle(fontSize: 15),
                              )
                            ],
                          ),
                          new Text(
                            "Rp. 330.000",
                            style: TextStyle(fontSize: 15),
                          )
                        ],
                      )),
                ),
                new Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Padding(
                      padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text(
                                "XL Prioritas",
                                style: TextStyle(fontSize: 15),
                              ),
                              new Text(
                                "08170000950",
                                style: TextStyle(fontSize: 15),
                              )
                            ],
                          ),
                          new Text(
                            "Rp. 220.000",
                            style: TextStyle(fontSize: 15),
                          )
                        ],
                      )),
                ),
                new Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Padding(
                      padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text(
                                "Indihome",
                                style: TextStyle(fontSize: 15),
                              ),
                              new Text(
                                "0227512877",
                                style: TextStyle(fontSize: 15),
                              )
                            ],
                          ),
                          new Text(
                            "Rp. 420.000",
                            style: TextStyle(fontSize: 15),
                          )
                        ],
                      )),
                ),
              ],
            ),
          ),
          new Divider(
            color: Color(0xff007022),
            thickness: 2,
          ),
        ],
      ),
    );
  }
}
