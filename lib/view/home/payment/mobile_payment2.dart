import 'package:flutter/material.dart';
import 'package:wokee_platform/view/home/payment/mobile_checkout2.dart';

class MobilePaymentPage2 extends StatefulWidget {
  @override
  _MobilePaymentPage2State createState() => _MobilePaymentPage2State();
}

class _MobilePaymentPage2State extends State<MobilePaymentPage2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      // backgroundColor: Colors.red,
      appBar: new PreferredSize(
        preferredSize: Size(double.infinity, 50),
        child: new ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25)),
          child: new AppBar(
            backgroundColor: Color(0xff007022),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: new Center(
                  child: new Text(
                    "Mobile Payment2",
                    style: TextStyle(color: Colors.white, fontSize: 30),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      body: new ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: new Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 20, bottom: 7),
                  child: new Container(
                    alignment: Alignment.topLeft,
                    child: new Text(
                      "Phone Number",
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                ),
                new Row(
                  children: <Widget>[
                    new Container(
                      width: 250,
                      child: new TextFormField(
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          hintText: '',
                          contentPadding:
                              EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(15.0)),
                        ),
                      ),
                    ),
                    new SizedBox(
                      width: 5,
                    ),
                    new IconButton(
                        iconSize: 30,
                        alignment: Alignment.topCenter,
                        onPressed: () {
                          // Navigator.push(
                          //     context,
                          //     new MaterialPageRoute(
                          //         builder: (context) =>
                          //             new MobilePaymentPage()));
                        },
                        icon: new Image.asset('assets/images/pickcontact.png')),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 35),
                  child: new Container(
                    alignment: Alignment.bottomRight,
                    child: new RaisedButton(
                      color: Color(0xff2E943E),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(7),
                      ),
                      onPressed: () {
                        Navigator.push(
                            context,
                            new MaterialPageRoute(
                                builder: (context) =>
                                    new MobileCheckout2Page()));
                      },
                      child: new Text(
                        "Validasi",
                        style: TextStyle(color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          new Divider(
            color: Color(0xff007022),
          )
        ],
      ),
    );
  }
}
