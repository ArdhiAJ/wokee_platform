import 'package:flutter/material.dart';

class InputPinPage extends StatefulWidget {
  @override
  _InputPinPageState createState() => _InputPinPageState();
}

class _InputPinPageState extends State<InputPinPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      // backgroundColor: Colors.red,
      appBar: new PreferredSize(
        preferredSize: Size(double.infinity, 50),
        child: new ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25)),
          child: new AppBar(
            backgroundColor: Color(0xff007022),
            title: new Center(
              child: new Text(
                "INPUT PIN",
                style: TextStyle(color: Colors.white, fontSize: 30),
              ),
            ),
          ),
        ),
      ),
      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            new Card(
              child: new Column(
                children: <Widget>[
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new SizedBox(
                        width: 40,
                      ),
                      new Text(
                        "Verfikasi Nasabah",
                        style: TextStyle(
                            color: Colors.green,
                            fontWeight: FontWeight.bold,
                            fontSize: 16),
                      ),
                      new IconButton(
                        onPressed: () {
                          // Navigator.push(
                          //     context,
                          //     new MaterialPageRoute(
                          //         builder: (context) => new PlnSelectedPage()));
                        },
                        icon: Icon(
                          Icons.close,
                          size: 18,
                        ),
                      ),
                    ],
                  ),
                  new Text("Silahkan Masukan PIN Mobile Anda"),
                  new SizedBox(
                    height: 10,
                  ),
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Container(
                        width: 200,
                        child: new TextFormField(
                          maxLength: 6,
                          obscureText: true,
                          // controller: _controller,
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                            hintText: 'PIN Anda',
                            // suffixIcon: IconButton(
                            //   iconSize: 20,
                            //   icon: Icon(Icons.close),
                            //   onPressed: () {
                            //     _controller.clear();
                            //   },
                            // ),
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 7, horizontal: 10),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(15.0)),
                          ),
                        ),
                      ),
                      new SizedBox(
                        width: 10,
                      ),
                      new ButtonTheme(
                        minWidth: 0,
                        height: 40,
                        child: new RaisedButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(7),
                            ),
                            color: Colors.green,
                            onPressed: () {
                              // Navigator.push(context, new MaterialPageRoute(builder: (context) => new PlnSuccess()));
                              // Navigator.push(context, new MaterialPageRoute(builder: (context) => new EWalletSuccess()));
                              // Navigator.push(context, new MaterialPageRoute(builder: (context) => new KartuKreditSuccess()));
                            },
                            child: new Icon(
                              Icons.arrow_forward,
                              color: Colors.white,
                            )),
                      ),
                    ],
                  ),
                  new SizedBox(
                    height: 30,
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
