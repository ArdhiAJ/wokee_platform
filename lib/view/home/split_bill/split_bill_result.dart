import 'package:flutter/material.dart';
import 'package:wokee_platform/view/home/main_menu.dart';

class SplitBillResult extends StatefulWidget {
  @override
  _SplitBillResultState createState() => _SplitBillResultState();
}

class _SplitBillResultState extends State<SplitBillResult> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      // backgroundColor: Colors.red,
      appBar: new PreferredSize(
        preferredSize: Size(double.infinity, 50),
        child: new ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25)),
          child: new AppBar(
            backgroundColor: Color(0xff007022),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: new Center(
                  child: new Text(
                    "Split Bill Result",
                    style: TextStyle(color: Colors.white, fontSize: 30),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      body: new ListView(
        children: <Widget>[
          new Card(
            color: Color(0xffF6F6F6),
            elevation: 5,
            margin: const EdgeInsets.fromLTRB(30, 30, 30, 20),
            shape: new RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20)),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 20, 20, 20),
              child: new Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 25, bottom: 15),
                    child: new Container(
                      alignment: Alignment.center,
                      child: new Text(
                        "Permintaan Split Bill",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  new Container(
                    alignment: Alignment.topLeft,
                    child: new Text(
                      "Tanggal Permintaan",
                      style: TextStyle(fontSize: 15),
                    ),
                  ),
                  new TextFormField(
                    keyboardType: TextInputType.text,
                    style: TextStyle(
                      fontSize: 22,
                      color: Color(0xff007022),
                    ),
                    decoration: InputDecoration(
                      hintText: '10 Februari 2020',
                      hintStyle: TextStyle(
                        fontSize: 19,
                        color: Color(0xff007022),
                      ),
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(7.0)),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: new Container(
                      alignment: Alignment.topLeft,
                      child: new Text(
                        "Total Tagihan",
                        style: TextStyle(fontSize: 15),
                      ),
                    ),
                  ),
                  new TextFormField(
                    keyboardType: TextInputType.number,
                    style: TextStyle(
                      fontSize: 15,
                      color: Color(0xff007022),
                    ),
                    decoration: InputDecoration(
                      hintText: 'Rp. 300.000',
                      hintStyle: TextStyle(
                        fontSize: 19,
                        color: Color(0xff007022),
                      ),
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(7.0)),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: new Container(
                        alignment: Alignment.center,
                        child: Column(
                          children: <Widget>[
                            new Text(
                              "Split Bill Ke",
                              style: TextStyle(fontSize: 15),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 100, right: 100),
                              child: new Divider(
                                color: Color(0xff007022),
                                thickness: 1,
                              ),
                            ),
                          ],
                        )),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 5, bottom: 45),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 5, right: 5),
                      child: new Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(bottom: 5),
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                new Text(
                                  "Saya",
                                  style: TextStyle(fontSize: 15),
                                ),
                                new Container(
                                  width: 100,
                                  child: new TextFormField(
                                    keyboardType: TextInputType.number,
                                    style: TextStyle(
                                      fontSize: 15,
                                      color: Color(0xff007022),
                                    ),
                                    decoration: InputDecoration(
                                      hintText: 'Rp. 150.000',
                                      hintStyle: TextStyle(
                                        fontSize: 15,
                                        color: Color(0xff007022),
                                      ),
                                      contentPadding: EdgeInsets.symmetric(
                                          vertical: 7, horizontal: 10),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 5),
                            child: new Divider(
                              color: Color(0xff007022),
                              thickness: 1,
                              height: 0,
                            ),
                          ),
                          new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              new Column(
                                children: <Widget>[
                                  new Text(
                                    "Atri Liana Subekti",
                                    style: TextStyle(fontSize: 15),
                                  ),
                                  new Text(
                                    "+6281289280600",
                                    style: TextStyle(fontSize: 15),
                                  ),
                                ],
                              ),
                              new Container(
                                width: 100,
                                child: new TextFormField(
                                  keyboardType: TextInputType.number,
                                  style: TextStyle(
                                    fontSize: 15,
                                    color: Color(0xff007022),
                                  ),
                                  decoration: InputDecoration(
                                    hintText: 'Rp. 150.000',
                                    hintStyle: TextStyle(
                                      fontSize: 15,
                                      color: Color(0xff007022),
                                    ),
                                    contentPadding: EdgeInsets.symmetric(
                                        vertical: 7, horizontal: 10),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 5),
                            child: new Divider(
                              color: Color(0xff007022),
                              thickness: 1,
                              height: 0,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20, left: 35, right: 35),
            child: new ButtonTheme(
              height: 45,
              child: new RaisedButton(
                color: Color(0xff2E943E),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12),
                ),
                onPressed: () {
                  // Navigator.push(
                  //     context,
                  //     new MaterialPageRoute(
                  //         builder: (context) => new SplitBillResult()));
                },
                child: new Text(
                  "Split Bill Baru",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10, left: 35, right: 35),
            child: new ButtonTheme(
              height: 45,
              child: new RaisedButton(
                color: Color(0xff2E943E),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12),
                ),
                onPressed: () {
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (context) => new MainMenuController()));
                },
                child: new Text(
                  "Selesai",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
