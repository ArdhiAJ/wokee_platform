import 'package:flutter/material.dart';
import 'package:wokee_platform/view/home/split_bill/split_bill_list.dart';

class SplitBillPage extends StatefulWidget {
  @override
  _SplitBillPageState createState() => _SplitBillPageState();
}

class _SplitBillPageState extends State<SplitBillPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      // backgroundColor: Colors.red,
      appBar: new PreferredSize(
        preferredSize: Size(double.infinity, 50),
        child: new ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25)),
          child: new AppBar(
            backgroundColor: Color(0xff007022),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: new Center(
                  child: new Text(
                    "Split Bill",
                    style: TextStyle(color: Colors.white, fontSize: 30),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      body: new ListView(
        children: <Widget>[
          new Card(
            color: Color(0xffF6F6F6),
            elevation: 5,
            margin: const EdgeInsets.fromLTRB(30, 30, 30, 20),
            shape: new RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20)),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 20, 20, 20),
              child: new Column(
                children: <Widget>[
                  new Container(
                    alignment: Alignment.topLeft,
                    child: new Text(
                      "Amount",
                      style: TextStyle(fontSize: 15),
                    ),
                  ),
                  new TextFormField(
                    keyboardType: TextInputType.number,
                    style: TextStyle(
                      fontSize: 22,
                      color: Color(0xff007022),
                    ),
                    decoration: InputDecoration(
                      hintText: 'Rp. 300.000',
                      hintStyle: TextStyle(
                        fontSize: 22,
                        color: Color(0xff007022),
                      ),
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(7.0)),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: new TextFormField(
                      keyboardType: TextInputType.number,
                      style: TextStyle(
                        fontSize: 15,
                        color: Color(0xff007022),
                      ),
                      decoration: InputDecoration(
                        hintText: 'Add Note (optional)',
                        hintStyle: TextStyle(
                          fontSize: 15,
                          color: Color(0xff007022),
                        ),
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(7.0)),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          new Container(
              alignment: Alignment.center,
              child: Column(
                children: <Widget>[
                  new Text(
                    "Split Bill Ke",
                    style: TextStyle(fontSize: 15),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 150, right: 150),
                    child: new Divider(
                      color: Color(0xff007022),
                      thickness: 1,
                    ),
                  ),
                ],
              )),
          Padding(
            padding: const EdgeInsets.only(top: 5),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                new Text(
                  "Saya",
                  style: TextStyle(fontSize: 15),
                ),
                new Container(
                  width: 150,
                  child: new TextFormField(
                    keyboardType: TextInputType.number,
                    style: TextStyle(
                      fontSize: 15,
                      color: Color(0xff007022),
                    ),
                    decoration: InputDecoration(
                      hintText: 'Rp. 0',
                      hintStyle: TextStyle(
                        fontSize: 15,
                        color: Color(0xff007022),
                      ),
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: new FloatingActionButton(
              elevation: 5,
              backgroundColor: Colors.white,
              onPressed: () {
                Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (context) => new SplitBillList()));
              },
              child: new Icon(
                Icons.add,
                color: Color(0xff007022),
              ),
            ),
          )
        ],
      ),
    );
  }
}
