import 'package:flutter/material.dart';
import 'package:wokee_platform/view/home/split_bill/split_bill_list2.dart';

class SplitBillList extends StatefulWidget {
  @override
  _SplitBillListState createState() => _SplitBillListState();
}

class _SplitBillListState extends State<SplitBillList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      // backgroundColor: Colors.red,
      appBar: new PreferredSize(
        preferredSize: Size(double.infinity, 50),
        child: new ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25)),
          child: new AppBar(
            backgroundColor: Color(0xff007022),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: new Center(
                  child: new Text(
                    "Split Bill",
                    style: TextStyle(color: Colors.white, fontSize: 30),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      body: new ListView(
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(40, 20, 0, 20),
            child: new Text(
              "Amount",
              style: TextStyle(fontSize: 15),
            ),
          ),
          new Container(
            height: 400,
            child: new ListView(
              children: <Widget>[
                new Card(
                  elevation: 5,
                  margin: const EdgeInsets.only(left: 35, right: 35, bottom: 5),
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
                    child: new Center(
                        child: new Text(
                      "Atri Liana Subekti",
                      style: TextStyle(fontSize: 22),
                    )),
                  ),
                ),
                new Card(
                  elevation: 5,
                  margin: const EdgeInsets.only(left: 35, right: 35, bottom: 5),
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
                    child: new Center(
                        child: new Text(
                      "Malik Pandu",
                      style: TextStyle(fontSize: 22),
                    )),
                  ),
                ),
                new Card(
                  elevation: 5,
                  margin: const EdgeInsets.only(left: 35, right: 35, bottom: 5),
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
                    child: new Center(
                        child: new Text(
                      "Fahmi Ridwan",
                      style: TextStyle(fontSize: 22),
                    )),
                  ),
                ),
                new Card(
                  elevation: 5,
                  margin: const EdgeInsets.only(left: 35, right: 35, bottom: 5),
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
                    child: new Center(
                        child: new Text(
                      "Abid Ghozi",
                      style: TextStyle(fontSize: 22),
                    )),
                  ),
                ),
                new Card(
                  elevation: 5,
                  margin: const EdgeInsets.only(left: 35, right: 35, bottom: 5),
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
                    child: new Center(
                        child: new Text(
                      "Laksana S",
                      style: TextStyle(fontSize: 22),
                    )),
                  ),
                ),
                new Card(
                  elevation: 5,
                  margin: const EdgeInsets.only(left: 35, right: 35, bottom: 5),
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
                    child: new Center(
                        child: new Text(
                      "Andika Handaru",
                      style: TextStyle(fontSize: 22),
                    )),
                  ),
                ),
                new Card(
                  elevation: 5,
                  margin: const EdgeInsets.only(left: 35, right: 35, bottom: 5),
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
                    child: new Center(
                        child: new Text(
                      "Faisal Toni",
                      style: TextStyle(fontSize: 22),
                    )),
                  ),
                ),
                new Card(
                  elevation: 5,
                  margin: const EdgeInsets.only(left: 35, right: 35, bottom: 5),
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
                    child: new Center(
                        child: new Text(
                      "Ilham Ramadhan",
                      style: TextStyle(fontSize: 22),
                    )),
                  ),
                ),
                new Card(
                  elevation: 5,
                  margin: const EdgeInsets.only(left: 35, right: 35, bottom: 5),
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
                    child: new Center(
                        child: new Text(
                      "Atri Liana Subekti",
                      style: TextStyle(fontSize: 22),
                    )),
                  ),
                ),
                new Card(
                  elevation: 5,
                  margin: const EdgeInsets.only(left: 35, right: 35, bottom: 5),
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
                    child: new Center(
                        child: new Text(
                      "Malik Pandu",
                      style: TextStyle(fontSize: 22),
                    )),
                  ),
                ),
                new Card(
                  elevation: 5,
                  margin: const EdgeInsets.only(left: 35, right: 35, bottom: 5),
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
                    child: new Center(
                        child: new Text(
                      "Fahmi Ridwan",
                      style: TextStyle(fontSize: 22),
                    )),
                  ),
                ),
                new Card(
                  elevation: 5,
                  margin: const EdgeInsets.only(left: 35, right: 35, bottom: 5),
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
                    child: new Center(
                        child: new Text(
                      "Abid Ghozi",
                      style: TextStyle(fontSize: 22),
                    )),
                  ),
                ),
                new Card(
                  elevation: 5,
                  margin: const EdgeInsets.only(left: 35, right: 35, bottom: 5),
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
                    child: new Center(
                        child: new Text(
                      "Laksana S",
                      style: TextStyle(fontSize: 22),
                    )),
                  ),
                ),
                new Card(
                  elevation: 5,
                  margin: const EdgeInsets.only(left: 35, right: 35, bottom: 5),
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
                    child: new Center(
                        child: new Text(
                      "Andika Handaru",
                      style: TextStyle(fontSize: 22),
                    )),
                  ),
                ),
                new Card(
                  elevation: 5,
                  margin: const EdgeInsets.only(left: 35, right: 35, bottom: 5),
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
                    child: new Center(
                        child: new Text(
                      "Faisal Toni",
                      style: TextStyle(fontSize: 22),
                    )),
                  ),
                ),
                new Card(
                  elevation: 5,
                  margin: const EdgeInsets.only(left: 35, right: 35, bottom: 5),
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
                    child: new Center(
                        child: new Text(
                      "Ilham Ramadhan",
                      style: TextStyle(fontSize: 22),
                    )),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 30, left: 35, right: 35),
            child: new ButtonTheme(
              height: 45,
              child: new RaisedButton(
                color: Color(0xff2E943E),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12),
                ),
                onPressed: () {
                  Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => new SplitBillList2()));
                },
                child: new Text(
                  "Berikutnya",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
