import 'package:flutter/material.dart';
import 'package:wokee_platform/view/home/home_page.dart' as home_page;
import 'package:wokee_platform/view/home/sub_account_page.dart'
    as sub_account_page;
import 'package:wokee_platform/view/home/transaction.dart' as transaction_page;
import 'package:wokee_platform/view/home/profile_page.dart' as profile_page;
import 'package:wokee_platform/view/model/model.dart';

class MainMenuController extends StatefulWidget {
  SavingAccount savingAC;

  MainMenuController({Key key, this.savingAC}) : super(key: key);

  @override
  _MainMenuControllerState createState() => _MainMenuControllerState();
}

class _MainMenuControllerState extends State<MainMenuController>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    _tabController = new TabController(vsync: this, length: 4);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print("Saving Account: ${widget.savingAC.savingAc}");
    print("Last Time Login: ${widget.savingAC.lastTimeLogin}");
    SavingAccountHomepage savingAccountHome =
        new SavingAccountHomepage("${widget.savingAC.savingAc}", "${widget.savingAC.lastTimeLogin}");
    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      // backgroundColor: Colors.white38,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: new SizedBox(
        width: 65,
        height: 65,
        child: new FloatingActionButton(
          backgroundColor: Color(0xff007022),
          onPressed: () {},
          tooltip: 'FAB',
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              new Image.asset(
                'assets/images/QrCode.png',
                height: 25,
              ),
              new Text(
                "Pay by\nQR",
                style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              )
            ],
          ),
        ),
      ),
      body: new TabBarView(
        controller: _tabController,
        children: <Widget>[
          new home_page.HomePage(svAcc: savingAccountHome),
          new sub_account_page.SubAccountPage(),
          new transaction_page.TransactionPage(),
          new profile_page.ProfilePage(),
        ],
      ),
      bottomNavigationBar: new ClipRRect(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20), topRight: Radius.circular(20)),
        child: new Material(
          color: Color(0xff007022),
          child: new TabBar(
            labelStyle:
                new TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
            controller: _tabController,
            tabs: <Widget>[
              new Tab(
                icon: new Image.asset(
                  'assets/images/home.png',
                  height: 35,
                ),
                text: "Home",
              ),
              new Tab(
                icon: new Image.asset(
                  'assets/images/subaccount.png',
                  height: 35,
                ),
                text: "SubAccount",
              ),
              new Tab(
                icon: new Image.asset(
                  'assets/images/transaction.png',
                  height: 35,
                ),
                text: "Transaction",
              ),
              new Tab(
                  icon: new Image.asset(
                    'assets/images/profile.png',
                    height: 35,
                  ),
                  text: "Profile"),
            ],
          ),
        ),
      ),
    );
  }
}
