import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:wokee_platform/view/model/color_loader_2.dart';
import 'package:wokee_platform/view/model/model.dart';
import 'package:wokee_platform/view/onboarding/product.dart';
import 'package:wokee_platform/view/onboarding/onboarding_page.dart' as ob;

class AboutYouPage2 extends StatefulWidget {
  final Ektp ektp;
  final CustomerIdAboutYou customerIdAboutYou;

  AboutYouPage2({Key key, this.ektp, this.customerIdAboutYou})
      : super(key: key);
  @override
  _AboutYouPage2State createState() => _AboutYouPage2State();
}

class _AboutYouPage2State extends State<AboutYouPage2> {
  bool _valueWAC = false;
  bool _valueDifAddress = false;
  bool _isHoverValueWorkingInAmericaComp = true;
  bool _isHoverValueDifferentAddress = true;
  var bt = "";
  var occ = "";
  var jp = "";
  final formKey = GlobalKey<FormState>();

  _showdialogWidget(String msg) {
      showDialog(
          context: context,
          barrierDismissible: false,
          child: new AlertDialog(
            title: Text("Warning"),
            content: new Text(
              msg,
              style: new TextStyle(fontSize: 16.0),
            ),
            actions: <Widget>[
              new FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: new Text("OK")),
              // new FlatButton(
              //   onPressed: () {
              //     // qaReset();
              //     // qaAuth();
              //     tempSmsOtp();
              //     // subAccountCreation();
              //     // legacyCif();
              //     // queryAccDetails();
              //   },
              //   // child: Text("Account Details"),
              //   child: Text("Legacy CIF"),
              // ),
            ],
          ));
    }

  void _submit() async {
    if (formKey.currentState.validate()) {
      if(bt != "" && occ != "" && jp != ""){
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return Center(
              child: ColorLoader2(),
            );
          });

        new Future.delayed(new Duration(seconds: 0), () async {
          String nomorKk = "${widget.ektp.nomorKK}";
          // print("Nomor KK: $nomorKk");
          String nomorEktp = "${widget.ektp.nomorEktp}";
          // print("Nomor EKTP: $nomorEktp");
          String namaLengkap = "${widget.ektp.namaLengkap}";
          // print("Nama Lengkap: $namaLengkap");
          String jenisKelamin = "${widget.ektp.jenisKelamin}";
          // print("Jenis Kelamin: $jenisKelamin");
          String tempatLahir = "${widget.ektp.tempatLahir}";
          // print("Tempat Lahir: $tempatLahir");
          String tanggalLahir = "${widget.ektp.tanggalLahir}";
          String alamat = "${widget.ektp.alamat}";
          // print("Nomor KK: $tanggalLahir");
          String nomorRt = "${widget.ektp.nomorRt}";
          // print("Nomor RT: $nomorRt");
          String nomorRw = "${widget.ektp.nomorRw}";
          // print("Nomor RW: $nomorRw");
          String kota = "${widget.ektp.kota}";
          // print("Kota: $kota");
          String provinsi = "${widget.ektp.provinsi}";
          // print("Provinsi: $provinsi");
          String kecamatan = "${widget.ektp.kecamatan}";
          // print("Kecamatan: $kecamatan");
          String kelurahan = "${widget.ektp.kelurahan}";
          // print("Kelurahan: $kelurahan");
          String agama = "${widget.ektp.agama}";
          // print("Agama: $agama");
          String statusPernikahan = "${widget.ektp.statusPernikahan}";
          // print("Status Pernikahan: $statusPernikahan");
          String pekerjaan = "${widget.ektp.pekerjaan}";
          String namaIbu = "${widget.ektp.namaIbu}";
          String pendidikan = "${widget.ektp.pendidikan}";
          String nomorProvinsi = "${widget.ektp.nomorProvinsi}";
          String nomorKabupaten = "${widget.ektp.nomorKabupaten}";
          EktpAboutYou ektp2 = new EktpAboutYou(
              nomorKk,
              nomorEktp,
              namaLengkap,
              jenisKelamin,
              tempatLahir,
              tanggalLahir,
              alamat,
              nomorRt,
              nomorRw,
              kota,
              provinsi,
              kecamatan,
              kelurahan,
              agama,
              statusPernikahan,
              pekerjaan,
              namaIbu,
              pendidikan,
              nomorProvinsi,
              nomorKabupaten);
          String customerIdAboutYouPage =
              "${widget.customerIdAboutYou.customerId}";
          CustomerIdProduct customerIdAboutYou =
              new CustomerIdProduct(customerIdAboutYouPage);

          Navigator.pushReplacement(
              context,
              new MaterialPageRoute(
                  builder: (context) => new ProductPage(
                      ektp2: ektp2, customerIdProduct: customerIdAboutYou)));
        });
      } else {
        _showdialogWidget("Please Fill Out The Form");
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    // String nomorProvinsi = "${widget.ektp.nomorProvinsi}";
    // String nomorKabupaten = "${widget.ektp.nomorKabupaten}";
    // print("Nomor Provinsi: $nomorProvinsi");
    // print("Nomor Kabupaten: $nomorKabupaten");
    // print("Customer ID (About You): ${widget.customerIdAboutYou.customerId}");
    // String nomorKk = "${widget.ektp.nomorKK}";
    // print("Nomor KK: $nomorKk");
    // String nomorEktp = "${widget.ektp.nomorEktp}";
    // print("Nomor EKTP: $nomorEktp");
    // String namaLengkap = "${widget.ektp.namaLengkap}";
    // print("Nama Lengkap: $namaLengkap");
    // String jenisKelamin = "${widget.ektp.jenisKelamin}";
    // print("Jenis Kelamin: $jenisKelamin");
    // String tempatLahir = "${widget.ektp.tempatLahir}";
    // print("Tempat Lahir: $tempatLahir");
    // String tanggalLahir = "${widget.ektp.tanggalLahir}";
    // print("Nomor KK: $tanggalLahir");
    // String nomorRt = "${widget.ektp.nomorRt}";
    // print("Nomor RT: $nomorRt");
    // String nomorRw = "${widget.ektp.nomorRw}";
    // print("Nomor RW: $nomorRw");
    // String kota = "${widget.ektp.kota}";
    // print("Kota: $kota");
    // String provinsi = "${widget.ektp.provinsi}";
    // print("Provinsi: $provinsi");
    // String kecamatan = "${widget.ektp.kecamatan}";
    // print("Kecamatan: $kecamatan");
    // String kelurahan = "${widget.ektp.kelurahan}";
    // print("Kelurahan: $kelurahan");
    // String agama = "${widget.ektp.agama}";
    // print("Agama: $agama");
    // String statusPernikahan = "${widget.ektp.statusPernikahan}";
    // print("Status Pernikahan: $statusPernikahan");
    // String pekerjaan = "${widget.ektp.pekerjaan}";
    // print("Pekerjaan: $pekerjaan");
    txtControllerNumberOfForeignTax.text = "";
    txtControllerNumberOfSocialSecurity.text = "";

    final formWorkingInAmaericanComp = new Column(
      children: <Widget>[
        new Container(
            alignment: Alignment.topLeft,
            child: new Row(
              children: <Widget>[
                new Text("Number of Foreign Tax/FATCA ",
                    style: TextStyle(fontSize: 16)),
                new Text(
                  "*",
                  style: TextStyle(color: Colors.red),
                )
              ],
            )),
        new TextFormField(
          controller: txtControllerNumberOfForeignTax,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            hintText: 'Enter number of Foreign TAX/FATCA here',
            contentPadding: EdgeInsets.symmetric(vertical: 7, horizontal: 10),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          ),
          validator: validateAll,
        ),
        new Container(
            alignment: Alignment.topLeft,
            child: new Row(
              children: <Widget>[
                new Text("Number of Sosical Security ",
                    style: TextStyle(fontSize: 16)),
                new Text(
                  "*",
                  style: TextStyle(color: Colors.red),
                )
              ],
            )),
        new TextFormField(
          controller: txtControllerNumberOfSocialSecurity,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            hintText: 'Enter Number of Social Security here',
            contentPadding: EdgeInsets.symmetric(vertical: 7, horizontal: 10),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          ),
          validator: validateAll,
        ),
      ],
    );

    txtControllerAddressEC.text = "";
    txtControllerNoRwEC.text = "";
    txtControllerNoRtEc.text = "";
    txtControllerMunicipalEC.text = "";
    txtControllerDistrictEC.text = "";
    txtControllerCityEC.text = "";
    txtControllerProvinceEC.text = "";
    txtControllerZipCodeEC.text = "";

    final formDifferentAddress = new Column(
      children: <Widget>[
        new Container(
            alignment: Alignment.topLeft,
            child: new Row(
              children: <Widget>[
                new Text("Address ", style: TextStyle(fontSize: 16)),
                new Text(
                  "*",
                  style: TextStyle(color: Colors.red),
                )
              ],
            )),
        new TextFormField(
          controller: txtControllerAddressEC,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            hintText: 'Enter your address here',
            contentPadding: EdgeInsets.symmetric(vertical: 7, horizontal: 10),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          ),
          validator: validateAll,
        ),
        new Container(
            alignment: Alignment.topLeft,
            child: new Row(
              children: <Widget>[
                new Text("No Rt ", style: TextStyle(fontSize: 16)),
                new Text(
                  "*",
                  style: TextStyle(color: Colors.red),
                )
              ],
            )),
        new TextFormField(
          controller: txtControllerNoRtEc,
          keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[
            WhitelistingTextInputFormatter.digitsOnly,
          ],
          decoration: InputDecoration(
            hintText: 'Enter No RT here',
            contentPadding: EdgeInsets.symmetric(vertical: 7, horizontal: 10),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          ),
          validator: validateAll,
        ),
        new Container(
            alignment: Alignment.topLeft,
            child: new Row(
              children: <Widget>[
                new Text("No Rw ", style: TextStyle(fontSize: 16)),
                new Text(
                  "*",
                  style: TextStyle(color: Colors.red),
                )
              ],
            )),
        new TextFormField(
          controller: txtControllerNoRwEC,
          keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[
            WhitelistingTextInputFormatter.digitsOnly,
          ],
          decoration: InputDecoration(
            hintText: 'Enter No RW here',
            contentPadding: EdgeInsets.symmetric(vertical: 7, horizontal: 10),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          ),
          validator: validateAll,
        ),
        new Container(
            alignment: Alignment.topLeft,
            child: new Row(
              children: <Widget>[
                new Text("Municipal ", style: TextStyle(fontSize: 16)),
                new Text(
                  "*",
                  style: TextStyle(color: Colors.red),
                )
              ],
            )),
        new TextFormField(
          controller: txtControllerMunicipalEC,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            hintText: 'Enter your Municipal here',
            contentPadding: EdgeInsets.symmetric(vertical: 7, horizontal: 10),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          ),
          validator: validateAll,
        ),
        new Container(
            alignment: Alignment.topLeft,
            child: new Row(
              children: <Widget>[
                new Text("District ", style: TextStyle(fontSize: 16)),
                new Text(
                  "*",
                  style: TextStyle(color: Colors.red),
                )
              ],
            )),
        new TextFormField(
          controller: txtControllerDistrictEC,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            hintText: 'Enter your District here',
            contentPadding: EdgeInsets.symmetric(vertical: 7, horizontal: 10),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          ),
          validator: validateAll,
        ),
        new Container(
            alignment: Alignment.topLeft,
            child: new Row(
              children: <Widget>[
                new Text("City ", style: TextStyle(fontSize: 16)),
                new Text(
                  "*",
                  style: TextStyle(color: Colors.red),
                )
              ],
            )),
        new TextFormField(
          controller: txtControllerCityEC,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            hintText: 'Enter your City here',
            contentPadding: EdgeInsets.symmetric(vertical: 7, horizontal: 10),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          ),
          validator: validateAll,
        ),
        new Container(
            alignment: Alignment.topLeft,
            child: new Row(
              children: <Widget>[
                new Text("Province ", style: TextStyle(fontSize: 16)),
                new Text(
                  "*",
                  style: TextStyle(color: Colors.red),
                )
              ],
            )),
        new TextFormField(
          controller: txtControllerProvinceEC,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            hintText: 'Enter your Province here',
            contentPadding: EdgeInsets.symmetric(vertical: 7, horizontal: 10),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          ),
          validator: validateAll,
        ),
        new Container(
            alignment: Alignment.topLeft,
            child: new Row(
              children: <Widget>[
                new Text("ZipCode ", style: TextStyle(fontSize: 16)),
                new Text(
                  "*",
                  style: TextStyle(color: Colors.red),
                )
              ],
            )),
        new TextFormField(
          controller: txtControllerZipCodeEC,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            hintText: 'Enter your ZipCode here',
            contentPadding: EdgeInsets.symmetric(vertical: 7, horizontal: 10),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          ),
          validator: validateAll,
        ),
      ],
    );

    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      // backgroundColor: Colors.red,
      appBar: new PreferredSize(
        preferredSize: Size(double.infinity, 50),
        child: new ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25)),
          child: new AppBar(
            backgroundColor: Color(0xff007022),
            title: new Center(
              child: new Text(
                "Onboarding",
                style: TextStyle(color: Colors.white, fontSize: 30),
              ),
            ),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.all(5),
                child: new InkWell(
                    // color: Color(0xff007022),
                    onTap: _submit,
                    // onTap: () {
                    //   String customerIdAboutYouPage =
                    //       "${widget.customerIdAboutYou.customerId}";
                    //   CustomerIdProduct customerIdAboutYou =
                    //       new CustomerIdProduct(customerIdAboutYouPage);
                    //   Navigator.pushReplacement(
                    //       context,
                    //       new MaterialPageRoute(
                    //           builder: (context) => new ProductPage(
                    //               customerIdProduct: customerIdAboutYou)));
                    // },
                    child: new Row(
                      children: <Widget>[
                        new Text(
                          "Next",
                          style: TextStyle(color: Colors.white, fontSize: 18),
                        ),
                        new Icon(
                          Icons.navigate_next,
                          color: Colors.white,
                          size: 30,
                        )
                      ],
                    )),
              )
            ],
          ),
        ),
      ),
      body: new Form(
        key: formKey,
        child: new ListView(
          physics: NeverScrollableScrollPhysics(),
          children: <Widget>[
            new Container(
              margin: const EdgeInsets.all(10),
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: new Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(bottom: 10),
                      child: new Text(
                        "About You",
                        style: TextStyle(
                            fontSize: 22, fontWeight: FontWeight.bold),
                      ),
                    ),
                    new Container(
                        alignment: Alignment.topLeft,
                        child: new Text("Customer Data",
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ))),
                    new SizedBox(
                      height: 30,
                      child: new CheckboxListTile(
                        value: _valueWAC,
                        title: new Text(
                          "Working in American Companies",
                        ),
                        onChanged: (bool value) {
                          setState(() {
                            _valueWAC = value;
                            if (_valueWAC != null) {
                              if (_valueWAC) {
                                setState(() {
                                  _isHoverValueWorkingInAmericaComp =
                                      !_isHoverValueWorkingInAmericaComp;
                                });
                              } else {
                                setState(() {
                                  _isHoverValueWorkingInAmericaComp = true;
                                });
                              }
                            } else {
                              _valueWAC = false;
                            }
                            // _valueWAC = value;
                            // if (_valueWAC == true) {
                            //   print(wac);
                            // } else {
                            //   print(wac2);
                            // }
                          });
                        },
                      ),
                    ),
                    new CheckboxListTile(
                      value: _valueDifAddress,
                      title: new Text(
                        "Different Address",
                      ),
                      onChanged: (bool value) {
                        setState(() {
                          _valueDifAddress = value;
                          if (_valueDifAddress != null) {
                            if (_valueDifAddress) {
                              setState(() {
                                _isHoverValueDifferentAddress =
                                    !_isHoverValueDifferentAddress;
                              });
                            } else {
                              setState(() {
                                _isHoverValueDifferentAddress = true;
                              });
                            }
                          } else {
                            _valueDifAddress = false;
                          }
                          // _valueDifAddress = value;
                        });
                      },
                    ),
                  ],
                ),
              ),
            ),
            new Container(
              margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
              height: 400,
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: new ListView(
                  physics: AlwaysScrollableScrollPhysics(),
                  children: <Widget>[
                    _isHoverValueWorkingInAmericaComp
                        ? new Container()
                        : formWorkingInAmaericanComp,

                    _isHoverValueDifferentAddress
                        ? new Container()
                        : formDifferentAddress,
                    new Container(
                        alignment: Alignment.topLeft,
                        child: new Row(
                          children: <Widget>[
                            new Text("Business Type ",
                                style: TextStyle(fontSize: 16)),
                            new Text(
                              "*",
                              style: TextStyle(color: Colors.red),
                            )
                          ],
                        )),
                    new FormField<String>(
                      // validator: validateAll,
                      builder: (FormFieldState<String> state) {
                        return InputDecorator(
                          decoration: InputDecoration(
                            contentPadding:
                                new EdgeInsets.symmetric(horizontal: 10.0),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20.0)),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: new DropdownButton<Bt>(
                              value: selectedBT,
                              onChanged: (Bt val) {
                                setState(() {
                                  selectedBT = val;
                                  bt = selectedBT.name;
                                  print("Id: ${selectedBT.id}");
                                });
                              },
                              hint: new Text("Choose Bussiness Type"),
                              items: bussinesType.map((Bt value) {
                                return new DropdownMenuItem<Bt>(
                                  value: value,
                                  child: new Row(
                                    children: <Widget>[
                                      new Text("${value.name}"),
                                    ],
                                  ),
                                );
                              }).toList(),
                              // value: dropdownSelectedItemPOAO,
                            ),
                          ),
                        );
                      },
                    ),
                    // new TextFormField(
                    //   keyboardType: TextInputType.number,
                    //   inputFormatters: <TextInputFormatter>[
                    //     WhitelistingTextInputFormatter.digitsOnly,
                    //   ],
                    //   decoration: InputDecoration(
                    //     hintText: 'Ibu Rumah Tangga',
                    //     contentPadding:
                    //         EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                    //     border: OutlineInputBorder(
                    //         borderRadius: BorderRadius.circular(20.0)),
                    //   ),
                    // ),
                    new Container(
                        alignment: Alignment.topLeft,
                        child: new Row(
                          children: <Widget>[
                            new Text("Occupation ",
                                style: TextStyle(fontSize: 16)),
                            new Text(
                              "*",
                              style: TextStyle(color: Colors.red),
                            )
                          ],
                        )),
                    new FormField<String>(
                      // validator: validateAll,
                      builder: (FormFieldState<String> state) {
                        return InputDecorator(
                          decoration: InputDecoration(
                            contentPadding:
                                new EdgeInsets.symmetric(horizontal: 10.0),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20.0)),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: new DropdownButton<Jt>(
                              value: selectedJT,
                              onChanged: (Jt val) {                                
                                setState(() {
                                  selectedJT = val;
                                  occ = selectedJT.name;
                                  print("Id: ${selectedJT.id}");
                                });
                              },
                              hint: new Text("Choose Your Occupation"),
                              items: jobType.map((Jt value) {
                                return new DropdownMenuItem<Jt>(
                                  value: value,
                                  child: new Row(
                                    children: <Widget>[
                                      new Text("${value.name}"),
                                    ],
                                  ),
                                );
                              }).toList(),
                              // value: dropdownSelectedItemPOAO,
                            ),
                          ),
                        );
                      },
                    ),
                    // new TextFormField(
                    //   controller: txtControllerOccupation,
                    //   keyboardType: TextInputType.text,
                    //   decoration: InputDecoration(
                    //     hintText: 'Enter your occupation',
                    //     contentPadding:
                    //         EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                    //     border: OutlineInputBorder(
                    //         borderRadius: BorderRadius.circular(20.0)),
                    //   ),
                    //   validator: validateAll,
                    // ),
                    new Container(
                        alignment: Alignment.topLeft,
                        child: new Row(
                          children: <Widget>[
                            new Text("Job Position ",
                                style: TextStyle(fontSize: 16)),
                            new Text(
                              "*",
                              style: TextStyle(color: Colors.red),
                            )
                          ],
                        )),
                    new FormField<String>(
                      // validator: validateAll,
                      builder: (FormFieldState<String> state) {
                        return InputDecorator(
                          decoration: InputDecoration(
                            contentPadding:
                                new EdgeInsets.symmetric(horizontal: 10.0),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20.0)),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: new DropdownButton<Jp>(
                              value: selectedJP,
                              onChanged: (Jp val) {                                
                                setState(() {
                                  selectedJP = val;
                                  jp = selectedJP.name;
                                  print("Id: ${selectedJP.id}");
                                });
                              },
                              hint: new Text("Choose Job Position"),
                              items: jobPosition.map((Jp value) {
                                return new DropdownMenuItem<Jp>(
                                  value: value,
                                  child: new Row(
                                    children: <Widget>[
                                      new Text("${value.name}"),
                                    ],
                                  ),
                                );
                              }).toList(),
                              // value: dropdownSelectedItemPOAO,
                            ),
                          ),
                        );
                      },
                    ),
                    // new TextFormField(
                    //   controller: txtControllerJobPosition,
                    //   keyboardType: TextInputType.text,
                    //   decoration: InputDecoration(
                    //     hintText: 'Enter your job position',
                    //     contentPadding:
                    //         EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                    //     border: OutlineInputBorder(
                    //         borderRadius: BorderRadius.circular(20.0)),
                    //   ),
                    //   validator: validateAll,
                    // ),
                    new Container(
                        alignment: Alignment.topLeft,
                        child: new Row(
                          children: <Widget>[
                            new Text("Company/Instution Name ",
                                style: TextStyle(fontSize: 16)),
                            new Text(
                              "*",
                              style: TextStyle(color: Colors.red),
                            )
                          ],
                        )),
                    new TextFormField(
                      controller: txtControllerCompanyName,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        hintText: 'Enter company name here',
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20.0)),
                      ),
                      validator: validateAll,
                    ),
                    new Container(
                        alignment: Alignment.topLeft,
                        child: new Row(
                          children: <Widget>[
                            new Text("Company/Instution Address ",
                                style: TextStyle(fontSize: 16)),
                            new Text(
                              "*",
                              style: TextStyle(color: Colors.red),
                            )
                          ],
                        )),
                    new TextFormField(
                      controller: txtControllerCompanyAddress,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        hintText: 'Enter company address here',
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20.0)),
                      ),
                      validator: validateAll,
                    ),
                    new Container(
                        alignment: Alignment.topLeft,
                        child: new Row(
                          children: <Widget>[
                            new Text("Company/Instution Province ",
                                style: TextStyle(fontSize: 16)),
                            new Text(
                              "*",
                              style: TextStyle(color: Colors.red),
                            )
                          ],
                        )),
                    new FormField<String>(
                      // validator: validateAll,
                      builder: (FormFieldState<String> state) {
                        return InputDecorator(
                          decoration: InputDecoration(
                            contentPadding:
                                new EdgeInsets.symmetric(horizontal: 10.0),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20.0)),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: new DropdownButton(
                              hint: new Text("Choose Company Province"),
                              items: <String>[
                                'Aceh',
                                'Bengkulu',
                                'Jambi',
                                'Kepulauan Bangka Belitung',
                                'Kepulauan Riau',
                                'Lampung',
                                'Riau',
                                'Sumatra Barat',
                                'Sumatra Selatan',
                                'Sumatra Utara',
                                'Banten',
                                'Gorontalo',
                                'Jakarta',
                                'Jawa Barat',
                                'Jawa Tengah',
                                'Jawa Timur',
                                'Kalimantan Barat',
                                'Kalimantan Selatan',
                                'Kalimantan Tengah',
                                'Kalimantan TImur',
                                'Kalimantan Utara',
                                'Maluku',
                                'Maluku Utara',
                                'Bali',
                                'Nusa Tenggara Barat',
                                'Nusa Tenggara Timur',
                                'Papua',
                                'Papua Barat',
                                'Sulawesi Barat',
                                'Sulawesi Selatan',
                                'Sulawesi Tengah',
                                'Sulawesi Tenggara',
                                'Sulawesi Utara',
                                'Yogyakarta'
                              ].map((String value) {
                                return new DropdownMenuItem<String>(
                                  value: value,
                                  child: new Row(
                                    children: <Widget>[
                                      new Text(value),
                                    ],
                                  ),
                                );
                              }).toList(),
                              value: dropdownSelectedItemCP,
                              onChanged: (val) {
                                dropdownSelectedItemCP = val;
                                setState(() {
                                  print(
                                      "Choose Company Province: $dropdownSelectedItemCP");
                                });
                              },
                            ),
                          ),
                        );
                      },
                    ),
                    new Container(
                        alignment: Alignment.topLeft,
                        child: new Row(
                          children: <Widget>[
                            new Text("Company/Instution District ",
                                style: TextStyle(fontSize: 16)),
                            new Text(
                              "*",
                              style: TextStyle(color: Colors.red),
                            )
                          ],
                        )),
                    new TextFormField(
                      controller: txtControllerCompanyDistrict,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        hintText: 'Enter your company district',
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20.0)),
                      ),
                      validator: validateAll,
                    ),
                    new Container(
                        alignment: Alignment.topLeft,
                        child: new Row(
                          children: <Widget>[
                            new Text("Purpose of Account Opening ",
                                style: TextStyle(fontSize: 16)),
                            new Text(
                              "*",
                              style: TextStyle(color: Colors.red),
                            )
                          ],
                        )),
                    new FormField<String>(
                      // validator: validateAll,
                      builder: (FormFieldState<String> state) {
                        return InputDecorator(
                          decoration: InputDecoration(
                            contentPadding:
                                new EdgeInsets.symmetric(horizontal: 10.0),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20.0)),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: new DropdownButton<Pao>(
                              value: selectedPAO,
                              onChanged: (Pao val) {
                                setState(() {
                                  selectedPAO = val;
                                  print("Id: ${selectedPAO.id}");
                                });
                              },
                              hint: new Text("Choose Purpose Account Open"),
                              items: purposeAccountOpening.map((Pao value) {
                                return new DropdownMenuItem<Pao>(
                                  value: value,
                                  child: new Row(
                                    children: <Widget>[
                                      new Text("${value.name}"),
                                    ],
                                  ),
                                );
                              }).toList(),
                              // value: dropdownSelectedItemPOAO,
                            ),
                          ),
                        );
                      },
                    ),
                    new Container(
                        alignment: Alignment.topLeft,
                        child: new Row(
                          children: <Widget>[
                            new Text("Range of Funds ",
                                style: TextStyle(fontSize: 16)),
                            new Text(
                              "*",
                              style: TextStyle(color: Colors.red),
                            )
                          ],
                        )),
                    new FormField<String>(
                      // validator: validateAll,
                      builder: (FormFieldState<String> state) {
                        return InputDecorator(
                          decoration: InputDecoration(
                            contentPadding:
                                new EdgeInsets.symmetric(horizontal: 10.0),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20.0)),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: new DropdownButton<Rof>(
                              value: selectedROF,
                              onChanged: (Rof val) {
                                setState(() {
                                  selectedROF = val;
                                  print("Id: ${selectedROF.id}");
                                });
                              },
                              hint: new Text("Choose Range Of Funds"),
                              items: rangeOfFund.map((Rof value) {
                                return new DropdownMenuItem<Rof>(
                                  value: value,
                                  child: new Row(
                                    children: <Widget>[
                                      new Text("${value.name}"),
                                    ],
                                  ),
                                );
                              }).toList(),
                              // value: dropdownSelectedItemPOAO,
                            ),
                          ),
                        );
                      },
                    ),
                    new Container(
                        alignment: Alignment.topLeft,
                        child: new Row(
                          children: <Widget>[
                            new Text("Range of Expense ",
                                style: TextStyle(fontSize: 16)),
                            new Text(
                              "*",
                              style: TextStyle(color: Colors.red),
                            )
                          ],
                        )),
                    new FormField<String>(
                      // validator: validateAll,
                      builder: (FormFieldState<String> state) {
                        return InputDecorator(
                          decoration: InputDecoration(
                            contentPadding:
                                new EdgeInsets.symmetric(horizontal: 10.0),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20.0)),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: new DropdownButton<Roe>(
                              value: selectedROE,
                              onChanged: (Roe val) {
                                setState(() {
                                  selectedROE = val;
                                  print("Id: ${selectedROE.id}");
                                });
                              },
                              hint: new Text("Choose Range Of Expenses"),
                              items: rangeOfExpense.map((Roe value) {
                                return new DropdownMenuItem<Roe>(
                                  value: value,
                                  child: new Row(
                                    children: <Widget>[
                                      new Text("${value.name}"),
                                    ],
                                  ),
                                );
                              }).toList(),
                              // value: dropdownSelectedItemPOAO,
                            ),
                          ),
                        );
                      },
                    ),
                    new Container(
                        alignment: Alignment.topLeft,
                        child: new Row(
                          children: <Widget>[
                            new Text("Source of Fund ",
                                style: TextStyle(fontSize: 16)),
                            new Text(
                              "*",
                              style: TextStyle(color: Colors.red),
                            )
                          ],
                        )),
                    new FormField<String>(
                      // validator: validateAll,
                      builder: (FormFieldState<String> state) {
                        return InputDecorator(
                          decoration: InputDecoration(
                            contentPadding:
                                new EdgeInsets.symmetric(horizontal: 10.0),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20.0)),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: new DropdownButton<Sof>(
                              value: selectedSOF,
                              onChanged: (Sof val) {
                                setState(() {
                                  selectedSOF = val;
                                  print("Id: ${selectedSOF.id}");
                                });
                              },
                              hint: new Text("Choose Source of Funds"),
                              items: sourceOfFund.map((Sof value) {
                                return new DropdownMenuItem<Sof>(
                                  value: value,
                                  child: new Row(
                                    children: <Widget>[
                                      new Text("${value.name}"),
                                    ],
                                  ),
                                );
                              }).toList(),
                              // value: dropdownSelectedItemPOAO,
                            ),
                          ),
                        );
                      },
                    ),
                    new Container(
                        alignment: Alignment.topLeft,
                        child: new Row(
                          children: <Widget>[
                            new Text("Beneficial Owner Name ",
                                style: TextStyle(fontSize: 16)),
                            new Text(
                              "*",
                              style: TextStyle(color: Colors.red),
                            )
                          ],
                        )),
                    new TextFormField(
                      controller: txtControllerBeneficialOwnerName,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        hintText: 'Enter your name here',
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20.0)),
                      ),
                      validator: validateAll,
                    ),
                    new Container(
                        alignment: Alignment.topLeft,
                        child: new Row(
                          children: <Widget>[
                            new Text("Beneficial Phone Number ",
                                style: TextStyle(fontSize: 16)),
                            new Text(
                              "*",
                              style: TextStyle(color: Colors.red),
                            )
                          ],
                        )),
                    new TextFormField(
                      controller: txtControllerBeneficialPhoneNumber,
                      keyboardType: TextInputType.phone,
                      // inputFormatters: <TextInputFormatter>[
                      //   WhitelistingTextInputFormatter.digitsOnly,
                      // ],
                      decoration: InputDecoration(
                        hintText: 'Enter phone number here',
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20.0)),
                      ),
                      validator: validateAll,
                    ),
                    // new SizedBox(
                    //   height: 35,
                    // ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
