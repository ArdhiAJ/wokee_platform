import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class ExCamPage extends StatefulWidget {
  @override
  _ExCamPageState createState() => _ExCamPageState();
}

class _ExCamPageState extends State<ExCamPage> {
  File photoImage;
  bool _viewVisibleBase64Encode = false;
  // bool _viewVisibleBase64Decode = false;

  Future getPhotoImageFromCamera() async {
    final File image = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      photoImage = image;
    });
  }

  void printWrapped(String text) {
    final pattern = RegExp('.{1,800}'); // 800 is the size of each chunk
    pattern.allMatches(text).forEach((match) => print(match.group(0)));
  }

  coba() {
    if (photoImage != null) {
      List<int> imageBytes = photoImage.readAsBytesSync();
      // print("Image Bytes: $imageBytes");
      String base64Image = base64Encode(imageBytes);
      // print("Base64 Image(Encode): $base64Image");
      // printWrapped("Base64 Image(Encode): $base64Image");
      print("Path File: $photoImage");
      return base64Image;
    } else {
      print("Tidak diconvert");
    }
  }

  // cobaDecode() {
  //   String cobaDecode = coba();
  //   Uint8List _bytesImage;
  //   String _imgString = "$cobaDecode";
  //   _bytesImage = Base64Decoder().convert(_imgString);
  //   Widget image = Image.memory(_bytesImage);
  //   return image;
  // }

  // cobaImage() {
  //   String coba4 = coba();
  //   final _byteImage = Base64Decoder().convert("$coba4");
  //   Widget image = Image.memory(_byteImage);
  //   return image;
  // }

  void showWidget() {
    setState(() {
      _viewVisibleBase64Encode = true;
    });
  }

  // void showWidgetDecode() {
  //   setState(() {
  //     _viewVisibleBase64Decode = true;
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    String coba1 = coba();
    
    // Image coba2 = cobaDecode();

    // void printWrapped(String text) {
    //   final pattern = RegExp('.{1,800}'); // 800 is the size of each chunk
    //   pattern.allMatches(text).forEach((match) => print(match.group(0)));
    // }

    // if (photoImage != null) {
    //   List<int> imageBytes = photoImage.readAsBytesSync();
    //   // print("Image Bytes: $imageBytes");
    //   String base64Image = base64Encode(imageBytes);
    //   // print("Base64 Image(Encode): $base64Image");
    //   printWrapped("Base64 Image(Encode): $base64Image");
    //   print("Path File: $photoImage");
    // } else {
    //   print("Tidak diconvert");
    // }

    return Scaffold(
      appBar: new AppBar(
        title: new Center(
          child: new Text("Convert Image"),
        ),
      ),
      body: Center(
        child: ListView(
          // physics: NeverScrollableScrollPhysics(),
          physics: AlwaysScrollableScrollPhysics(),
          children: <Widget>[
            new SizedBox(
              height: 30,
            ),
            new Container(
              width: MediaQuery.of(context).size.width,
              height: 150.0,
              child: new Center(
                child: photoImage == null
                    ? new Text("No Image Selected")
                    : Image.file(photoImage),
              ),
            ),
            new SizedBox(
              height: 30,
            ),
            FloatingActionButton(
              onPressed: getPhotoImageFromCamera,
              child: new Column(
                children: <Widget>[
                  new Icon(Icons.camera_alt),
                  new Text(
                    "Take Photo",
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
            new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new RaisedButton(
                  onPressed: showWidget,
                  child: new Text("Encode"),
                ),
                // new RaisedButton(
                //   onPressed: () {
                //     cobaImage();
                //   },
                //   child: new Text("Decode"),
                // ),
              ],
            ),
            new Visibility(
                maintainSize: true,
                maintainAnimation: true,
                maintainState: true, 
                visible: _viewVisibleBase64Encode,
                child: new Container(
                  height: 200,
                  child: new ListView(
                    physics: AlwaysScrollableScrollPhysics(),
                    children: <Widget>[new Text("$coba1")],
                  ),
                )),
            // new Visibility(
            //     maintainSize: true,
            //     maintainAnimation: true,
            //     maintainState: true,
            //     visible: _viewVisibleBase64Decode,
            //     child: new Container(
            //       height: 200,
            //       child: new ListView(
            //         physics: AlwaysScrollableScrollPhysics(),
            //         children: <Widget>[
            //           new Image.asset("$coba2")
            //         ],
            //       ),
            //     )),
          ],
        ),
      ),
    );
  }
}
