import 'package:flutter/material.dart';
import 'package:wokee_platform/view/login/login_page.dart';
import 'package:wokee_platform/view/model/color_loader_2.dart';
import 'package:wokee_platform/view/model/model.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';

class FinalOnboardingPage extends StatefulWidget {
  final EktpAgreement ektpAgreement;
  final EktpFinalOnboading ektpFinalOnBoarding;
  final CustomerIdFinalOnboarding customerIdFinalOnBoarding;

  FinalOnboardingPage(
      {Key key,
      this.ektpAgreement,
      this.ektpFinalOnBoarding,
      this.customerIdFinalOnBoarding})
      : super(key: key);

  @override
  _FinalOnboardingPageState createState() => _FinalOnboardingPageState();
}

class _FinalOnboardingPageState extends State<FinalOnboardingPage> {
  bool _agreeTerms = false;
  bool _agreeProduct = false;

  @override
  Widget build(BuildContext context) {
    txtControllerEktpEmail.text = "${txtControllerEmail.text}";
    txtControllerEktpAgreement.text = "${widget.ektpAgreement.nomorEktp}";
    txtControllerEktpMaidenName.text = "${widget.ektpAgreement.namaIbu}";
    txtControllerEktpMobileNumber.text =
        "${txtControllerBeneficialPhoneNumber.text}";
    txtControllerEktpAccountNickname.text =
        "${widget.ektpAgreement.namaLengkap}";
    txtControllerEktpGender.text = "${widget.ektpAgreement.jenisKelamin}";
    txtControllerEktpPlaceOfBirth.text = "${widget.ektpAgreement.tempatLahir}";
    txtControllerEktpDob.text = "${widget.ektpAgreement.tanggalLahir}";
    txtControllerEktpMaritalStatus.text =
        "${widget.ektpAgreement.statusPernikahan}";
    txtControllerEktpNoKK.text = "${widget.ektpAgreement.nomorKK}";
    txtControllerEktpAddress.text = "${widget.ektpAgreement.alamat}";
    txtControllerEktpNoRW.text = "${widget.ektpAgreement.nomorRw}";
    txtControllerEktpNoRT.text = "${widget.ektpAgreement.nomorRt}";
    txtControllerEktpMunicipal.text = "${widget.ektpAgreement.kelurahan}";
    txtControllerEktpDistrict.text = "${widget.ektpAgreement.kecamatan}";
    txtControllerEktpCity.text = "${widget.ektpAgreement.kota}";
    txtControllerEktpBeneficialOwnerName.text =
        "${txtControllerBeneficialPhoneNumber.text}";
    txtControllerEktpBeneficialPhoneNumber.text =
        "${txtControllerBeneficialOwnerName.text}";
    txtControllerEktpSubAccountNickname.text =
        "${txtControllerSubAccountNickname.text}";
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: new PreferredSize(
        preferredSize: Size(double.infinity, 50),
        child: new ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25)),
          child: new AppBar(
            backgroundColor: Color(0xff007022),
            title: new Center(
              child: new Text(
                "Onboarding",
                style: TextStyle(color: Colors.white, fontSize: 30),
              ),
            ),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.all(5),
                child: new InkWell(
                    // color: Color(0xff007022),
                    onTap: () {
                      if (_agreeTerms == false) {
                        _showdialogAgreement(
                            "Please check Agree to the term and conditions's checkbox, before to countinue this process");
                      } else {
                        _navigateOnboarding();
                        // Navigator.pop(context);
                      }
                    },
                    child: new Row(
                      children: <Widget>[
                        new Text(
                          "Submit",
                          style: TextStyle(color: Colors.white, fontSize: 18),
                        ),
                        new Icon(
                          Icons.navigate_next,
                          color: Colors.white,
                          size: 30,
                        )
                      ],
                    )),
              )
            ],
          ),
        ),
      ),
      body: new ListView(
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          new Container(
              height: 200,
              margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
              decoration: BoxDecoration(
                  border: Border.all(width: 1, color: Colors.black)),
              child: new ListView(
                physics: AlwaysScrollableScrollPhysics(),
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: new Text(
                      "SYARAT DAN KETENTUAN",
                      style: new TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 5, bottom: 5),
                    child: new Text(
                      "PERNYATAAN NASABAH / STATEMENT OF CUSTOMERS",
                      style: new TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 25, right: 10),
                    child: new Column(
                      children: <Widget>[
                        new Text(
                          'Dengan menandatangi Formulir\nPembukaan Data Nasabah Perorangan ini\("Formulir") Bank Bukopin ("Bank"). Saya menyatakan dan menyetujui hal-hal sebagai berikut:\n',
                          style:
                              new TextStyle(color: Colors.black, fontSize: 14),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: new Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text(
                                "1.\t",
                                style: new TextStyle(
                                    color: Colors.black, fontSize: 14),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width * 0.7,
                                child: new Text(
                                  'Data, informasi dan/atau dokumen yang dipersyaratkan oleh Bank belum atau tidak Saya lengkapi dengan alasan apapun dan/atau diduga tidak sesuai dengan data atau kondisi yang sebenarnya dan/atau dokumen yang\n',
                                  style: new TextStyle(
                                      color: Colors.black, fontSize: 14),
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: new Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text(
                                "2.\t",
                                style: new TextStyle(
                                    color: Colors.black, fontSize: 14),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width * 0.7,
                                child: new Text(
                                  'Bank tidak memfasilitasi dan berhak membatalkan transaksi yang berhubungan terorisme atau perbuatan melawan hukum, aktivitas yang berhubungan dengan pencucian uang (Money Laundry) sesuai dengan perundang-undangan yang berlaku.\n',
                                  style: new TextStyle(
                                      color: Colors.black, fontSize: 14),
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: new Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text(
                                "4.\t",
                                style: new TextStyle(
                                    color: Colors.black, fontSize: 14),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width * 0.7,
                                child: new Text(
                                  'Memberikan persetujuan kepada Bank untuk memberikan data pribadi saya kepada pihak lain (diluar Bank) untuk tujuan komersial\n',
                                  style: new TextStyle(
                                      color: Colors.black, fontSize: 14),
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: new Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text(
                                "5.\t",
                                style: new TextStyle(
                                    color: Colors.black, fontSize: 14),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width * 0.7,
                                child: new Text(
                                  'Setiap kuasa dan kewenangan yang Saya berikan kepada Bank sebagaimana dimaksud dalam Formulir ini maupun yang Saya berikan di kemudian hari tidak dapat ditarik kembali atau diakhiri dengan alasan apapun. Untuk maksud tersebut Saya setuju untuk mengesampingkan berlakunya Pasal 1813, 1814, 1816, Kitab Undang-undang Hukum Perdata Indonesia.\n',
                                  style: new TextStyle(
                                      color: Colors.black, fontSize: 14),
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: new Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text(
                                "6.\t",
                                style: new TextStyle(
                                    color: Colors.black, fontSize: 14),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width * 0.7,
                                child: new Text(
                                  'Seluruh informasi dan dokumen yang diserahkan kepada Bank serta seluruh isi Formulir dan dokumen-dokumen pendukung lainnya merupakan satu kesatuan yang tidak terpisahkan dengan Formulir Pembukaan Rekening dan  tunduk  kepada  Hukum  Negara Indonesia beserta seluruh peraturan yang terkait.\n',
                                  style: new TextStyle(
                                      color: Colors.black, fontSize: 14),
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              )),
          new Container(
            height: 280,
            margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
            decoration: BoxDecoration(
                border: Border.all(width: 1, color: Colors.black)),
            child: Padding(
              padding: const EdgeInsets.only(left: 15, right: 15),
              child: new ListView(
                physics: AlwaysScrollableScrollPhysics(),
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 10, bottom: 10),
                    child: Container(
                      alignment: Alignment.center,
                      child: new Text(
                        "Please confirm your details",
                        style: new TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  new Container(
                      alignment: Alignment.topLeft,
                      child: new Row(
                        children: <Widget>[
                          new Text("Email",
                              style:
                                  TextStyle(fontSize: 14, color: Colors.grey)),
                        ],
                      )),
                  new TextFormField(
                    controller: txtControllerEktpEmail,
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                      // hintText: 'Enter your name here',
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                      // border: OutlineInputBorder(
                      //     borderRadius: BorderRadius.circular(20.0)),
                    ),
                    // validator: validateAll,
                  ),
                  new Container(
                      alignment: Alignment.topLeft,
                      child: new Row(
                        children: <Widget>[
                          new Text("E-KTP",
                              style:
                                  TextStyle(fontSize: 14, color: Colors.grey)),
                        ],
                      )),
                  new TextFormField(
                    controller: txtControllerEktpAgreement,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      // hintText: 'Enter your name here',
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                      // border: OutlineInputBorder(
                      //     borderRadius: BorderRadius.circular(20.0)),
                    ),
                    // validator: validateAll,
                  ),
                  new Container(
                      alignment: Alignment.topLeft,
                      child: new Row(
                        children: <Widget>[
                          new Text("Mother's Maiden Name",
                              style:
                                  TextStyle(fontSize: 14, color: Colors.grey)),
                        ],
                      )),
                  new TextFormField(
                    controller: txtControllerEktpMaidenName,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      // hintText: 'Enter your name here',
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                      // border: OutlineInputBorder(
                      //     borderRadius: BorderRadius.circular(20.0)),
                    ),
                    // validator: validateAll,
                  ),
                  new Container(
                      alignment: Alignment.topLeft,
                      child: new Row(
                        children: <Widget>[
                          new Text("Mobile Number",
                              style:
                                  TextStyle(fontSize: 14, color: Colors.grey)),
                        ],
                      )),
                  new TextFormField(
                    controller: txtControllerEktpBeneficialPhoneNumber,
                    keyboardType: TextInputType.phone,
                    decoration: InputDecoration(
                      // hintText: 'Enter your name here',
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                      // border: OutlineInputBorder(
                      //     borderRadius: BorderRadius.circular(20.0)),
                    ),
                    // validator: validateAll,
                  ),
                  new Container(
                      alignment: Alignment.topLeft,
                      child: new Row(
                        children: <Widget>[
                          new Text("Account Nickname",
                              style:
                                  TextStyle(fontSize: 14, color: Colors.grey)),
                        ],
                      )),
                  new TextFormField(
                    controller: txtControllerEktpAccountNickname,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      // hintText: 'Enter your name here',
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                      // border: OutlineInputBorder(
                      //     borderRadius: BorderRadius.circular(20.0)),
                    ),
                    // validator: validateAll,
                  ),
                  new Container(
                      alignment: Alignment.topLeft,
                      child: new Row(
                        children: <Widget>[
                          new Text("Gender",
                              style:
                                  TextStyle(fontSize: 14, color: Colors.grey)),
                        ],
                      )),
                  new TextFormField(
                    controller: txtControllerEktpGender,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      // hintText: 'Enter your name here',
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                      // border: OutlineInputBorder(
                      //     borderRadius: BorderRadius.circular(20.0)),
                    ),
                    // validator: validateAll,
                  ),
                  new Container(
                      alignment: Alignment.topLeft,
                      child: new Row(
                        children: <Widget>[
                          new Text("Place Of Birth",
                              style:
                                  TextStyle(fontSize: 14, color: Colors.grey)),
                        ],
                      )),
                  new TextFormField(
                    controller: txtControllerEktpPlaceOfBirth,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      // hintText: 'Enter your name here',
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                      // border: OutlineInputBorder(
                      //     borderRadius: BorderRadius.circular(20.0)),
                    ),
                    // validator: validateAll,
                  ),
                  new Container(
                      alignment: Alignment.topLeft,
                      child: new Row(
                        children: <Widget>[
                          new Text("DOB",
                              style:
                                  TextStyle(fontSize: 14, color: Colors.grey)),
                        ],
                      )),
                  new TextFormField(
                    controller: txtControllerEktpDob,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      // hintText: 'Enter your name here',
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                      // border: OutlineInputBorder(
                      //     borderRadius: BorderRadius.circular(20.0)),
                    ),
                    // validator: validateAll,
                  ),
                  new Container(
                      alignment: Alignment.topLeft,
                      child: new Row(
                        children: <Widget>[
                          new Text("Marital Status",
                              style:
                                  TextStyle(fontSize: 14, color: Colors.grey)),
                        ],
                      )),
                  new TextFormField(
                    controller: txtControllerEktpMaritalStatus,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      // hintText: 'Enter your name here',
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                      // border: OutlineInputBorder(
                      //     borderRadius: BorderRadius.circular(20.0)),
                    ),
                    // validator: validateAll,
                  ),
                  new Container(
                      alignment: Alignment.topLeft,
                      child: new Row(
                        children: <Widget>[
                          new Text("No KK",
                              style:
                                  TextStyle(fontSize: 14, color: Colors.grey)),
                        ],
                      )),
                  new TextFormField(
                    controller: txtControllerEktpNoKK,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      // hintText: 'Enter your name here',
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                      // border: OutlineInputBorder(
                      //     borderRadius: BorderRadius.circular(20.0)),
                    ),
                    // validator: validateAll,
                  ),
                  new Container(
                      alignment: Alignment.topLeft,
                      child: new Row(
                        children: <Widget>[
                          new Text("Address",
                              style:
                                  TextStyle(fontSize: 14, color: Colors.grey)),
                        ],
                      )),
                  new TextFormField(
                    controller: txtControllerEktpAddress,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      // hintText: 'Enter your name here',
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                      // border: OutlineInputBorder(
                      //     borderRadius: BorderRadius.circular(20.0)),
                    ),
                    // validator: validateAll,
                  ),
                  new Container(
                      alignment: Alignment.topLeft,
                      child: new Row(
                        children: <Widget>[
                          new Text("No Rt",
                              style:
                                  TextStyle(fontSize: 14, color: Colors.grey)),
                        ],
                      )),
                  new TextFormField(
                    controller: txtControllerEktpNoRT,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      // hintText: 'Enter your name here',
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                      // border: OutlineInputBorder(
                      //     borderRadius: BorderRadius.circular(20.0)),
                    ),
                    // validator: validateAll,
                  ),
                  new Container(
                      alignment: Alignment.topLeft,
                      child: new Row(
                        children: <Widget>[
                          new Text("No Rw",
                              style:
                                  TextStyle(fontSize: 14, color: Colors.grey)),
                        ],
                      )),
                  new TextFormField(
                    controller: txtControllerEktpNoRW,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      // hintText: 'Enter your name here',
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                      // border: OutlineInputBorder(
                      //     borderRadius: BorderRadius.circular(20.0)),
                    ),
                    // validator: validateAll,
                  ),
                  new Container(
                      alignment: Alignment.topLeft,
                      child: new Row(
                        children: <Widget>[
                          new Text("Municipal",
                              style:
                                  TextStyle(fontSize: 14, color: Colors.grey)),
                        ],
                      )),
                  new TextFormField(
                    controller: txtControllerEktpMunicipal,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      // hintText: 'Enter your name here',
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                      // border: OutlineInputBorder(
                      //     borderRadius: BorderRadius.circular(20.0)),
                    ),
                    // validator: validateAll,
                  ),
                  new Container(
                      alignment: Alignment.topLeft,
                      child: new Row(
                        children: <Widget>[
                          new Text("District",
                              style:
                                  TextStyle(fontSize: 14, color: Colors.grey)),
                        ],
                      )),
                  new TextFormField(
                    controller: txtControllerEktpDistrict,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      // hintText: 'Enter your name here',
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                      // border: OutlineInputBorder(
                      //     borderRadius: BorderRadius.circular(20.0)),
                    ),
                    // validator: validateAll,
                  ),
                  new Container(
                      alignment: Alignment.topLeft,
                      child: new Row(
                        children: <Widget>[
                          new Text("City",
                              style:
                                  TextStyle(fontSize: 14, color: Colors.grey)),
                        ],
                      )),
                  new TextFormField(
                    controller: txtControllerEktpCity,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      // hintText: 'Enter your name here',
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                      // border: OutlineInputBorder(
                      //     borderRadius: BorderRadius.circular(20.0)),
                    ),
                    // validator: validateAll,
                  ),
                  new Container(
                      alignment: Alignment.topLeft,
                      child: new Row(
                        children: <Widget>[
                          new Text("Beneficial Owner Name",
                              style:
                                  TextStyle(fontSize: 14, color: Colors.grey)),
                        ],
                      )),
                  new TextFormField(
                    controller: txtControllerEktpBeneficialOwnerName,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      // hintText: 'Enter your name here',
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                      // border: OutlineInputBorder(
                      //     borderRadius: BorderRadius.circular(20.0)),
                    ),
                    // validator: validateAll,
                  ),
                  new Container(
                      alignment: Alignment.topLeft,
                      child: new Row(
                        children: <Widget>[
                          new Text("Beneficial Phone Number",
                              style:
                                  TextStyle(fontSize: 14, color: Colors.grey)),
                        ],
                      )),
                  new TextFormField(
                    controller: txtControllerEktpBeneficialPhoneNumber,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      // hintText: 'Enter your name here',
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                      // border: OutlineInputBorder(
                      //     borderRadius: BorderRadius.circular(20.0)),
                    ),
                    // validator: validateAll,
                  ),
                  new Container(
                      alignment: Alignment.topLeft,
                      child: new Row(
                        children: <Widget>[
                          new Text("Sub Account NickName",
                              style:
                                  TextStyle(fontSize: 14, color: Colors.grey)),
                        ],
                      )),
                  new TextFormField(
                    controller: txtControllerEktpSubAccountNickname,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      // hintText: 'Enter your name here',
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                      // border: OutlineInputBorder(
                      //     borderRadius: BorderRadius.circular(20.0)),
                    ),
                    // validator: validateAll,
                  ),
                ],
              ),
            ),
          ),
          new Container(
            height: 70,
            margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
            decoration: BoxDecoration(
                border: Border.all(width: 1, color: Colors.black)),
            child: new ListView(
              physics: AlwaysScrollableScrollPhysics(),
              children: <Widget>[
                new SizedBox(
                  height: 30,
                  child: new CheckboxListTile(
                    controlAffinity: ListTileControlAffinity.leading,
                    value: _agreeTerms,
                    onChanged: (bool value) {
                      setState(() {
                        _agreeTerms = value;
                      });
                    },
                    title: new Text(
                      "Agree to the terms and conditions",
                      style: TextStyle(
                          color: Colors.black, fontStyle: FontStyle.italic),
                    ),
                  ),
                ),
                new CheckboxListTile(
                  controlAffinity: ListTileControlAffinity.leading,
                  value: _agreeProduct,
                  onChanged: (bool value) {
                    setState(() {
                      _agreeProduct = value;
                    });
                  },
                  title: new Text(
                    "Agree to get product offering",
                    style: TextStyle(
                        color: Colors.black, fontStyle: FontStyle.italic),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  _showdialogAgreement(String msg) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: new AlertDialog(
          title: Text("Warning"),
          content: new Text(
            msg,
            style: new TextStyle(fontSize: 16.0),
          ),
          actions: <Widget>[
            new FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: new Text("OK")),
          ],
        ));
  }

  //failed
  var valueStatusCode2 = "";
  var valueStatusDesc2 = "";
  //failed
  var valueDataResCode = "";
  var valueDataMessage = "";

  var valueFailed = "";
  var valueFailed2 = "";

  Future serviceOnBoarding() async {
    var url = "http://10.2.62.35:8080/wokee/okoce/onboarding";
    print("URL: $url");

    String customerId = "${widget.customerIdFinalOnBoarding.customerId}";
    print(
        "Customer ID (Your Identity): ${widget.customerIdFinalOnBoarding.customerId}");
    String emailOnBoarding = "${txtControllerEmail.text}";
    print("Email: ${txtControllerEmail.text}");
    String nomorKk = "${widget.ektpFinalOnBoarding.nomorKK}";
    print("Nomor KK: $nomorKk");
    String nomorEktp = "${widget.ektpFinalOnBoarding.nomorEktp}";
    print("Nomor EKTP: $nomorEktp");
    String namaLengkap = "${widget.ektpFinalOnBoarding.namaLengkap}";
    print("Nama Lengkap: $namaLengkap");
    String jenisKelamin = "${widget.ektpFinalOnBoarding.jenisKelamin}";
    print("Jenis Kelamin: $jenisKelamin");
    String tempatLahir = "${widget.ektpFinalOnBoarding.tempatLahir}";
    print("Tempat Lahir: $tempatLahir");
    String parsingDob =
        "${widget.ektpFinalOnBoarding.tanggalLahir}".replaceAll("-", "");
    print("Date Of Birth: $parsingDob");
    String namaIbu = "${widget.ektpFinalOnBoarding.namaIbu}";
    print("Nama Ibu: $namaIbu");
    String alamat = "${widget.ektpFinalOnBoarding.alamat}";
    print("Alamat: $alamat");
    // String tanggalLahir = "${widget.ektpFinalOnBoarding.tanggalLahir}";
    // print("Tanggal Lahir: $tanggalLahir");
    String nomorRt = "${widget.ektpFinalOnBoarding.nomorRt}";
    print("Nomor RT: $nomorRt");
    String nomorRw = "${widget.ektpFinalOnBoarding.nomorRw}";
    print("Nomor RW: $nomorRw");
    String kota = "${widget.ektpFinalOnBoarding.kota}";
    print("Kota: $kota");
    String provinsi = "${widget.ektpFinalOnBoarding.provinsi}";
    print("Provinsi: $provinsi");
    String kecamatan = "${widget.ektpFinalOnBoarding.kecamatan}";
    print("Kecamatan: $kecamatan");
    String kelurahan = "${widget.ektpFinalOnBoarding.kelurahan}";
    print("Kelurahan: $kelurahan");
    String agama = "${widget.ektpFinalOnBoarding.agama}";
    print("Agama: $agama");
    String statusPernikahan = "${widget.ektpFinalOnBoarding.statusPernikahan}";
    print("Status Pernikahan: $statusPernikahan");
    String pekerjaan = "${widget.ektpFinalOnBoarding.pekerjaan}";
    print("Pekerjaan: $pekerjaan");
    String pendidikan = "${widget.ektpFinalOnBoarding.pendidikan}";
    print("Pendidikan: $pendidikan");

    print("BusinessType: ${selectedBT.id}");
    print("PurposeAccountOpen: ${selectedPAO.id}");
    print("SourceOfFund: ${selectedSOF.id}");
    print("RangeOfFund: ${selectedROF.id}");
    print("RangeOfExpense: ${selectedROE.id}");
    print("BenOwnerName: ${txtControllerBeneficialOwnerName.text}");
    print("BenOwnerMobileNo: ${txtControllerBeneficialPhoneNumber.text}");
    print("JobType: ${selectedJT.id}");
    print("JobPos: ${selectedJP.id}");
    print("CompName: ${txtControllerCompanyName.text}");
    print("CompAddr: ${txtControllerCompanyAddress.text}");
    String nomorProvinsi = "${widget.ektpFinalOnBoarding.nomorProvinsi}";
    String nomorKabupaten = "${widget.ektpFinalOnBoarding.nomorKabupaten}";
    if(nomorKabupaten.length == 1){
      nomorKabupaten = "0"+nomorKabupaten;
    }
    print("CompProv: $nomorProvinsi");
    String compCity = "$nomorProvinsi" + "$nomorKabupaten";
    print("CompCity: $compCity");
    String photo = "$customerId"+"_PHOTO.JPG";
    print("Photo: $photo");
    String ektp = "$customerId"+"_EKTP.JPG";
    print("eKTP: $ektp");
    String npwp = "$customerId"+"_NPWP.JPG";
    print("NPWP: $npwp");
    String sign = "$customerId"+"_SIGN.JPG";
    print("SIGN: $sign");

    Map dataBody = {
      "CustomerID": "$customerId",
      "Email": "$emailOnBoarding",
      "MobileNo": "${txtControllerMobileNumber.text}",
      "NoKK": "$nomorKk",
      "NoEKTP": "$nomorEktp",
      "FullName": "$namaLengkap",
      "Gender": "$jenisKelamin",
      "PlaceOfBirth": "$tempatLahir",
      "DateOfBirth": "$parsingDob",
      "MaidenName": "$namaIbu",
      "Address": "$alamat",
      "NoRT": "$nomorRt",
      "NoRW": "$nomorRw",
      "Municipal": "$kelurahan",
      "District": "$kecamatan",
      "City": "$kota",
      "Province": "$provinsi",
      "ZipCode": "10240",
      "Religion": "$agama",
      "MartialStatus": "$statusPernikahan",
      "Education": "$pendidikan",
      "JobTitle": "$pekerjaan",
      "BusinessType": "${selectedBT.id}",
      "PurposeAccountOpen": "${selectedPAO.id}",
      "SourceOfFund": "${selectedSOF.id}",
      "RangeOfFund": "${selectedROF.id}",
      "RangeOfExpense": "${selectedROE.id}",
      "BenOwnerName": "${txtControllerBeneficialOwnerName.text}",
      "BenOwnerMobileNo": "${txtControllerBeneficialPhoneNumber.text}",
      "MobileNoLegacy": "",
      "NumOfForeignTax": "${txtControllerNumberOfForeignTax.text}",
      "NumOfSocialSecurity": "${txtControllerNumberOfSocialSecurity.text}",
      "CifLegacyID": "",
      "AddressEC": "${txtControllerAddressEC.text}",
      "NoRTEC": "${txtControllerNoRtEc.text}",
      "NoRWEC": "${txtControllerNoRwEC.text}",
      "MunicipalEC": "${txtControllerMunicipalEC.text}",
      "DistrictEC": "${txtControllerDistrictEC.text}",
      "CityEC": "${txtControllerCityEC.text}",
      "ProvinceEC": "${txtControllerProvinceEC.text}",
      "ZipCodeEC": "${txtControllerZipCodeEC.text}",
      "MarketingFlag": "Y",
      "ProductAC": "${txtControllerProduct.text}",
      "SeparateIntFlag": "N",
      "CharityAC": "",
      "NickNameSub": "${txtControllerSubAccountNickname.text}",
      "PhotoFileName": "$photo",
      "EktpFileName": "$ektp",
      "NpwpFileName": "$npwp",
      "SignatureFileName": "$npwp",
      // "PhotoFileName": "{CUST_ID}_Ektp.jpg",
      // "EktpFileName": "{CUST_ID}_Ektp.jpg",
      // "NpwpFileName": "{CUST_ID}_Npwp.jpg",
      // "SignatureFileName": "{CUST_ID}_Sign.jpg",
      "AtiEsmID": "CUSTOMER.ONBOARDING",
      "JobType": "${selectedJT.id}",
      "JobPos": "${selectedJP.id}",
      "CompName": "${txtControllerCompanyName.text}",
      "CompAddr": "${txtControllerCompanyAddress.text}",
      "CompProv": "$nomorProvinsi",
      "CompCity": "$compCity"
    };

    // Map dataBody = {
    //   "CustomerID": "$customerId",
    //   "Email": "$emailOnBoarding",
    //   "MobileNo": "${txtControllerBeneficialPhoneNumber.text}",
    //   "NoKK": "$nomorKk",
    //   "NoEKTP": "$nomorEktp",
    //   "FullName": "$namaLengkap",
    //   "Gender": "$jenisKelamin",
    //   "PlaceOfBirth": "$tempatLahir",
    //   "DateOfBirth": "$parsingDob",
    //   "MaidenName": "$namaIbu",
    //   "Address": "$alamat",
    //   "NoRT": "$nomorRt",
    //   "NoRW": "$nomorRw",
    //   "Municipal": "$kelurahan",
    //   "District": "$kecamatan",
    //   "City": "$kota",
    //   "Province": "$provinsi",
    //   "ZipCode": "10240",
    //   "Religion": "$agama",
    //   "MartialStatus": "$statusPernikahan",
    //   "Education": "$pendidikan",
    //   "JobTitle": "$pekerjaan",
    //   "BusinessType": "${selectedBT.id}",
    //   "PurposeAccountOpen": "${selectedPAO.id}",
    //   "SourceOfFund": "${selectedSOF.id}",
    //   "RangeOfFund": "${selectedROF.id}",
    //   "RangeOfExpense": "${selectedROE.id}",
    //   "BenOwnerName": "${txtControllerBeneficialOwnerName.text}",
    //   "BenOwnerMobileNo": "${txtControllerBeneficialPhoneNumber.text}",
    //   "MarketingFlag": "Y",
    //   "ProductAC": "${txtControllerProduct.text}",
    //   "SeparateIntFlag": "N",
    //   "PhotoFileName": "{CUST_ID}_PHOTO.PNG",
    //   "EktpFileName": "{CUST_ID}_EKTP.PNG",
    //   "NpwpFileName": "{CUST_ID}_NPWP.PNG",
    //   "SignatureFileName": "{CUST_ID}_SIGN.PNG",
    //   "AtiEsmID": "CUSTOMER.ONBOARDING",
    //   "JobType": "${selectedJT.id}",
    //   "JobPos": "${selectedJP.id}",
    //   "CompName": "${txtControllerCompanyName.text}",
    //   "CompAddr": "${txtControllerCompanyAddress.text}",
    //   "CompProv": "$nomorProvinsi",
    //   "CompCity": "$compCity"
    //   // "CompProv": "$provinsi",
    //   // "CompCity": "${txtControllerCompanyDistrict.text}"
    // };

    var body = json.encode(dataBody);

    final response = await http.post(url,
        headers: {"Content-Type": "application/json"}, body: body);

    final int statusCode = response.statusCode;

    if (statusCode == 200) {
      print("Connection Success!");
      print("Status Code: $statusCode");
      var result = json.decode(response.body);
      var result1 = result['Status']['statusCode'];
      var result2 = result['Status']['statusDesc'];
      var result3 = result['Data']['resCode'];
      var result4 = result['Data']['Message'];

      print("Status Code: $result1");
      print("status Desc: $result2");
      print("Data (ResCode): $result3");
      print("Data (Message): $result4");

      valueStatusCode2 = result1.toString();
      valueStatusDesc2 = result2.toString();
      valueDataResCode = result3.toString();
      valueDataMessage = result4.toString();
      return valueStatusCode2 +
          valueStatusDesc2 +
          valueDataResCode +
          valueDataMessage;
    } else {
      print("Connection Failed!");
      print("Status Code Failed: $statusCode");
      var result = json.decode(response.body);
      var failed1 = result['Data']['resCode'];
      var failed2 = result['Data']['Message'];
      valueFailed = failed1.toString();
      valueFailed2 = failed2.toString();
      return valueFailed + valueFailed2;
      // throw new Exception("Error while fetching data");
    }
  }

  var cekOnBoard = "";
  _navigateOnboarding() async {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Center(
            child: ColorLoader2(),
          );
        });

    await new Future.delayed(const Duration(seconds: 0));
    String statusOnBoard = await serviceOnBoarding();
    cekOnBoard = "$valueStatusCode2";
    if (cekOnBoard == '200') {
      print("OnBoarding Success");
      Fluttertoast.showToast(
          msg: "OnBoarding Successful",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 1,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);
    }
    Navigator.push(
        context, new MaterialPageRoute(builder: (context) => new LoginPage()));
    txtControllerPassword.clear();
  }
}
