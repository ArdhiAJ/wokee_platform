import 'package:flutter/material.dart';
import 'package:wokee_platform/view/model/color_loader_2.dart';
import 'package:wokee_platform/view/model/model.dart';
import 'package:wokee_platform/view/onboarding/your_identity.dart';

class ProductPage extends StatefulWidget {
  final EktpAboutYou ektp2;
  final CustomerIdProduct customerIdProduct;

  ProductPage({Key key, this.ektp2, this.customerIdProduct}) : super(key: key);
  @override
  _ProductPageState createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {
  bool _checkSeparateInterestAccount = false;
  bool _checkSubAccount = false;
  bool _checkCharityAccount = false;
  bool _isHoverValueSeparateInterest = true;
  bool _isHoverValueSubAccount = true;
  final formKey = GlobalKey<FormState>();

  void _submit() async {
    if (formKey.currentState.validate()) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return Center(
              child: ColorLoader2(),
            );
          });
      if (_checkSeparateInterestAccount == false) {
        new Future.delayed(new Duration(seconds: 0), () async {
          String nomorKk = "${widget.ektp2.nomorKK}";
          // print("Nomor KK: $nomorKk");
          String nomorEktp = "${widget.ektp2.nomorEktp}";
          // print("Nomor EKTP: $nomorEktp");
          String namaLengkap = "${widget.ektp2.namaLengkap}";
          // print("Nama Lengkap: $namaLengkap");
          String jenisKelamin = "${widget.ektp2.jenisKelamin}";
          // print("Jenis Kelamin: $jenisKelamin");
          String tempatLahir = "${widget.ektp2.tempatLahir}";
          // print("Tempat Lahir: $tempatLahir");
          String tanggalLahir = "${widget.ektp2.tanggalLahir}";
          String alamat = "${widget.ektp2.alamat}";
          // print("Nomor KK: $tanggalLahir");
          String nomorRt = "${widget.ektp2.nomorRt}";
          // print("Nomor RT: $nomorRt");
          String nomorRw = "${widget.ektp2.nomorRw}";
          // print("Nomor RW: $nomorRw");
          String kota = "${widget.ektp2.kota}";
          // print("Kota: $kota");
          String provinsi = "${widget.ektp2.provinsi}";
          // print("Provinsi: $provinsi");
          String kecamatan = "${widget.ektp2.kecamatan}";
          // print("Kecamatan: $kecamatan");
          String kelurahan = "${widget.ektp2.kelurahan}";
          // print("Kelurahan: $kelurahan");
          String agama = "${widget.ektp2.agama}";
          // print("Agama: $agama");
          String statusPernikahan = "${widget.ektp2.statusPernikahan}";
          // print("Status Pernikahan: $statusPernikahan");
          String pekerjaan = "${widget.ektp2.pekerjaan}";
          // print("Pekerjaan: $pekerjaan");
          String namaIbu = "${widget.ektp2.namaIbu}";
          String pendidikan = "${widget.ektp2.pendidikan}";
          String nomorProvinsi = "${widget.ektp2.nomorProvinsi}";
          String nomorKabupaten = "${widget.ektp2.nomorKabupaten}";
          EktpProduct ektp3 = new EktpProduct(
              nomorKk,
              nomorEktp,
              namaLengkap,
              jenisKelamin,
              tempatLahir,
              tanggalLahir,
              alamat,
              nomorRt,
              nomorRw,
              kota,
              provinsi,
              kecamatan,
              kelurahan,
              agama,
              statusPernikahan,
              pekerjaan,
              namaIbu,
              pendidikan,
              nomorProvinsi,
              nomorKabupaten);
          String customerIdProductPage =
              "${widget.customerIdProduct.customerId}";
          CustomerIdYourIdentity customerIdProduct =
              new CustomerIdYourIdentity(customerIdProductPage);

          Navigator.pushReplacement(
              context,
              new MaterialPageRoute(
                  builder: (context) => new YourIdentityPage(
                      ektp3: ektp3,
                      customerIdYourIdentity: customerIdProduct)));
        });
      } else {
        new Future.delayed(new Duration(seconds: 0), () async {
          String nomorKk = "${widget.ektp2.nomorKK}";
          // print("Nomor KK: $nomorKk");
          String nomorEktp = "${widget.ektp2.nomorEktp}";
          // print("Nomor EKTP: $nomorEktp");
          String namaLengkap = "${widget.ektp2.namaLengkap}";
          // print("Nama Lengkap: $namaLengkap");
          String jenisKelamin = "${widget.ektp2.jenisKelamin}";
          // print("Jenis Kelamin: $jenisKelamin");
          String tempatLahir = "${widget.ektp2.tempatLahir}";
          // print("Tempat Lahir: $tempatLahir");
          String tanggalLahir = "${widget.ektp2.tanggalLahir}";
          String alamat = "${widget.ektp2.alamat}";
          // print("Nomor KK: $tanggalLahir");
          String nomorRt = "${widget.ektp2.nomorRt}";
          // print("Nomor RT: $nomorRt");
          String nomorRw = "${widget.ektp2.nomorRw}";
          // print("Nomor RW: $nomorRw");
          String kota = "${widget.ektp2.kota}";
          // print("Kota: $kota");
          String provinsi = "${widget.ektp2.provinsi}";
          // print("Provinsi: $provinsi");
          String kecamatan = "${widget.ektp2.kecamatan}";
          // print("Kecamatan: $kecamatan");
          String kelurahan = "${widget.ektp2.kelurahan}";
          // print("Kelurahan: $kelurahan");
          String agama = "${widget.ektp2.agama}";
          // print("Agama: $agama");
          String statusPernikahan = "${widget.ektp2.statusPernikahan}";
          // print("Status Pernikahan: $statusPernikahan");
          String pekerjaan = "${widget.ektp2.pekerjaan}";
          // print("Pekerjaan: $pekerjaan");
          String namaIbu = "${widget.ektp2.namaIbu}";
          String pendidikan = "${widget.ektp2.pendidikan}";
          String nomorProvinsi = "${widget.ektp2.nomorProvinsi}";
          String nomorKabupaten = "${widget.ektp2.nomorKabupaten}";
          EktpProduct ektp3 = new EktpProduct(
              nomorKk,
              nomorEktp,
              namaLengkap,
              jenisKelamin,
              tempatLahir,
              tanggalLahir,
              alamat,
              nomorRt,
              nomorRw,
              kota,
              provinsi,
              kecamatan,
              kelurahan,
              agama,
              statusPernikahan,
              pekerjaan,
              namaIbu,
              pendidikan,
              nomorProvinsi,
              nomorKabupaten);
          String customerIdProductPage =
              "${widget.customerIdProduct.customerId}";
          CustomerIdYourIdentity customerIdProduct =
              new CustomerIdYourIdentity(customerIdProductPage);

          Navigator.pushReplacement(
              context,
              new MaterialPageRoute(
                  builder: (context) => new YourIdentityPage(
                      ektp3: ektp3,
                      customerIdYourIdentity: customerIdProduct)));
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    txtControllerProduct.text = "Saving Account (Wokee)";
    txtControllerSubAccountNickname.text = "";
    String nomorProvinsi = "${widget.ektp2.nomorProvinsi}";
    String nomorKabupaten = "${widget.ektp2.nomorKabupaten}";
    print("Nomor Provinsi: $nomorProvinsi");
    print("Nomor Kabupaten: $nomorKabupaten");
    // print("Comp City: $nomorProvinsi"+"$nomorKabupaten");
    // print("Customer ID (Product): ${widget.customerIdProduct.customerId}");
    // String nomorKk = "${widget.ektp2.nomorKK}";
    // print("Nomor KK: $nomorKk");
    // String nomorEktp = "${widget.ektp2.nomorEktp}";
    // print("Nomor EKTP: $nomorEktp");
    // String namaLengkap = "${widget.ektp2.namaLengkap}";
    // print("Nama Lengkap: $namaLengkap");
    // String jenisKelamin = "${widget.ektp2.jenisKelamin}";
    // print("Jenis Kelamin: $jenisKelamin");
    // String tempatLahir = "${widget.ektp2.tempatLahir}";
    // print("Tempat Lahir: $tempatLahir");
    // String tanggalLahir = "${widget.ektp2.tanggalLahir}";
    // print("Nomor KK: $tanggalLahir");
    // String nomorRt = "${widget.ektp2.nomorRt}";
    // print("Nomor RT: $nomorRt");
    // String nomorRw = "${widget.ektp2.nomorRw}";
    // print("Nomor RW: $nomorRw");
    // String kota = "${widget.ektp2.kota}";
    // print("Kota: $kota");
    // String provinsi = "${widget.ektp2.provinsi}";
    // print("Provinsi: $provinsi");
    // String kecamatan = "${widget.ektp2.kecamatan}";
    // print("Kecamatan: $kecamatan");
    // String kelurahan = "${widget.ektp2.kelurahan}";
    // print("Kelurahan: $kelurahan");
    // String agama = "${widget.ektp2.agama}";
    // print("Agama: $agama");
    // String statusPernikahan = "${widget.ektp2.statusPernikahan}";
    // print("Status Pernikahan: $statusPernikahan");
    // String pekerjaan = "${widget.ektp2.pekerjaan}";
    // print("Pekerjaan: $pekerjaan");
    final formSubAccountNickname = new Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 10),
          child: new Container(
              alignment: Alignment.topLeft,
              child: new Text("Sub Account Nickname",
                  style: TextStyle(
                    fontSize: 16,
                  ))),
        ),
        new TextFormField(
          controller: txtControllerSubAccountNickname,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            hintText: 'Please enter sub account nickname',
            contentPadding: EdgeInsets.symmetric(vertical: 7, horizontal: 10),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          ),
          validator: validateAll,
        ),
      ],
    );

    final formSeparateInterest = new Column(
      children: <Widget>[
        new Row(
          children: <Widget>[
            new Text(
              "Select your Account",
              style: TextStyle(
                fontSize: 16,
              ),
            ),
            new Flexible(
              child: new Column(
                children: <Widget>[
                  new CheckboxListTile(
                    controlAffinity: ListTileControlAffinity.leading,
                    value: _checkSubAccount,
                    onChanged: (bool value) {
                      setState(() {
                        _checkSubAccount = value;
                        if (_checkSubAccount != null) {
                          if (_checkSubAccount) {
                            if (_checkCharityAccount) {
                              setState(() {
                                _isHoverValueSubAccount =
                                    !_isHoverValueSubAccount;
                                _checkCharityAccount = !_checkCharityAccount;
                              });
                            } else {
                              setState(() {
                                _isHoverValueSubAccount =
                                    !_isHoverValueSubAccount;
                              });
                            }
                          } else {
                            setState(() {
                              _isHoverValueSubAccount = true;
                            });
                          }
                        } else {
                          _checkSubAccount = false;
                        }
                        // _checkSubAccount = value; //yg sebelumnya
                      });
                    },
                    title: new Text(
                      "Sub Account",
                      style: TextStyle(
                        color: Color(0xff007022),
                      ),
                    ),
                  ),
                  new CheckboxListTile(
                    controlAffinity: ListTileControlAffinity.leading,
                    value: _checkCharityAccount,
                    onChanged: (bool value) {
                      setState(() {
                        _checkCharityAccount = value;
                        if (_checkCharityAccount != null) {
                          if (_checkCharityAccount) {
                            if (_checkSubAccount) {
                              setState(() {
                                _checkSubAccount = !_checkSubAccount;
                                _isHoverValueSubAccount =
                                    !_isHoverValueSubAccount;
                                // _isHoverValueKK = !_isHoverValueKK;
                              });
                            } else {
                              setState(() {});
                            }
                          } else {}
                        } else {
                          _checkCharityAccount = false;
                        }
                      });
                    },
                    title: new Text(
                      "Charity Account",
                      style: TextStyle(
                        color: Color(0xff007022),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
        new Divider(
          color: Colors.black,
          height: 1,
        ),
      ],
    );

    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      // backgroundColor: Colors.red,
      appBar: new PreferredSize(
        preferredSize: Size(double.infinity, 50),
        child: new ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25)),
          child: new AppBar(
            backgroundColor: Color(0xff007022),
            title: new Center(
              child: new Text(
                "Onboarding",
                style: TextStyle(color: Colors.white, fontSize: 30),
              ),
            ),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.all(5),
                child: new InkWell(
                    // color: Color(0xff007022),
                    onTap: _submit,
                    // onTap: () {
                    //   String customerIdProductPage =
                    //       "${widget.customerIdProduct.customerId}";
                    //   CustomerIdYourIdentity customerIdProduct =
                    //       new CustomerIdYourIdentity(customerIdProductPage);
                    //   Navigator.pushReplacement(
                    //       context,
                    //       new MaterialPageRoute(
                    //           builder: (context) => new YourIdentityPage(
                    //               customerIdYourIdentity: customerIdProduct)));
                    // },
                    child: new Row(
                      children: <Widget>[
                        new Text(
                          "Next",
                          style: TextStyle(color: Colors.white, fontSize: 18),
                        ),
                        new Icon(
                          Icons.navigate_next,
                          color: Colors.white,
                          size: 30,
                        )
                      ],
                    )),
              )
            ],
          ),
        ),
      ),
      body: new Form(
        key: formKey,
        child: new ListView(
          children: <Widget>[
            new Container(
              color: Colors.white,
              margin: const EdgeInsets.all(10),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: new Column(
                  children: <Widget>[
                    new Text(
                      "Product",
                      style:
                          TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: new Container(
                          alignment: Alignment.topLeft,
                          child: new Text("Product",
                              style: TextStyle(
                                fontSize: 16,
                              ))),
                    ),
                    new TextFormField(
                      controller: txtControllerProduct,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        hintText: 'Please enter product name',
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20.0)),
                      ),
                      validator: validateAll,
                    ),
                    new CheckboxListTile(
                      // controlAffinity: ListTileControlAffinity.leading,
                      value: _checkSeparateInterestAccount,
                      onChanged: (bool value) {
                        setState(() {
                          _checkSeparateInterestAccount = value;
                          if (_checkSeparateInterestAccount != null) {
                            if (_checkSeparateInterestAccount) {
                              setState(() {
                                _isHoverValueSeparateInterest =
                                    !_isHoverValueSeparateInterest;
                              });
                            } else {
                              if (_checkCharityAccount) {
                                setState(() {
                                  _isHoverValueSeparateInterest = true;
                                  _checkCharityAccount = !_checkCharityAccount;
                                  // _isHoverValueSubAccount = !_isHoverValueSubAccount;
                                });
                              }

                              if (_checkSubAccount) {
                                setState(() {
                                  _isHoverValueSeparateInterest = true;
                                  _checkSubAccount = !_checkSubAccount;
                                  _isHoverValueSubAccount =
                                      !_isHoverValueSubAccount;
                                  // _isHoverValueSubAccount = !_isHoverValueSubAccount;
                                });
                              } else {
                                setState(() {
                                  _isHoverValueSeparateInterest = true;
                                  // _isHoverValueSubAccount = !_isHoverValueSubAccount;
                                });
                              }
                            }
                          } else {
                            _checkSeparateInterestAccount = false;
                          }
                          // _checkSeparateInterestAccount = value; //yg sebelumnya
                        });
                      },
                      title: new Text(
                        "Separate interest to another account",
                        style: TextStyle(
                          color: Color(0xff007022),
                        ),
                      ),
                    ),
                    _isHoverValueSeparateInterest
                        ? new Container()
                        : formSeparateInterest,
                    _isHoverValueSubAccount
                        ? new Container()
                        : formSubAccountNickname,
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
