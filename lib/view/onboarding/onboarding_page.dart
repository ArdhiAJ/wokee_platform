import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:wokee_platform/service/disdukcapil/dukcapil_api.dart';
import 'package:wokee_platform/service/disdukcapil/dukcapil_json.dart';
import 'package:wokee_platform/view/model/color_loader_2.dart';
import 'package:wokee_platform/view/model/model.dart';
import 'package:wokee_platform/view/onboarding/aboutyou2.dart';
import 'package:wokee_platform/service/disdukcapil/dukcapil.dart';
import 'package:wokee_platform/service/account/legacy_cif.dart';
import 'package:wokee_platform/service/account/query_acc_details.dart';
import 'package:wokee_platform/service/account/sub_acc_creation.dart';
import 'package:wokee_platform/service/account/tempChangeOTP.dart';
import 'package:wokee_platform/service/account/qa_auth.dart';
import 'package:wokee_platform/service/account/qa_reset.dart';
import 'package:wokee_platform/view/onboarding/home_onboarding.dart';
import 'package:wokee_platform/view/onboarding/otp_cif.dart';

class OnboardingPage extends StatefulWidget {
  final CustomerIdOnBoardingPage customerIdOnBoarding;

  OnboardingPage({Key key, this.customerIdOnBoarding}) : super(key: key);
  @override
  _OnboardingPageState createState() => _OnboardingPageState();
}

class _OnboardingPageState extends State<OnboardingPage> {
  // int _currentStepper = 0;
  bool _checkValueMotherName = false;
  bool _checkValueKK = false;
  bool _isHoverValueMotherName = true;
  bool _isHoverValueKK = true;
  bool _viewVisibleAboutYou = false;
  var checkCompProv = "";
  var checkPurpose = "";
  var checkROF = "";
  var checkROE = "";
  var checkSOF = "";
  final formKey = GlobalKey<FormState>();
  final formKey2 = GlobalKey<FormState>();

  void showWidget() {
    setState(() {
      _viewVisibleAboutYou = true;
    });
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      txtControllerEktp.clear();
      txtControllerMotherName.clear();
      txtControllerKkNumber.clear();
    });
  }

  void _submitVerEktp() async {
    if (formKey.currentState.validate()) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return Center(
              child: ColorLoader2(),
            );
          });

      new Future.delayed(new Duration(seconds: 3), () async {
        await dukcapilApi();
        print("EKTP(Inputan): ${txtControllerEktp.text}");
        print("Status Retrieve Data Dukcapil(Service): $valueStatusCode");
        print("Nama Ibu(Service): $valueNamaIbu");
        print("Nama Ibu(Inputan): ${txtControllerMotherName.text}");
        print("Nomor KK(Service): $valueNomorKartuKeluarga");
        print("Nomor KK(Inputan): ${txtControllerKkNumber.text}");

        var motherName = txtControllerMotherName.text.toUpperCase();
        var nomorKK = txtControllerKkNumber.text.toUpperCase();
        if ("$valueNamaIbu" == "$motherName") {
          Navigator.pop(context);
          _showdialogVerEktpSuccess("Verification Success");
          String hitServiceLegacyCif = await legacyCif();
          if (valueCifNoLegacy != "" && valueMobileNoLegacy != "") {
            showWidget();
          } else {
            showWidget();
            Navigator.push(context,
                new MaterialPageRoute(builder: (context) => new CifOTPPage()));
          }
          print("Terverifikasi");
        } else if ("$valueNomorKartuKeluarga" == "$nomorKK") {
          Navigator.pop(context);
          _showdialogVerEktpSuccess("Verifikasi Sukses");
          String hitServiceLegacyCif = await legacyCif();
          if (valueCifNoLegacy != "" && valueMobileNoLegacy != "") {
            showWidget();
          } else {
            showWidget();
            Navigator.push(context,
                new MaterialPageRoute(builder: (context) => new CifOTPPage()));
          }
          print("Terverifikasi");
        } else {
          Navigator.pop(context);
          _showdialogVerEktpFailed("eKTP Not Found");
          _viewVisibleAboutYou = false;
          print("Verifikasi Gagal");
        }
      });
    }
  }

  void _next() async {
    if (_viewVisibleAboutYou == false) {
      print("You Cannot Continue This Process");
      _showdialogWidget(
          "Before continue this process, please verify your identity!");
    } else {
      if (formKey2.currentState.validate()) {
        print(checkCompProv + "" + checkPurpose + "" + checkROE + "" + checkROF + "" + checkSOF);
        if(checkCompProv != "" && checkPurpose != "" && checkROE != "" && checkROF != "" && 
            checkSOF != ""){
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return Center(
                    child: ColorLoader2(),
                  );
                });
            Ektp ektp = new Ektp(
                "$valueNomorKartuKeluarga",
                "$valueEktp",
                "$valueNamaLengkap",
                "$valueJenisKelamin",
                "$valueTempatLahir",
                "$valueTanggalLahir",
                "$valueAlamat",
                "$valueRt",
                "$valueRw",
                "$valueNamaKabupaten",
                "$valueNomorProvinsi",
                "$valueNamaKecamatan",
                "$valueNamaKelurahan",
                "$valueAgama",
                "$valueStatusKawin",
                "$valuePekerjaan",
                "$valueNamaIbu",
                "$valuePendidikan",
                "$valueNomorProvinsi",
                "$valueNomorKabupatan");
            String customerIdOnboardingPage =
                "${widget.customerIdOnBoarding.customerId}";
            CustomerIdAboutYou customerIdOnBoarding =
                new CustomerIdAboutYou(customerIdOnboardingPage);
            Navigator.push(
                context,
                new MaterialPageRoute(
                    builder: (context) => new AboutYouPage2(
                        ektp: ektp, customerIdAboutYou: customerIdOnBoarding)));
        } else {
          _showdialogWidget("Please Fill Out The Form");
        }
      }
    }
  }

  _showdialogWidget(String msg) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: new AlertDialog(
          title: Text("Warning"),
          content: new Text(
            msg,
            style: new TextStyle(fontSize: 16.0),
          ),
          actions: <Widget>[
            new FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: new Text("OK")),
            // new FlatButton(
            //   onPressed: () {
            //     // qaReset();
            //     // qaAuth();
            //     tempSmsOtp();
            //     // subAccountCreation();
            //     // legacyCif();
            //     // queryAccDetails();
            //   },
            //   // child: Text("Account Details"),
            //   child: Text("Legacy CIF"),
            // ),
          ],
        ));
  }

  _showdialogVerOptions(String msg) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: new AlertDialog(
          title: Text("Warning"),
          content: new Text(
            msg,
            style: new TextStyle(fontSize: 16.0),
          ),
          actions: <Widget>[
            new FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                  _checkValueKK  = false;
                  _checkValueMotherName = false;
                },
                child: new Text("OK")),
          ],
        ));
  }



  @override
  Widget build(BuildContext context) {
    print("Nomor Provinsi: $valueNomorProvinsi");
    print("Nomor Kabupaten: $valueNomorKabupatan");
    // print(
    //     "Customer ID (Onboarding_Page): ${widget.customerIdOnBoarding.customerId}");
    final formMotherName = new Column(
      children: <Widget>[
        new Container(
            alignment: Alignment.topLeft,
            child: new Text("Mother's Maiden Name",
                style: TextStyle(fontSize: 16))),
        new TextFormField(
          controller: txtControllerMotherName,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            hintText: 'Enter your mother name here',
            contentPadding: EdgeInsets.symmetric(vertical: 7, horizontal: 10),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          ),
          validator: validateMotherName,
        ),
      ],
    );

    final formKK = new Column(
      children: <Widget>[
        new Container(
            alignment: Alignment.topLeft,
            child: new Text("Kartu Keluarga Number",
                style: TextStyle(fontSize: 16))),
        new TextFormField(
          controller: txtControllerKkNumber,
          keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[
            WhitelistingTextInputFormatter.digitsOnly,
          ],
          decoration: InputDecoration(
            hintText: 'Enter your family number here',
            contentPadding: EdgeInsets.symmetric(vertical: 7, horizontal: 10),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          ),
          validator: validateKK,
        ),
      ],
    );
    // List<Step> steps = [
    //   new Step(
    //     title: const Text("About You"),
    //     content: Column(
    //       children: <Widget>[
    //         new Text("1")
    //       ],
    //     )
    //   ),
    //   new Step(
    //     title: const Text("Product"),
    //     content: Column(
    //       children: <Widget>[
    //         new Text("2")
    //       ],
    //     )
    //   ),
    //   new Step(
    //     title: const Text("Your Identity"),
    //     content: Column(
    //       children: <Widget>[
    //         new Text("3")
    //       ],
    //     )
    //   ),
    //   new Step(
    //     title: const Text("Agreement"),
    //     content: Column(
    //       children: <Widget>[
    //         new Text("4")
    //       ],
    //     )
    //   )
    // ];

    String _txtStatusDukcapil = "Verified";
        return Scaffold(
          backgroundColor: Color(0xFFF2F2F2),
          // backgroundColor: Colors.red,
          appBar: new PreferredSize(
            preferredSize: Size(double.infinity, 50),
            child: new ClipRRect(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(25),
                  bottomRight: Radius.circular(25)),
              child: new AppBar(
                backgroundColor: Color(0xff007022),
                title: new Center(
                  child: new Text(
                    "Onboarding",
                    style: TextStyle(color: Colors.white, fontSize: 30),
                  ),
                ),
                actions: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(5),
                    child: new InkWell(
                        // color: Color(0xff007022),
                        onTap: _next,
                        // onTap: () {
                        //   String customerIdOnboardingPage =
                        //       "${widget.customerIdOnBoarding.customerId}";
                        //   CustomerIdAboutYou customerIdOnBoarding =
                        //       new CustomerIdAboutYou(customerIdOnboardingPage);
                        //   Navigator.push(
                        //       context,
                        //       new MaterialPageRoute(
                        //           builder: (context) => new AboutYouPage2(
                        //               customerIdAboutYou: customerIdOnBoarding)));
                        // },
                        child: new Row(
                          children: <Widget>[
                            new Text(
                              "Next",
                              style: TextStyle(color: Colors.white, fontSize: 18),
                            ),
                            new Icon(
                              Icons.navigate_next,
                              color: Colors.white,
                              size: 30,
                            )
                          ],
                        )),
                  )
                ],
              ),
            ),
          ),
          body: new Form(
            key: formKey,
            child: new ListView(
              // physics: NeverScrollableScrollPhysics(),
              children: <Widget>[
                new Container(
                    margin: const EdgeInsets.all(10),
                    color: Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: new Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(bottom: 10),
                            child: new Text(
                              "About You",
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.bold),
                            ),
                          ),
                          new Container(
                              alignment: Alignment.topLeft,
                              child: new Text("Your e-KTP Number/NIK",
                                  style: TextStyle(fontSize: 16))),
                          new TextFormField(
                            controller: txtControllerEktp,
                            keyboardType: TextInputType.number,
                            inputFormatters: <TextInputFormatter>[
                              WhitelistingTextInputFormatter.digitsOnly,
                            ],
                            decoration: InputDecoration(
                              hintText: 'Enter your e-KTP number here',
                              contentPadding:
                                  EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20.0)),
                            ),
                            validator: validateNIK,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: new Container(
                                alignment: Alignment.topLeft,
                                child: new Text("Verification Options",
                                    style: TextStyle(fontSize: 16))),
                          ),
                          new SizedBox(
                            height: 30,
                            child: new CheckboxListTile(
                              controlAffinity: ListTileControlAffinity.leading,
                              value: _checkValueMotherName,
                              onChanged: (bool value) {
                                setState(() {
                                  _checkValueMotherName = value;
                                  if (_checkValueMotherName != null) {
                                    if (_checkValueMotherName) {
                                      if(_checkValueKK){
                                        setState(() {
                                          _checkValueKK = !_checkValueKK;
                                          _isHoverValueMotherName = !_isHoverValueMotherName;
                                          _isHoverValueKK = !_isHoverValueKK;
                                        });
                                      } else {
                                        setState(() {
                                          _isHoverValueMotherName = !_isHoverValueMotherName;
                                        });
                                      }
                                    } else {
                                      setState(() {
                                       _isHoverValueMotherName = true; 
                                      });
                                    }
                                  } else {
                                    _checkValueMotherName = false;
                                  }
                                });
                                // setState(() {
                                //   _checkValueMotherName = value;
                                // });
                              },
                              title: new Text("Mother's Maiden Name"),
                            ),
                          ),
                          new CheckboxListTile(
                            controlAffinity: ListTileControlAffinity.leading,
                            value: _checkValueKK,
                            onChanged: (bool value) {
                              setState(() {
                                _checkValueKK = value;
                                if (_checkValueKK != null) {
                                  if (_checkValueKK) {
                                    if(_checkValueMotherName){
                                      setState(() {
                                        _checkValueMotherName = !_checkValueMotherName;
                                        _isHoverValueMotherName = !_isHoverValueMotherName;
                                        _isHoverValueKK = !_isHoverValueKK;
                                      });
                                    } else {
                                      setState(() {
                                        _isHoverValueKK = !_isHoverValueKK;
                                      });
                                    }
                                  } else {
                                    setState(() {
                                     _isHoverValueKK = true; 
                                    });
                                  }
                                } else {
                                  _checkValueKK = false;
                                }
                              });
                              // setState(() {
                              //   _checkValueKK = value;
                              // });
                            },
                            title: new Text("Kartu Keluarga Number"),
                          ),
                          _isHoverValueMotherName
                              ? new Container()
                              : formMotherName,
                          _isHoverValueKK ? new Container() : formKK,
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: new Divider(
                              color: Colors.black,
                              height: 1,
                            ),
                          ),
                          new Align(
                            alignment: Alignment.topRight,
                            child: new ButtonTheme(
                              minWidth: 80.0,
                              child: new RaisedButton(
                                color: Color(0xff007022),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                onPressed: _submitVerEktp,
                                child: Text("Verify",
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 16)),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )),
                new Visibility(
                  maintainSize: true,
                  maintainAnimation: true,
                  maintainState: true,
                  visible: _viewVisibleAboutYou,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Container(
                      color: Colors.white,
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: new Form(
                          key: formKey2,
                          child: new Column(
                            children: <Widget>[
                              new Container(
                                  alignment: Alignment.topLeft,
                                  child: new Row(
                                    children: <Widget>[
                                      new Text("Company/Instution Name ",
                                          style: TextStyle(fontSize: 16)),
                                      new Text(
                                        "*",
                                        style: TextStyle(color: Colors.red),
                                      )
                                    ],
                                  )),
                              new TextFormField(
                                controller: txtControllerCompanyName,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                  hintText: 'Enter company name here',
                                  contentPadding: EdgeInsets.symmetric(
                                      vertical: 7, horizontal: 10),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(20.0)),
                                ),
                                validator: validateAll,
                              ),
                              new Container(
                                  alignment: Alignment.topLeft,
                                  child: new Row(
                                    children: <Widget>[
                                      new Text("Company/Instution Address ",
                                          style: TextStyle(fontSize: 16)),
                                      new Text(
                                        "*",
                                        style: TextStyle(color: Colors.red),
                                      )
                                    ],
                                  )),
                              new TextFormField(
                                controller: txtControllerCompanyAddress,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                  hintText: 'Enter company address here',
                                  contentPadding: EdgeInsets.symmetric(
                                      vertical: 7, horizontal: 10),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(20.0)),
                                ),
                                validator: validateAll,
                              ),
                              new Container(
                                  alignment: Alignment.topLeft,
                                  child: new Row(
                                    children: <Widget>[
                                      new Text("Company/Instution Province ",
                                          style: TextStyle(fontSize: 16)),
                                      new Text(
                                        "*",
                                        style: TextStyle(color: Colors.red),
                                      )
                                    ],
                                  )),
                              new FormField<String>(
                                // validator: validateAll
                              builder: (FormFieldState<String> state) {
                              return InputDecorator(
                                decoration: InputDecoration(
                                  contentPadding: new EdgeInsets.symmetric(
                                      horizontal: 10.0),
                                  border: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.circular(20.0)),
                                ),
                                child: DropdownButtonHideUnderline(
                                  child: new DropdownButton(
                                    hint: new Text("Choose Company Province"),
                                    items: <String>[
                                      'Aceh',
                                      'Bengkulu',
                                      'Jambi',
                                      'Kepulauan Bangka Belitung',
                                      'Kepulauan Riau',
                                      'Lampung',
                                      'Riau',
                                      'Sumatra Barat',
                                      'Sumatra Selatan',
                                      'Sumatra Utara',
                                      'Banten',
                                      'Gorontalo',
                                      'Jakarta',
                                      'Jawa Barat',
                                      'Jawa Tengah',
                                      'Jawa Timur',
                                      'Kalimantan Barat',
                                      'Kalimantan Selatan',
                                      'Kalimantan Tengah',
                                      'Kalimantan TImur',
                                      'Kalimantan Utara',
                                      'Maluku',
                                      'Maluku Utara',
                                      'Bali',
                                      'Nusa Tenggara Barat',
                                      'Nusa Tenggara Timur',
                                      'Papua',
                                      'Papua Barat',
                                      'Sulawesi Barat',
                                      'Sulawesi Selatan',
                                      'Sulawesi Tengah',
                                      'Sulawesi Tenggara',
                                      'Sulawesi Utara',
                                      'Yogyakarta'
                                    ].map((String value) {
                                      return new DropdownMenuItem<String>(
                                        value: value,
                                        child: new Row(
                                          children: <Widget>[
                                            new Text(value),
                                          ],
                                        ),
                                      );
                                    }).toList(),
                                    value: dropdownSelectedItemCP,
                                    onChanged: (val) {
                                      dropdownSelectedItemCP = val;
                                        setState(() {
                                          checkCompProv = dropdownSelectedItemCP;
                                          print(
                                              "Choose Company Province: $dropdownSelectedItemCP");
                                        });
                                    },
                                  ),
                                ),
                              );
                            },
                          ),
                          // new TextFormField(
                          //   keyboardType: TextInputType.number,
                          //   inputFormatters: <TextInputFormatter>[
                          //     WhitelistingTextInputFormatter.digitsOnly,
                          //   ],
                          //   decoration: InputDecoration(
                          //     hintText: 'Jawa Barat',
                          //     contentPadding:
                          //         EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                          //     border: OutlineInputBorder(
                          //         borderRadius: BorderRadius.circular(20.0)),
                          //   ),
                          // ),
                          new Container(
                              alignment: Alignment.topLeft,
                              child: new Row(
                                children: <Widget>[
                                  new Text("Company/Instution District ",
                                      style: TextStyle(fontSize: 16)),
                                  new Text(
                                    "*",
                                    style: TextStyle(color: Colors.red),
                                  )
                                ],
                              )),
                          new TextFormField(
                            controller: txtControllerCompanyDistrict,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              hintText: 'Enter your company district',
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 7, horizontal: 10),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20.0)),
                            ),
                            validator: validateAll,
                          ),
                          new Container(
                              alignment: Alignment.topLeft,
                              child: new Row(
                                children: <Widget>[
                                  new Text("Purpose of Account Opening ",
                                      style: TextStyle(fontSize: 16)),
                                  new Text(
                                    "*",
                                    style: TextStyle(color: Colors.red),
                                  )
                                ],
                              )),
                          new FormField<String>(
                            // validator: validateAll,
                            builder: (FormFieldState<String> state) {
                              return InputDecorator(
                                decoration: InputDecoration(
                                  contentPadding: new EdgeInsets.symmetric(
                                      horizontal: 10.0),
                                  border: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.circular(20.0)),
                                ),
                                child: DropdownButtonHideUnderline(
                                  child: new DropdownButton<Pao>(
                                    value: selectedPAO,
                                    onChanged: (Pao val) {
                                      setState(() {
                                        selectedPAO = val;
                                        checkPurpose = selectedPAO.name;
                                        print("Id: ${selectedPAO.id}");
                                      });
                                    },
                                    hint:
                                        new Text("Choose Purpose Account Open"),
                                    items:
                                        purposeAccountOpening.map((Pao value) {
                                      return new DropdownMenuItem<Pao>(
                                        value: value,
                                        child: new Row(
                                          children: <Widget>[
                                            new Text("${value.name}"),
                                          ],
                                        ),
                                      );
                                    }).toList(),
                                    // value: dropdownSelectedItemPOAO,
                                  ),
                                ),
                              );
                            },
                          ),
                          // new TextFormField(
                          //   keyboardType: TextInputType.number,
                          //   inputFormatters: <TextInputFormatter>[
                          //     WhitelistingTextInputFormatter.digitsOnly,
                          //   ],
                          //   decoration: InputDecoration(
                          //     hintText: 'Lain-Lain',
                          //     contentPadding:
                          //         EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                          //     border: OutlineInputBorder(
                          //         borderRadius: BorderRadius.circular(20.0)),
                          //   ),
                          // ),
                          new Container(
                              alignment: Alignment.topLeft,
                              child: new Row(
                                children: <Widget>[
                                  new Text("Range of Funds ",
                                      style: TextStyle(fontSize: 16)),
                                  new Text(
                                    "*",
                                    style: TextStyle(color: Colors.red),
                                  )
                                ],
                              )),
                          new FormField<String>(
                            // validator: validateAll,
                            builder: (FormFieldState<String> state) {
                              return InputDecorator(
                                decoration: InputDecoration(
                                  contentPadding: new EdgeInsets.symmetric(
                                      horizontal: 10.0),
                                  border: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.circular(20.0)),
                                ),
                                child: DropdownButtonHideUnderline(
                                  child: new DropdownButton<Rof>(
                                    value: selectedROF,
                                    onChanged: (Rof val) {
                                      setState(() {
                                        selectedROF = val;
                                        checkROF = selectedROF.name;
                                        print("Id: ${selectedROF.id}");
                                      });
                                    },
                                    hint: new Text("Choose Range Of Funds"),
                                    items: rangeOfFund.map((Rof value) {
                                      return new DropdownMenuItem<Rof>(
                                        value: value,
                                        child: new Row(
                                          children: <Widget>[
                                            new Text("${value.name}"),
                                          ],
                                        ),
                                      );
                                    }).toList(),
                                    // value: dropdownSelectedItemPOAO,
                                  ),
                                ),
                              );
                            },
                          ),
                          // new TextFormField(
                          //   keyboardType: TextInputType.number,
                          //   inputFormatters: <TextInputFormatter>[
                          //     WhitelistingTextInputFormatter.digitsOnly,
                          //   ],
                          //   decoration: InputDecoration(
                          //     hintText: '10 JT s/d 25 JT',
                          //     contentPadding:
                          //         EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                          //     border: OutlineInputBorder(
                          //         borderRadius: BorderRadius.circular(20.0)),
                          //   ),
                          // ),
                          new Container(
                              alignment: Alignment.topLeft,
                              child: new Row(
                                children: <Widget>[
                                  new Text("Range of Expense ",
                                      style: TextStyle(fontSize: 16)),
                                  new Text(
                                    "*",
                                    style: TextStyle(color: Colors.red),
                                  )
                                ],
                              )),
                          new FormField<String>(
                            // validator: validateAll,
                            builder: (FormFieldState<String> state) {
                              return InputDecorator(
                                decoration: InputDecoration(
                                  contentPadding: new EdgeInsets.symmetric(
                                      horizontal: 10.0),
                                  border: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.circular(20.0)),
                                ),
                                child: DropdownButtonHideUnderline(
                                  child: new DropdownButton<Roe>(
                                    value: selectedROE,
                                    onChanged: (Roe val) {
                                      setState(() {
                                        selectedROE = val;
                                        checkROE = selectedROE.name;
                                        print("Id: ${selectedROE.id}");
                                      });
                                    },
                                    hint: new Text("Choose Range Of Expenses"),
                                    items: rangeOfExpense.map((Roe value) {
                                      return new DropdownMenuItem<Roe>(
                                        value: value,
                                        child: new Row(
                                          children: <Widget>[
                                            new Text("${value.name}"),
                                          ],
                                        ),
                                      );
                                    }).toList(),
                                    // value: dropdownSelectedItemPOAO,
                                  ),
                                ),
                              );
                            },
                          ),
                          // new TextFormField(
                          //   keyboardType: TextInputType.number,
                          //   inputFormatters: <TextInputFormatter>[
                          //     WhitelistingTextInputFormatter.digitsOnly,
                          //   ],
                          //   decoration: InputDecoration(
                          //     hintText: '5 JT s/d 10 JT',
                          //     contentPadding:
                          //         EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                          //     border: OutlineInputBorder(
                          //         borderRadius: BorderRadius.circular(20.0)),
                          //   ),
                          // ),
                          new Container(
                              alignment: Alignment.topLeft,
                              child: new Row(
                                children: <Widget>[
                                  new Text("Source of Fund ",
                                      style: TextStyle(fontSize: 16)),
                                  new Text(
                                    "*",
                                    style: TextStyle(color: Colors.red),
                                  )
                                ],
                              )),
                          new FormField<String>(
                            // validator: validateAll,
                            builder: (FormFieldState<String> state) {
                              return InputDecorator(
                                decoration: InputDecoration(
                                  contentPadding: new EdgeInsets.symmetric(
                                      horizontal: 10.0),
                                  border: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.circular(20.0)),
                                ),
                                child: DropdownButtonHideUnderline(
                                  child: new DropdownButton<Sof>(
                                    value: selectedSOF,
                                    onChanged: (Sof val) {
                                      setState(() {
                                        selectedSOF = val;
                                        checkSOF = selectedSOF.name;
                                        print("Id: ${selectedSOF.id}");
                                      });
                                    },
                                    hint: new Text("Choose Source of Funds"),
                                    items: sourceOfFund.map((Sof value) {
                                      return new DropdownMenuItem<Sof>(
                                        value: value,
                                        child: new Row(
                                          children: <Widget>[
                                            new Text("${value.name}"),
                                          ],
                                        ),
                                      );
                                    }).toList(),
                                    // value: dropdownSelectedItemPOAO,
                                  ),
                                ),
                              );
                            },
                          ),
                          // new TextFormField(
                          //   keyboardType: TextInputType.number,
                          //   inputFormatters: <TextInputFormatter>[
                          //     WhitelistingTextInputFormatter.digitsOnly,
                          //   ],
                          //   decoration: InputDecoration(
                          //     hintText: 'Lainnya',
                          //     contentPadding:
                          //         EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                          //     border: OutlineInputBorder(
                          //         borderRadius: BorderRadius.circular(20.0)),
                          //   ),
                          // ),
                          new Container(
                              alignment: Alignment.topLeft,
                              child: new Row(
                                children: <Widget>[
                                  new Text("Beneficial Owner Name ",
                                      style: TextStyle(fontSize: 16)),
                                  new Text(
                                    "*",
                                    style: TextStyle(color: Colors.red),
                                  )
                                ],
                              )),
                          new TextFormField(
                            controller: txtControllerBeneficialOwnerName,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              hintText: 'Enter your name here',
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 7, horizontal: 10),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20.0)),
                            ),
                            validator: validateAll,
                          ),
                          new Container(
                              alignment: Alignment.topLeft,
                              child: new Row(
                                children: <Widget>[
                                  new Text("Beneficial Phone Number ",
                                      style: TextStyle(fontSize: 16)),
                                  new Text(
                                    "*",
                                    style: TextStyle(color: Colors.red),
                                  )
                                ],
                              )),
                          new TextFormField(
                            controller: txtControllerBeneficialPhoneNumber,
                            keyboardType: TextInputType.phone,
                            // inputFormatters: <TextInputFormatter>[
                            //   WhitelistingTextInputFormatter.digitsOnly,
                            // ],
                            decoration: InputDecoration(
                              hintText: 'Enter phone number here',
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 7, horizontal: 10),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20.0)),
                            ),
                            validator: validateAll,
                          ),
                          // new SizedBox(
                          //   height: 70,
                          // )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
      // new Container(
      //   child: new Stepper(
      //     currentStep: this._currentStepper,
      //     type: StepperType.horizontal,
      //     steps: steps,
      //   ),
      // ),
    );
  }

  _showdialogVerEktpSuccess(String msg) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: new AlertDialog(
          title: Center(child: Text("eKTP Verification")),
          content: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              new Text(
                msg,
                style: new TextStyle(fontSize: 16.0),
              ),
              new Text("--------------------"),
              new Text("User Data\n"),
              new Text("No KK: $valueNomorKartuKeluarga"),
              new Text("Full Name: $valueNamaLengkap"),
              new Text("Gender: $valueJenisKelamin"),
              new Text("Place Of Birth: $valueTempatLahir"),
              new Text("Date Of Birth: $valueTanggalLahir"),
              new Text("Maiden Name: $valueNamaIbu"),
              new Text("Address: $valueAlamat"),
              new Text("No RT: $valueRt"),
              new Text("No Rw: $valueRw"),
              new Text("Municipal: $valueNamaKelurahan"),
              new Text("District: $valueNamaKecamatan"),
              new Text("City: $valueNamaKabupaten"),
              new Text("Province: $valueNamaProvinsi"),
              //harusnya ada zipcode
              new Text("Religion: $valueAgama"),
              new Text("Martial Status: $valueStatusKawin"),
              new Text("Pendidikan: $valuePendidikan"),
              new Text("Job Title: $valuePekerjaan"),
            ],
          ),
          actions: <Widget>[
            new FlatButton(
                onPressed: () async {
                  Navigator.pop(context);
                },
                child: new Text("OK"))
          ],
        ));
  }

  _showdialogVerEktpFailed(String msg) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: new AlertDialog(
          title: Center(child: Text("eKTP Verification")),
          content: new Text(
            msg,
            style: new TextStyle(fontSize: 16.0),
          ),
          actions: <Widget>[
            new FlatButton(
                onPressed: () {
                  // Navigator.push(context, new MaterialPageRoute(builder: (context) => new HomeOnboarding()));
                  Navigator.pop(context);
                },
                child: new Text("OK")),
          ],
        ));
  }
}
