
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:wokee_platform/service/account/tempChangeOTP.dart';
import 'package:wokee_platform/service/register/otp_service.dart';
import 'package:wokee_platform/view/model/color_loader_2.dart';
import 'package:wokee_platform/view/register/user_registration.dart';
import 'package:wokee_platform/view/model/model.dart';
import 'package:wokee_platform/view/register/request_otp.dart';

class CifOTPPage extends StatefulWidget {
  CifOTPPage({Key key}) : super(key: key);
  @override
  _CifOTPPageState createState() => _CifOTPPageState();
}

class _CifOTPPageState extends State<CifOTPPage> {
  final formKey = GlobalKey<FormState>();

  void _submit() async {
    if (formKey.currentState.validate()) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return Center(
              child: ColorLoader2(),
            );
          });

      new Future.delayed(new Duration(seconds: 0), () async {
        //dismiss loader
        await tempSmsOtp();
        _navigateToUserOnBord();
        // Navigator.push(
        //     context,
        //     new MaterialPageRoute(
        //         builder: (context) => new UserRegistrationPage()));
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      // backgroundColor: Colors.red,
      appBar: new PreferredSize(
        preferredSize: Size(double.infinity, 50),
        child: new ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25)),
          child: new AppBar(
            backgroundColor: Color(0xff007022),
            title: new Center(
              child: new Text(
                "OTP Verification",
                style: TextStyle(color: Colors.white, fontSize: 25),
              ),
            ),
            actions: <Widget>[
              Padding(
                  padding: const EdgeInsets.all(8),
                  child: new Image.asset('assets/images/logo_wokee.png')),
            ],
          ),
        ),
      ),
      body: new Form(
        key: formKey,
        child: new ListView(
          padding: const EdgeInsets.fromLTRB(15, 25, 15, 0),
          children: <Widget>[
            new Text(
              "OTP has been sent to the registered\nmobile number. Please enter your OTP",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
              textAlign: TextAlign.center,
            ),
            new SizedBox(
              height: 25,
            ),
            new TextFormField(
              controller: txtControllerPhoneCif,
              keyboardType: TextInputType.phone,
              // maxLength: 6,
              // inputFormatters: <TextInputFormatter>[
              //   WhitelistingTextInputFormatter.digitsOnly,
              // ], //
              decoration: InputDecoration(
                labelText: 'Enter your phone number here',
                prefixIcon: Icon(Icons.security),
                contentPadding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30.0)),
              ),
              // validator: validateOTP,
            ),
            new SizedBox(
              height: 20,
            ),
            new RaisedButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              color: Color(0xff007022),
              // onPressed: (){
              //   Navigator.push(
              //               context,
              //               new MaterialPageRoute(
              //                   builder: (context) =>
              //                       new UserRegistrationPage()));
              // },
              onPressed: _submit,
              // onPressed: () {
              //   Navigator.push(
              //       context,
              //       new MaterialPageRoute(
              //           builder: (context) => new UserRegistrationPage()));
              // },
              child: new Text(
                "OK",
                style: TextStyle(color: Colors.white, fontSize: 17, fontWeight: FontWeight.bold),
              ),
            ),
            new SizedBox(
              height: 15,
            ),
            new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Text(
                  "You haven't got the OTP code yet? ",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                new GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (context) => new RequestOTPPage()));
                    },
                    child: new Text(
                      "Send OTP code again",
                      style: TextStyle(color: Colors.blue),
                    ))
              ],
            ),
            new SizedBox(
              height: 15,
            ),
            new Text(
              "Please contact Halo Bukopin 14005\nif your mobile number is no longer active",
              style: TextStyle(color: Colors.grey),
              textAlign: TextAlign.center,
            )
          ],
        ),
      ),
    );
  }

  _showdialog(String msg) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: new AlertDialog(
          // title: Text("Pesan"),
          content: new Text(
            msg,
            style: new TextStyle(fontSize: 16.0),
          ),
          actions: <Widget>[
            new FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                  // txtControllerEmail.clear();
                  // txtControllerPassword.clear();
                },
                child: new Text("OK"))
          ],
        ));
  }

  _navigateToUserOnBord() async {
    await new Future.delayed(const Duration(seconds: 0));
    // var route = new MaterialPageRoute(
    //   builder: (BuildContext context) => new UserRegistrationPage(),
    // );
    if (valueStatusCodeTempOTP == '0') {
      print("OTP Verificarion Successful");
      Navigator.pop(context); //dismiss loader
      Fluttertoast.showToast(
          msg: "OTP Verificarion Successful",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 1,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);
      Navigator.pop(context);
      // Navigator.of(context).push(route);
    } else if (valueCode == "1") {
      print("OTP Verificarion Failed");
      Navigator.pop(context); //Failed loader
      // _showdialog("SMS OTP belum dibuat");
      _showdialog("OTP SMS has not been created");
    } 
  }
}
