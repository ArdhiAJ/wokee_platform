import 'package:flutter/material.dart';
import 'package:wokee_platform/view/home/main_menu.dart';
import 'package:wokee_platform/view/model/model.dart';
import 'package:wokee_platform/view/onboarding/final_onboarding.dart';
import 'package:wokee_platform/view/onboarding/onboarding_page.dart';
import 'package:wokee_platform/view/login/login_page.dart';
import 'package:wokee_platform/view/model/color_loader_2.dart';
import 'package:wokee_platform/service/homepage/logout_service.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:wokee_platform/view/onboarding/aboutyou2.dart';
import 'package:wokee_platform/view/onboarding/your_identity.dart';
import 'package:wokee_platform/service/login/atiQECUsername.dart';

class HomeOnboarding extends StatefulWidget {
  final CustomerIdHomeOnBoarding customerIdHomeOnBoarding;

  HomeOnboarding({Key key, this.customerIdHomeOnBoarding}) : super(key: key);
  @override
  _HomeOnboardingState createState() => _HomeOnboardingState();
}

class _HomeOnboardingState extends State<HomeOnboarding> {

  @override
  void initState() {
    super.initState();
    setState(()  {
      atiQCUsername();
    });
  }

  @override
  Widget build(BuildContext context) {
    // print("Check: $wac");
    // print("Uncheck: $wac2");
    // print("Customer ID (Home_Onboarding): ${widget.customerIdHomeOnBoarding.customerId}");
    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      // backgroundColor: Colors.red,
      appBar: new PreferredSize(
        preferredSize: Size(double.infinity, 50),
        child: new ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25)),
          child: new AppBar(
            backgroundColor: Color(0xff007022),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: new Center(
                  child: new Text(
                    "Home",
                    style: TextStyle(color: Colors.white, fontSize: 30),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      body: new ListView(
        children: <Widget>[
          new Card(
            margin: const EdgeInsets.fromLTRB(0, 0, 0, 0),
            color: Colors.white,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(0)),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
              child: new Column(
                children: <Widget>[
                  new Align(
                    alignment: Alignment.topLeft,
                    child: new Image.asset(
                      'assets/images/logo_wokee.png',
                      width: 50,
                    ),
                  ),
                  new Align(
                    alignment: Alignment.bottomRight,
                    child: new Text(
                      "Welcome $valueName",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Colors.black87),
                    ),
                  ),
                  new Align(
                      alignment: Alignment.bottomRight,
                      child: new Text(
                        "Last Login $valueLastTimeLogin",
                        style: TextStyle(fontSize: 16, color: Colors.black54),
                      )),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10, right: 10),
            child: new RaisedButton(
                color: Color(0xff007022),
                shape: new RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(7)),
                onPressed: () {
                  String customerIdHomeOnBoarding =
                      "${widget.customerIdHomeOnBoarding.customerId}";
                  CustomerIdOnBoardingPage customerIdHome =
                      new CustomerIdOnBoardingPage(customerIdHomeOnBoarding);
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (context) => new OnboardingPage(
                                customerIdOnBoarding: customerIdHome,
                              )));

                  // Navigator.push(
                  //     context,
                  //     new MaterialPageRoute(
                  //         builder: (context) => new YourIdentityPage()));
                },
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Icon(
                      Icons.assignment,
                      color: Colors.white,
                    ),
                    new SizedBox(
                      width: 10,
                    ),
                    new Text(
                      "Onboarding",
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    )
                  ],
                )),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10, right: 10),
            child: new RaisedButton(
                color: Color(0xff007022),
                shape: new RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(7)),
                onPressed: () async {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return Center(
                          child: ColorLoader2(),
                        );
                      });
                  new Future.delayed(new Duration(seconds: 0), () async {
                    await logoutUser();
                    _nextLogout();
                    // Navigator.pushReplacement(context,
                    // new MaterialPageRoute(builder: (context) => new MyLoginPage(new BasicAppPresenter())));
                  });
                },
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Icon(
                      Icons.assignment,
                      color: Colors.white,
                    ),
                    new SizedBox(
                      width: 10,
                    ),
                    new Text(
                      "Logout",
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    )
                  ],
                )),
          ),
          // Padding(
          //   padding: const EdgeInsets.only(left: 10, right: 10),
          //   child: new RaisedButton(
          //     color: Color(0xff007022),
          //     shape: new RoundedRectangleBorder(
          //       borderRadius: BorderRadius.circular(7)
          //     ),
          //     onPressed: (){
          //       Navigator.push(context, new MaterialPageRoute(builder: (context) => new MainMenuController()));
          //     },
          //     child: new Row(
          //       mainAxisAlignment: MainAxisAlignment.center,
          //       children: <Widget>[
          //         new Icon(Icons.assignment, color: Colors.white,),
          //         new SizedBox(
          //           width: 10,
          //         ),
          //         new Text("Home", style: TextStyle(color: Colors.white, fontSize: 16),)
          //       ],
          //     )
          //   ),
          // )
        ],
      ),
    );
  }

  _nextLogout() async {
    await new Future.delayed(const Duration(seconds: 3));
    if (valueCode == '0') {
      print("Logout User Success!");
      Navigator.pop(context);
      Fluttertoast.showToast(
          msg: "Logout Successfull",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 1,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);
      // txtControllerPassword.clear();
      Navigator.pushReplacement(context,
          new MaterialPageRoute(builder: (context) => new LoginPage()));
      txtControllerPassword.clear();
    } else {
      print("Logout User Failed!");
      Navigator.pop(context);
      _showdialog("Message: $valueMessage");
    }
  }

  _showdialog(String msg) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: new AlertDialog(
          // title: Text("Pesan"),
          content: new Text(
            msg,
            style: new TextStyle(fontSize: 16.0),
          ),
          actions: <Widget>[
            new FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                  txtControllerEmail.clear();
                  txtControllerPassword.clear();
                },
                child: new Text("OK"))
          ],
        ));
  }
}
