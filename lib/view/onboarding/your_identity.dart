import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:signature/signature.dart';
import 'package:http/http.dart' as http;
import 'package:wokee_platform/view/model/color_loader_2.dart';
import 'package:wokee_platform/view/model/model.dart';
import 'package:wokee_platform/view/login/login_page.dart';
import 'package:wokee_platform/view/onboarding/your_identity_service.dart';
import 'package:path_provider/path_provider.dart';
import 'package:wokee_platform/view/onboarding/local_storage.dart';
import 'package:wokee_platform/view/onboarding/final_onboarding.dart';

class YourIdentityPage extends StatefulWidget {
  final EktpProduct ektp3;
  final CustomerIdYourIdentity customerIdYourIdentity;

  YourIdentityPage({Key key, this.ektp3, this.customerIdYourIdentity})
      : super(key: key);

  @override
  _YourIdentityPageState createState() => _YourIdentityPageState();
}

class _YourIdentityPageState extends State<YourIdentityPage> {
  File _photoImage;
  File _photoEKTP;
  File _photoNPWP;
  int _currentStep = 0;
  bool _checkNpwp = false;
  bool _isHoverNpwp = true;
  // String newFileName = "";
  final SignatureController _controllerCanvas =
      SignatureController(penStrokeWidth: 2, penColor: Colors.black);

  // final String path = await getApplicationDocumentsDirectory().path;
  // final File newImage = await image.copy('$path/image1.png');

  // Future saveImage() async {
  //   await LocalStorage().saveImageLocal(
  //     photoImage: photoImage,
  //     newFilename: newFileName,
  //   );
  // }

  Future getPhotoImageFromCamera() async {
    final File image = await ImagePicker.pickImage(
        source: ImageSource.camera, maxHeight: 480, maxWidth: 640);
    setState(() {
      _photoImage = image;
      // print("Path File: $image");
    });
  }

  Future getEktpImageFromCamera() async {
    var image = await ImagePicker.pickImage(
        source: ImageSource.camera, maxHeight: 480, maxWidth: 640);
    setState(() {
      _photoEKTP = image;
    });
  }

  Future getEktpImageFromGallery() async {
    var image = await ImagePicker.pickImage(
        source: ImageSource.gallery, maxHeight: 480, maxWidth: 640);
    setState(() {
      _photoEKTP = image;
    });
  }

  Future getNpwpImageFromCamera() async {
    var image = await ImagePicker.pickImage(
        source: ImageSource.camera, maxHeight: 480, maxWidth: 640);
    setState(() {
      _photoNPWP = image;
    });
  }

  Future getNpwpImageFromGallery() async {
    var image = await ImagePicker.pickImage(
        source: ImageSource.gallery, maxHeight: 480, maxWidth: 640);
    setState(() {
      _photoNPWP = image;
    });
  }

  void printWrapped(String text) {
    final pattern = RegExp('.{1,800}'); // 800 is the size of each chunk
    pattern.allMatches(text).forEach((match) => print(match.group(0)));
  }

  base64EncodePhoto() {
    if (_photoImage != null) {
      List<int> imageBytes = _photoImage.readAsBytesSync();
      // print("Image Bytes: $imageBytes");
      String base64Photo = base64Encode(imageBytes);
      // print("Base64 Image(Encode): $base64Image");
      // printWrapped("Base64 Image(Encode): $base64Photo");
      print("Path Photo: $_photoImage");
      return base64Photo;
    } else {
      print("Tidak diconvert");
    }
  }

  base64EncodeEktp() {
    if (_photoEKTP != null) {
      List<int> imageBytes = _photoEKTP.readAsBytesSync();
      // print("Image Bytes: $imageBytes");
      String base64Ektp = base64Encode(imageBytes);
      // print("Base64 Image(Encode): $base64Image");
      // printWrapped("Base64 Image(Encode): $base64Image");
      print("Path EKTP: $_photoEKTP");
      return base64Ektp;
    } else {
      print("Tidak diconvert");
    }
  }

  base64EncodeNpwp() {
    if (_photoNPWP != null) {
      List<int> imageBytes = _photoNPWP.readAsBytesSync();
      // print("Image Bytes: $imageBytes");
      String base64Npwp = base64Encode(imageBytes);
      // print("Base64 Image(Encode): $base64Image");
      // printWrapped("Base64 Image(Encode): $base64Image");
      print("Path NPWP: $_photoNPWP");
      return base64Npwp;
    } else {
      print("Tidak diconvert");
    }
  }

  void _submitPicture() async {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Center(
            child: ColorLoader2(),
          );
        });

    new Future.delayed(new Duration(seconds: 0), () async {
      await serviceImage();
      if (valueStatus == 'SUCCESS') {
        // _navigateOnboarding();
        String nomorKk = "${widget.ektp3.nomorKK}";
        // print("Nomor KK: $nomorKk");
        String nomorEktp = "${widget.ektp3.nomorEktp}";
        // print("Nomor EKTP: $nomorEktp");
        String namaLengkap = "${widget.ektp3.namaLengkap}";
        // print("Nama Lengkap: $namaLengkap");
        String jenisKelamin = "${widget.ektp3.jenisKelamin}";
        // print("Jenis Kelamin: $jenisKelamin");
        String tempatLahir = "${widget.ektp3.tempatLahir}";
        // print("Tempat Lahir: $tempatLahir");
        String tanggalLahir = "${widget.ektp3.tanggalLahir}";
        String alamat = "${widget.ektp3.alamat}";
        // print("Nomor KK: $tanggalLahir");
        String nomorRt = "${widget.ektp3.nomorRt}";
        // print("Nomor RT: $nomorRt");
        String nomorRw = "${widget.ektp3.nomorRw}";
        // print("Nomor RW: $nomorRw");
        String kota = "${widget.ektp3.kota}";
        // print("Kota: $kota");
        String provinsi = "${widget.ektp3.provinsi}";
        // print("Provinsi: $provinsi");
        String kecamatan = "${widget.ektp3.kecamatan}";
        // print("Kecamatan: $kecamatan");
        String kelurahan = "${widget.ektp3.kelurahan}";
        // print("Kelurahan: $kelurahan");
        String agama = "${widget.ektp3.agama}";
        // print("Agama: $agama");
        String statusPernikahan = "${widget.ektp3.statusPernikahan}";
        // print("Status Pernikahan: $statusPernikahan");
        String pekerjaan = "${widget.ektp3.pekerjaan}";
        // print("Pekerjaan: $pekerjaan");
        String namaIbu = "${widget.ektp3.namaIbu}";
        String pendidikan = "${widget.ektp3.pendidikan}";
        String nomorProvinsi = "${widget.ektp3.nomorProvinsi}";
        String nomorKabupaten = "${widget.ektp3.nomorKabupaten}";
        EktpAgreement ektp4 = new EktpAgreement(
            nomorKk,
            nomorEktp,
            namaLengkap,
            jenisKelamin,
            tempatLahir,
            tanggalLahir,
            alamat,
            nomorRt,
            nomorRw,
            kota,
            provinsi,
            kecamatan,
            kelurahan,
            agama,
            statusPernikahan,
            pekerjaan,
            namaIbu,
            pendidikan,
            nomorProvinsi,
            nomorKabupaten);

        String nomorKk2 = "${widget.ektp3.nomorKK}";
        // print("Nomor KK: $nomorKk");
        String nomorEktp2 = "${widget.ektp3.nomorEktp}";
        // print("Nomor EKTP: $nomorEktp");
        String namaLengkap2 = "${widget.ektp3.namaLengkap}";
        // print("Nama Lengkap: $namaLengkap");
        String jenisKelamin2 = "${widget.ektp3.jenisKelamin}";
        // print("Jenis Kelamin: $jenisKelamin");
        String tempatLahir2 = "${widget.ektp3.tempatLahir}";
        // print("Tempat Lahir: $tempatLahir");
        String tanggalLahir2 = "${widget.ektp3.tanggalLahir}";
        String alamat2 = "${widget.ektp3.alamat}";
        // print("Nomor KK: $tanggalLahir");
        String nomorRt2 = "${widget.ektp3.nomorRt}";
        // print("Nomor RT: $nomorRt");
        String nomorRw2 = "${widget.ektp3.nomorRw}";
        // print("Nomor RW: $nomorRw");
        String kota2 = "${widget.ektp3.kota}";
        // print("Kota: $kota");
        String provinsi2 = "${widget.ektp3.provinsi}";
        // print("Provinsi: $provinsi");
        String kecamatan2 = "${widget.ektp3.kecamatan}";
        // print("Kecamatan: $kecamatan");
        String kelurahan2 = "${widget.ektp3.kelurahan}";
        // print("Kelurahan: $kelurahan");
        String agama2 = "${widget.ektp3.agama}";
        // print("Agama: $agama");
        String statusPernikahan2 = "${widget.ektp3.statusPernikahan}";
        // print("Status Pernikahan: $statusPernikahan");
        String pekerjaan2 = "${widget.ektp3.pekerjaan}";
        // print("Pekerjaan: $pekerjaan");
        String namaIbu2 = "${widget.ektp3.namaIbu}";
        String pendidikan2 = "${widget.ektp3.pendidikan}";
        String nomorProvinsi2 = "${widget.ektp3.nomorProvinsi}";
        String nomorKabupaten2 = "${widget.ektp3.nomorKabupaten}";
        EktpFinalOnboading ektp5 = new EktpFinalOnboading(
            nomorKk2,
            nomorEktp2,
            namaLengkap2,
            jenisKelamin2,
            tempatLahir2,
            tanggalLahir2,
            alamat2,
            nomorRt2,
            nomorRw2,
            kota2,
            provinsi2,
            kecamatan2,
            kelurahan2,
            agama2,
            statusPernikahan2,
            pekerjaan2,
            namaIbu2,
            pendidikan2,
            nomorProvinsi2,
            nomorKabupaten2);

        String customerIdYourIdentityPage =
            "${widget.customerIdYourIdentity.customerId}";
        CustomerIdFinalOnboarding customerIdYourIdentity =
            new CustomerIdFinalOnboarding(customerIdYourIdentityPage);

        Navigator.push(
            context,
            new MaterialPageRoute(
                builder: (context) => new FinalOnboardingPage(
                    ektpAgreement: ektp4,
                    ektpFinalOnBoarding: ektp5,
                    customerIdFinalOnBoarding: customerIdYourIdentity)));
      }
      // Navigator.pop(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    // print(
    //     "Customer ID (Your Identity): ${widget.customerIdYourIdentity.customerId}");
    // String emailOnBoarding = "${txtControllerEmail.text}";
    // print("Email On Boarding: $emailOnBoarding");
    // String nomorKk = "${widget.ektp3.nomorKK}";
    // print("Nomor KK: $nomorKk");
    // String nomorEktp = "${widget.ektp3.nomorEktp}";
    // print("Nomor EKTP: $nomorEktp");
    // String namaLengkap = "${widget.ektp3.namaLengkap}";
    // print("Nama Lengkap: $namaLengkap");
    // String jenisKelamin = "${widget.ektp3.jenisKelamin}";
    // print("Jenis Kelamin: $jenisKelamin");
    // String tempatLahir = "${widget.ektp3.tempatLahir}";
    // print("Tempat Lahir: $tempatLahir");
    // String tanggalLahir = "${widget.ektp3.tanggalLahir}";
    // print("Tanggal Lahir: $tanggalLahir");
    // String namaIbu = "${widget.ektp3.namaIbu}";
    // print("Nama Ibu: $namaIbu");
    // String alamat = "${widget.ektp3.alamat}";
    // print("Alamat: $alamat");
    // String nomorRt = "${widget.ektp3.nomorRt}";
    // print("Nomor RT: $nomorRt");
    // String nomorRw = "${widget.ektp3.nomorRw}";
    // print("Nomor RW: $nomorRw");
    // String kota = "${widget.ektp3.kota}";
    // print("Kota: $kota");
    // String provinsi = "${widget.ektp3.provinsi}";
    // print("Provinsi: $provinsi");
    // String kecamatan = "${widget.ektp3.kecamatan}";
    // print("Kecamatan: $kecamatan");
    // String kelurahan = "${widget.ektp3.kelurahan}";
    // print("Kelurahan: $kelurahan");
    // String agama = "${widget.ektp3.agama}";
    // print("Agama: $agama");
    // String statusPernikahan = "${widget.ektp3.statusPernikahan}";
    // print("Status Pernikahan: $statusPernikahan");
    // String pekerjaan = "${widget.ektp3.pekerjaan}";
    // print("Pekerjaan: $pekerjaan");
    // String pendidikan = "${widget.ektp3.pendidikan}";
    // print("Pendidikan: $pendidikan");
    // print("BusinessType: ${selectedBT.id}");
    // print("PurposeAccountOpen: ${selectedPAO.id}");
    // print("SourceOfFund: ${selectedSOF.id}");
    // print("RangeOfFund: ${selectedROF.id}");
    // print("RangeOfExpense: ${selectedROE.id}");
    // print("BenOwnerName: ${txtControllerBeneficialOwnerName.text}");
    // print("BenOwnerMobileNo: ${txtControllerBeneficialPhoneNumber.text}");
    // print("ProductAC: ${txtControllerProduct.text}");
    // print("JobType: $pekerjaan");
    // print("JobPos: ${txtControllerJobPosition.text}");
    // print("CompName: ${txtControllerCompanyName.text}");
    // print("CompAddr: ${txtControllerCompanyAddress.text}");
    // String nomorProvinsi = "${widget.ektp3.nomorProvinsi}";
    // String nomorKabupaten = "${widget.ektp3.nomorKabupaten}";
    // print("CompProv: $nomorProvinsi");
    // String compCity = "$nomorProvinsi"+"$nomorKabupaten";
    // print("CompCity: $compCity");

    final _uploadPhoto = new Column(
      children: <Widget>[
        new InkWell(
            onTap: () {
              getPhotoImageFromCamera();
            },
            child: new Row(
              children: <Widget>[
                new Icon(
                  Icons.add_a_photo,
                  color: Color(0xff007022),
                ),
                new Text("Take Photo")
              ],
            )),
      ],
    );

    final _uploadEKTP = new Column(
      children: <Widget>[
        new Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            new InkWell(
              onTap: getEktpImageFromCamera,
              child: new Row(
                children: <Widget>[
                  new Icon(
                    Icons.add_a_photo,
                    color: Color(0xff007022),
                  ),
                  new Text("Take E-KTP"),
                ],
              ),
            ),
            new InkWell(
              onTap: getEktpImageFromGallery,
              child: new Row(
                children: <Widget>[
                  new Icon(
                    Icons.collections,
                    color: Color(0xff007022),
                  ),
                  new Text("From Gallery")
                ],
              ),
            )
          ],
        ),
      ],
    );

    final checkboxCaptureNpwp = new CheckboxListTile(
      value: _checkNpwp,
      onChanged: (bool value) {
        setState(() {
          _checkNpwp = value;
          if (_checkNpwp != null) {
            if (_checkNpwp) {
              setState(() {
                _isHoverNpwp = !_isHoverNpwp;
              });
            } else {
              setState(() {
                _isHoverNpwp = true;
                // _isHoverUncheckNpwp ? new Text("No Image Selected") : new Text("No Image Selected");
                //disable container photo
              });
            }
          } else {
            _checkNpwp = false;
          }
        });
      },
      title: new Text(
        "Capture NPWP Photo",
        style: TextStyle(color: Colors.black, fontSize: 14),
      ),
      controlAffinity: ListTileControlAffinity.leading,
    );

    final agreementNpwp = new Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        new Text("If you bring the npwp please check the\ncheck box above"),
        new SizedBox(
          height: 10,
        ),
        new Text(
            "If no, you agree to submit the details to the\nnearest branch at a later time")
      ],
    );

    final _uploadNpwp = new Column(
      children: <Widget>[
        new Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            new InkWell(
              onTap: getNpwpImageFromCamera,
              child: new Row(
                children: <Widget>[
                  new Icon(
                    Icons.add_a_photo,
                    color: Color(0xff007022),
                  ),
                  new Text("Take E-KTP"),
                ],
              ),
            ),
            new InkWell(
              onTap: getNpwpImageFromGallery,
              child: new Row(
                children: <Widget>[
                  new Icon(
                    Icons.collections,
                    color: Color(0xff007022),
                  ),
                  new Text("From Gallery")
                ],
              ),
            )
          ],
        ),
      ],
    );

    final _signatureCanvas = Signature(
      controller: _controllerCanvas,
      // width: MediaQuery.of(context).size.width,
      width: MediaQuery.of(context).size.width * 0.7,
      height: 100,
      backgroundColor: Colors.white,
    );

    final _uploadSignature = new Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        new Container(
            // margin: const EdgeInsets.fromLTRB(30, 0, 30, 0),
            height: 160,
            decoration: BoxDecoration(
                border: Border.all(color: Colors.black, width: 2),
                borderRadius: BorderRadius.circular(10)),
            child: new Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: _signatureCanvas,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 35, right: 35),
                  child: new Divider(
                    color: Colors.grey,
                    thickness: 2,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10, right: 7),
                  child: new Container(
                    alignment: Alignment.bottomRight,
                    child: new InkWell(
                      onTap: () {
                        setState(() {
                          _controllerCanvas.clear();
                        });
                      },
                      child: new Text(
                        "reset",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ),
              ],
            )),
      ],
    );

    List<Step> steps = [
      new Step(
        title: const Text('Upload Your Photo'),
        // isActive: true,
        content: Column(
          children: <Widget>[
            new Container(
              width: MediaQuery.of(context).size.width,
              height: 150.0,
              child: new Center(
                child: _photoImage == null
                    ? new Text("No Image Selected")
                    : Image.file(_photoImage),
              ),
            ),
            _uploadPhoto,
          ],
        ),
      ),
      new Step(
        title: const Text('Upload Your E-KTP'),
        // isActive: true,
        content: Column(
          children: <Widget>[
            new Container(
              width: MediaQuery.of(context).size.width,
              height: 150.0,
              child: new Center(
                child: _photoEKTP == null
                    ? new Text("No Image Selected")
                    : Image.file(_photoEKTP),
              ),
            ),
            _uploadEKTP,
          ],
        ),
      ),
      new Step(
        title: const Text('Upload Your NPWP ID'),
        // isActive: true,
        // state: StepState.complete,
        content: Column(
          children: <Widget>[
            checkboxCaptureNpwp,
            new Container(
              width: MediaQuery.of(context).size.width,
              height: 150.0,
              child: new Center(
                child: _photoNPWP == null
                    ? new Text("No Image Selected")
                    : Image.file(_photoNPWP),
              ),
            ),
            _isHoverNpwp ? agreementNpwp : _uploadNpwp,
          ],
        ),
      ),
      new Step(
        title: const Text('Draw Your Signature'),
        // isActive: true,
        // state: StepState.complete,
        content: Column(
          children: <Widget>[
            _uploadSignature,
          ],
        ),
      ),
    ];

    return Scaffold(
        backgroundColor: Color(0xFFF2F2F2),
        // backgroundColor: Colors.red,
        appBar: new PreferredSize(
          preferredSize: Size(double.infinity, 50),
          child: new ClipRRect(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(25),
                bottomRight: Radius.circular(25)),
            child: new AppBar(
              backgroundColor: Color(0xff007022),
              title: new Center(
                child: new Text(
                  "Onboarding",
                  style: TextStyle(color: Colors.white, fontSize: 30),
                ),
              ),
              actions: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(5),
                  child: new InkWell(
                      // color: Color(0xff007022),
                      onTap: () {
                        print("Photo Image: $_photoImage");
                        print("Photo EKTP: $_photoEKTP");
                        print("Photo Npwp: $_photoNPWP");
                        if (_photoImage == null) {
                          _showdialogSendImage(
                              "Please upload your selfie image, before continue this process");
                        } else if (_photoEKTP == null) {
                          _showdialogSendImage(
                              "Please upload your eKTP image, before continue this process");
                        } 
                        else if (_controllerCanvas.isEmpty) {
                          _showdialogSendImage(
                              "Please draw your signature, before continue this process");
                        } 
                        else if (_photoImage != null && _photoEKTP != null && _controllerCanvas.isNotEmpty) {
                          _submitPicture();
                        }
                      },
                      child: new Row(
                        children: <Widget>[
                          new Text(
                            "Next",
                            style: TextStyle(color: Colors.white, fontSize: 18),
                          ),
                          new Icon(
                            Icons.navigate_next,
                            color: Colors.white,
                            size: 30,
                          )
                        ],
                      )),
                )
              ],
            ),
          ),
        ),
        body: new ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: new Align(
                alignment: Alignment.center,
                child: new Text(
                  "Your Identity",
                  style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                ),
              ),
            ),
            new Container(
              child: new Stepper(
                currentStep: this._currentStep,
                steps: steps,
                type: StepperType.vertical,
                controlsBuilder: (BuildContext context,
                    {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
                  return Padding(
                    padding: const EdgeInsets.only(top: 15),
                    // child: Row(
                    //   children: <Widget>[
                    //     Container(
                    //       child: null,
                    //     ),
                    //     Container(
                    //       child: null,
                    //     ),
                    //   ],
                    // ),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new InkWell(
                          onTap: onStepCancel,
                          child: new Text(
                            "Cancel",
                            style: TextStyle(
                              color: Color(0xff007022),
                            ),
                          ),
                        ),
                        new SizedBox(
                          width: 50,
                        ),
                        new RaisedButton(
                            color: Color(0xff007022),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            onPressed: onStepContinue,
                            child: new Text(
                              "Continue",
                              style: TextStyle(color: Colors.white),
                            )),
                      ],
                    ),
                  );
                },
                onStepTapped: (step) {
                  setState(() {
                    _currentStep = step;
                  });
                },
                onStepContinue: () {
                  setState(() {
                    if (_currentStep < steps.length - 1) {
                      _currentStep = _currentStep + 1;
                    } else {
                      _currentStep = 0;
                    }
                  });
                },
                onStepCancel: () {
                  setState(() {
                    if (_currentStep > 0) {
                      _currentStep = _currentStep - 1;
                    } else {
                      _currentStep = 0;
                    }
                  });
                },
              ),
            ),
          ],
        ));
  }

  var valueStatusCode = "";
  var valueStatusDesc = "";
  var valueTrxId = "";
  var valueStatus = "";
  var valueFailed = "";
  var valueFailed2 = "";

  Future serviceImage() async {
    print("-->User Photo (Service)<--");
    String base64Photo = base64EncodePhoto();
    // print("Base64 (Photo): $base64Photo");
    String base64Ektp = base64EncodeEktp();
    // print("Base64 (Photo): $base64Ektp");
    String base64Npwp = base64EncodeNpwp();
    // print("Base64 (Photo): $base64Npwp");
    String base64Sign = base64EncodeNpwp();
    // print("Base64 (Photo): $base64Npwp");
    print("-->User Photo (Service)<--");
    print("Base64 Photo: $base64Photo");
    print("Base64 E-KTP: $base64Ektp");
    print("Base64 NPWP: $base64Npwp");
    print("Base64 Signature: $base64Npwp");

    String customerIdYourIdentityPage =
        "${widget.customerIdYourIdentity.customerId}";
    String photo = "$customerIdYourIdentityPage" + "_PHOTO.JPG";
    print("Photo: $photo");
    String ektp = "$customerIdYourIdentityPage" + "_EKTP.JPG";
    print("eKTP: $ektp");
    String npwp = "$customerIdYourIdentityPage" + "_NPWP.JPG";
    print("NPWP: $npwp");
    String sign = "$customerIdYourIdentityPage" + "_SIGN.JPG";
    print("SIGN: $sign");

    var url = "http://10.2.62.35:8080/wokee/okoce/uploadfile";
    print("URL: $url");

    Map dataBody = {
      "ektpFileName": "$ektp",
      "ektpImage": "$base64Ektp",
      "npwpFileName": "$npwp",
      "npwpImage": "$base64Npwp",
      "signFileName": "$npwp",
      "signImage": "$base64Npwp",
      "photoFileName": "$photo",
      "photoImage": "$base64Photo"
    };

    var body = json.encode(dataBody);

    final response = await http.post(url,
        headers: {"Content-Type": "application/json"}, body: body);

    final int statusCode = response.statusCode;

    if (statusCode == 200) {
      print("Connection Success!");
      print("Status Code: $statusCode");
      var result = json.decode(response.body);
      var result1 = result['Status']['statusCode'];
      var result2 = result['Status']['statusDesc'];
      var result3 = result['Data']['trxId'];
      var result4 = result['Data']['status'];

      print("Status Code: $result1");
      print("status Desc: $result2");
      print("Data (Trx ID): $result3");
      print("Data (Status): $result4");

      valueStatusCode = result1.toString();
      valueStatusDesc = result2.toString();
      valueTrxId = result3.toString();
      valueStatus = result4.toString();
      return valueStatusCode + valueStatusDesc + valueTrxId + valueStatus;
    } else {
      print("Connection Failed!");
      print("Status Code Failed: $statusCode");
      var result = json.decode(response.body);
      var failed1 = result['Status']['statusCode'];
      var failed2 = result['Status']['statusDesc'];
      valueFailed = failed1.toString();
      valueFailed2 = failed2.toString();
      return valueFailed + valueFailed2;
      // throw new Exception("Error while fetching data");
    }
  }

  _showdialogSendImage(String msg) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: new AlertDialog(
          title: Text("Warning"),
          content: new Text(
            msg,
            style: new TextStyle(fontSize: 16.0),
          ),
          actions: <Widget>[
            new FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: new Text("OK"))
          ],
        ));
  }
}
