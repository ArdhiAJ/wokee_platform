import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:wokee_platform/service/register/request_OTP_Service.dart';
import 'package:wokee_platform/view/model/color_loader_2.dart';
import 'package:wokee_platform/view/register/register_otp.dart';
import 'package:wokee_platform/view/model/model.dart';

class RequestOTPPage extends StatefulWidget {
  @override
  _RequestOTPPageState createState() => _RequestOTPPageState();
}

class _RequestOTPPageState extends State<RequestOTPPage> {
  final formKey = GlobalKey<FormState>();

  void _submit() async {
    if (formKey.currentState.validate()) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return Center(
              child: ColorLoader2(),
            );
          });

      new Future.delayed(new Duration(seconds: 0), () async {
        //dismiss loader
        // await otpUserRequest();
        // _navigateToOTP();
        Navigator.push(context,
            new MaterialPageRoute(builder: (context) => new RegisterOTPPage()));
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      // backgroundColor: Colors.red,
      appBar: new PreferredSize(
        preferredSize: Size(double.infinity, 50),
        child: new ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25)),
          child: new AppBar(
            backgroundColor: Color(0xff007022),
            title: new Center(
              child: new Text(
                "Request OTP",
                style: TextStyle(color: Colors.white, fontSize: 25),
              ),
            ),
            actions: <Widget>[
              Padding(
                  padding: const EdgeInsets.all(8),
                  child: new Image.asset('assets/images/logo_wokee.png')),
            ],
          ),
        ),
      ),
      body: new Form(
        key: formKey,
        child: new ListView(
          padding: const EdgeInsets.fromLTRB(15, 25, 15, 0),
          children: <Widget>[
            new Text(
              "Please enter your email again",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
              textAlign: TextAlign.center,
            ),
            new SizedBox(
              height: 15,
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 5),
              child: new Text(
                "Email",
                style: TextStyle(fontSize: 16),
              ),
            ),
            new TextFormField(
              controller: txtControllerRequestOTPEmail,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                labelText: 'Please enter your email here',
                prefixIcon: Icon(Icons.email),
                contentPadding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30.0)),
              ),
              validator: validateEmail,
            ),
            new SizedBox(
              height: 20,
            ),
            new RaisedButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              color: Color(0xff007022),
              // onPressed: (){
              //   Navigator.push(
              //               context,
              //               new MaterialPageRoute(
              //                   builder: (context) =>
              //                       new RegisterOTPPage()));
              // },
              onPressed: _submit,
              // onPressed: () {
              //   Navigator.push(
              //       context,
              //       new MaterialPageRoute(
              //           builder: (context) => new RegisterOTPPage()));
              // },
              child: new Text(
                "Next",
                style: TextStyle(color: Colors.white, fontSize: 17, fontWeight: FontWeight.bold),
              ),
            ),
            new SizedBox(
              height: 15,
            ),
            new Text(
              "Please contact Halo Bukopin 14005\nif your mobile number is no longer active",
              style: TextStyle(color: Colors.grey),
              textAlign: TextAlign.center,
            )
          ],
        ),
      ),
    );
  }

  _navigateToOTP() async {
    await new Future.delayed(const Duration(seconds: 5));
    if (valueCode == '0') {
      print("Request OTP Success");
      Navigator.pop(context); //dismiss loader
      Fluttertoast.showToast(
          msg: "Request OTP Berhasil",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 1,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);
      Navigator.push(context,
          new MaterialPageRoute(builder: (context) => new RegisterOTPPage()));
    } if (valueCode == "22002") {
      print("Login Failed");
      Navigator.pop(context); //dismiss loader
      // _showdialog("SMS OTP belum dibuat");
      _showdialog("User Not Found");
    } 
    else {
      print("Request OTP Failed");
      Navigator.pop(context); //dismiss loader
      _showdialog("Message: $valueDesc");
    }
  }

  _showdialog(String msg) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: new AlertDialog(
          // title: Text("Pesan"),
          content: new Text(
            msg,
            style: new TextStyle(fontSize: 16.0),
          ),
          actions: <Widget>[
            new FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                  // txtControllerEmail.clear();
                  // txtControllerPassword.clear();
                },
                child: new Text("OK"))
          ],
        ));
  }
}
