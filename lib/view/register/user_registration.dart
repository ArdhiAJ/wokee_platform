import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:wokee_platform/service/register/user_registrationT24.dart';
import 'package:wokee_platform/view/login/login_page.dart';
import 'package:wokee_platform/view/model/color_loader_2.dart';

class UserRegistrationPage extends StatefulWidget {
  @override
  _UserRegistrationPageState createState() => _UserRegistrationPageState();
}

class _UserRegistrationPageState extends State<UserRegistrationPage> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      // backgroundColor: Colors.red,
      appBar: new PreferredSize(
        preferredSize: Size(double.infinity, 50),
        child: new ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25)),
          child: new AppBar(
            backgroundColor: Color(0xff007022),
            title: new Center(
              child: new Text(
                "User Registration",
                style: TextStyle(color: Colors.white, fontSize: 25),
              ),
            ),
            actions: <Widget>[
              Padding(
                  padding: const EdgeInsets.all(8),
                  child: new Image.asset('assets/images/logo_wokee.png')),
            ],
          ),
        ),
      ),
      body: new ListView(
        padding: const EdgeInsets.fromLTRB(15, 25, 15, 0),
        children: <Widget>[
          new Text(
            "Registration Successful",
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
          new SizedBox(
            height: 15,
          ),
          new Text(
            "Please press the finish button, then check your email\nto get the default Password and PIN",
            style: TextStyle(fontSize: 16),
            textAlign: TextAlign.center,
          ),
          new SizedBox(
            height: 15,
          ),
          new RaisedButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            color: Color(0xff007022),
            onPressed: () async {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return Center(
                      child: ColorLoader2(),
                    );
                  });

              new Future.delayed(new Duration(seconds: 0), () async {
                //dismis loader
                await userRegisT24();
                _navigateToLoginAfterReg();
                  // Navigator.push(context,
                  // new MaterialPageRoute(builder: (context) => new LoginPage()));
              });
            },
            // onPressed: () {
            //   Navigator.push(context,
            //       new MaterialPageRoute(builder: (context) => new LoginPage()));
            // },
            child: new Text(
              "Finish",
              style: TextStyle(color: Colors.white, fontSize: 17, fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }

  _navigateToLoginAfterReg() async {
    await new Future.delayed(const Duration(seconds: 5));
    var route = new MaterialPageRoute(
        builder: (BuildContext context) => new LoginPage());
    if (valueMessage == 'Transaction Success') {
      print("User Register T24 Success!");
      Navigator.pop(context); //dismiss loader
      Fluttertoast.showToast(
          msg: "Registrasi User Successful",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 1,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);
      Navigator.of(context).pushReplacement(route);
    } else {
      print("User Register T24 Failed!");
      Navigator.pop(context); //dismiss loader
      _showdialog("Status: $valueMessage");
    }
  }

  _showdialog(String msg) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: new AlertDialog(
          // title: Text("Pesan"),
          content: new Text(
            msg,
            style: new TextStyle(fontSize: 16.0),
          ),
          actions: <Widget>[
            new FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: new Text("OK"))
          ],
        ));
  }
}
