import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:wokee_platform/view/model/color_loader_2.dart';
import 'package:wokee_platform/view/model/model.dart';
import 'package:wokee_platform/service/register/register_service.dart';
import 'package:wokee_platform/view/register/register_otp.dart';

class RegisterPage extends StatefulWidget {
  RegisterPage({Key key}) : super(key: key);
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final FocusNode _firstInput = new FocusNode();
  final FocusNode _secondInput = new FocusNode();
  final FocusNode _thirdInput = new FocusNode();
  final FocusNode _fourInput = new FocusNode();
  final formKey = GlobalKey<FormState>();
  bool _isDisableKodeNegara = false;

  void _submit() async {
    if(formKey.currentState.validate()){
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return Center(
              child: ColorLoader2(),
            );
          });

      new Future.delayed(new Duration(seconds: 0), () async {
        await registerUser();
        _navigateToOtp();
        // Navigator.push(context,
        //     new MaterialPageRoute(builder: (context) => new RegisterOTPPage()));
      });
    }
  }

  @override
  void initState() {
    super.initState();
    txtControllerCodeCountry.text = "+62";
    // setState(() {
    //   txtControllerNamaDepan.clear();
    //   txtControllerNamaBelakang.clear();
    //   txtControllerEmail.clear();
    //   txtControllerMobileNumber.clear();
    // });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      // backgroundColor: Colors.red,
      appBar: new PreferredSize(
        preferredSize: Size(double.infinity, 50),
        child: new ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25)),
          child: new AppBar(
            backgroundColor: Color(0xff007022),
            title: new Center(
              child: new Text(
                "Account Registration",
                style: TextStyle(color: Colors.white, fontSize: 25),
              ),
            ),
            actions: <Widget>[
              Padding(
                  padding: const EdgeInsets.all(8),
                  child: new Image.asset('assets/images/logo_wokee.png')),
            ],
          ),
        ),
      ),
      body: new Form(
        key: formKey,
        child: new ListView(
          padding: const EdgeInsets.fromLTRB(15, 15, 15, 0),
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 5),
              child: new Text(
                "First Name",
                style: TextStyle(fontSize: 16),
              ),
            ),
            new TextFormField(
              controller: txtControllerNamaDepan,
              keyboardType: TextInputType.text,
              focusNode: _firstInput,
              decoration: InputDecoration(
                labelText: 'Enter your first name here',
                prefixIcon: Icon(Icons.text_fields),
                contentPadding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30.0)),
              ),
              onEditingComplete: () => //perubahan terbaru
                  FocusScope.of(context).requestFocus(_secondInput),
              validator: validateFirstName,
            ),
            new SizedBox(
              height: 15,
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 5),
              child: new Text(
                "Last Name",
                style: TextStyle(fontSize: 16),
              ),
            ),
            new TextFormField(
              controller: txtControllerNamaBelakang,
              keyboardType: TextInputType.text,
              focusNode: _secondInput,
              decoration: InputDecoration(
                labelText: 'Enter your last name here',
                prefixIcon: Icon(Icons.text_fields),
                contentPadding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30.0)),
              ),
              onEditingComplete: () => //perubahan terbaru
                  FocusScope.of(context).requestFocus(_thirdInput),
              validator: validateLastName,
            ),
            new SizedBox(
              height: 15,
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 5),
              child: new Text(
                "Email",
                style: TextStyle(fontSize: 16),
              ),
            ),
            new TextFormField(
              controller: txtControllerEmail,
              keyboardType: TextInputType.text,
              focusNode: _thirdInput,
              decoration: InputDecoration(
                labelText: 'Enter your email here',
                prefixIcon: Icon(Icons.email),
                contentPadding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30.0)),
              ),
              onEditingComplete: () => //perubahan terbaru
                  FocusScope.of(context).requestFocus(_fourInput),
              validator: validateEmail,
            ),
            new SizedBox(
              height: 15,
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 5),
              child: new Text(
                "Mobile Number",
                style: TextStyle(fontSize: 16),
              ),
            ),

            Row(
              children: <Widget>[
                new Flexible(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 0, right: 50),
                    child: new TextFormField(
                      controller: txtControllerCodeCountry,
                      enabled: _isDisableKodeNegara,
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                        prefixIcon: Icon(Icons.phone),
                        // prefixIcon: Icon(Icons.flag),
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 19.0),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30.0)),
                      ),
                    ),
                  ),
                ),
                new Flexible(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 0, right: 0),
                    child: new TextFormField(
                      controller: txtControllerMobileNumber,
                      // enabled: _isDisableKodeNegara,
                      focusNode: _fourInput,
                      keyboardType: TextInputType.phone,
                      inputFormatters: <TextInputFormatter>[
                        WhitelistingTextInputFormatter.digitsOnly,
                      ],
                      decoration: InputDecoration(
                        hintText: "8**********",
                        // prefixIcon: Icon(Icons.phone),
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 19.0),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30.0)),
                      ),
                      validator: validateMobileNumber,
                    ),
                  ),
                ),
              ],
            ),

            // new TextFormField(
            //   controller: txtControllerNomorHandphone,
            //   keyboardType: TextInputType.phone,
            //   focusNode: _fourInput,
            //   decoration: InputDecoration(
            //     labelText: 'Masukkan Nomor HP Kamu Disini',
            //     prefixIcon: Icon(Icons.email),
            //     contentPadding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
            //     border: OutlineInputBorder(
            //         borderRadius: BorderRadius.circular(30.0)),
            //   ),
            //   validator: validateMobileNumber,
            // ),
            new SizedBox(
              height: 15,
            ),
            new RaisedButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              color: Color(0xff007022),
              // onPressed: (){
              //   Navigator.push(
              //               context,
              //               new MaterialPageRoute(
              //                   builder: (context) =>
              //                       new RegisterOTPPage()));
              // },
              onPressed: _submit,
              // onPressed: () {
              //   Navigator.push(
              //       context,
              //       new MaterialPageRoute(
              //           builder: (context) => new RegisterOTPPage()));
              // },
              child: new Text(
                "Next",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 17,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _showdialog(String msg) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: new AlertDialog(
          // title: Text("Pesan"),
          content: new Text(
            msg,
            style: new TextStyle(fontSize: 16.0),
          ),
          actions: <Widget>[
            new FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                  txtControllerEmail.clear();
                  txtControllerMobileNumber.clear();
                  // txtControllerNomorHandphone.clear();
                  // txtControllerPassword.clear();
                },
                child: new Text("OK"))
          ],
        ));
  }

  _navigateToOtp() async {
    print(
        "Mobile Number: ${txtControllerCodeCountry.text + txtControllerMobileNumber.text}");
    await new Future.delayed(const Duration(seconds: 3));
    var route = new MaterialPageRoute(
      builder: (BuildContext context) => new RegisterOTPPage(),
    );
    if (valueCode == '0') {
      print("Register Success");
      Navigator.pop(context); //dismiss loader
      Fluttertoast.showToast(
          msg: "Registrasi Successful",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 1,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);
      Navigator.of(context).push(route);
    } else if (valueCode == "1") {
      print("Register Failed");
      Navigator.pop(context); //dismiss loader
      // _showdialog("Nama pengguna atau nomor ponsel sudah terdaftar");
      _showdialog("Your username or mobile number is already registered");
    } else {
      Navigator.pop(context); //dismiss loader
      _showdialog("Status: $valueMessage");
    }
  }
}
