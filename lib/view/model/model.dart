import 'package:flutter/material.dart';
import 'package:date_format/date_format.dart';

final txtControllerNamaDepan = new TextEditingController();
final txtControllerNamaBelakang = new TextEditingController();
final txtControllerEmail = new TextEditingController();
final txtControllerNomorHandphone = new TextEditingController();

final txtControllerOTP = new TextEditingController();

final txtControllerRequestOTPEmail = new TextEditingController();

final txtControllerPassword = new TextEditingController();
final txtControllerNewPassword = new TextEditingController();
final txtControllerOldPassword = new TextEditingController();
final txtControllerConfirmNewPassword = new TextEditingController();
final txtControllerOldPin = new TextEditingController();
final txtControllerNewPin = new TextEditingController();
final txtControllerConfirmNewPin = new TextEditingController();
final txtControllerUserNameForgotPass = new TextEditingController();
final txtControlleruserPasswordForgotPass = new TextEditingController();
final txtControllerConfirmUserPasswordForgotPass = new TextEditingController();
final txtControllerCodeCountry = new TextEditingController();
final txtControllerMobileNumber = new TextEditingController();

final txtControllerMaskingEmail = new TextEditingController();

final txtControllerEktp = new TextEditingController();
final txtControllerMotherName = new TextEditingController();
final txtControllerKkNumber = new TextEditingController();

final txtControllerCompanyName = new TextEditingController();
final txtControllerCompanyAddress = new TextEditingController();
final txtControllerBeneficialOwnerName = new TextEditingController();
final txtControllerBeneficialPhoneNumber = new TextEditingController();
final txtControllerCompanyDistrict = new TextEditingController();

final txtControllerOccupation = new TextEditingController();
final txtControllerJobPosition = new TextEditingController();

final txtControllerProduct = new TextEditingController();
final txtControllerSubAccountNickname = new TextEditingController();

final txtControllerEktpEmail = new TextEditingController();
final txtControllerEktpAgreement = new TextEditingController();
final txtControllerEktpMaidenName = new TextEditingController();
final txtControllerEktpMobileNumber = new TextEditingController();
final txtControllerEktpAccountNickname = new TextEditingController();
final txtControllerEktpGender = new TextEditingController();
final txtControllerEktpPlaceOfBirth = new TextEditingController();
final txtControllerEktpDob = new TextEditingController();
final txtControllerEktpMaritalStatus = new TextEditingController();
final txtControllerEktpNoKK = new TextEditingController();
final txtControllerEktpAddress = new TextEditingController();
final txtControllerEktpNoRW = new TextEditingController();
final txtControllerEktpNoRT = new TextEditingController();
final txtControllerEktpMunicipal = new TextEditingController();
final txtControllerEktpDistrict = new TextEditingController();
final txtControllerEktpCity = new TextEditingController();
final txtControllerEktpBeneficialOwnerName = new TextEditingController();
final txtControllerEktpBeneficialPhoneNumber = new TextEditingController();
final txtControllerEktpSubAccountNickname = new TextEditingController();

final txtControllerPhoneCif = new TextEditingController();

final txtControllerNumberOfForeignTax = new TextEditingController();
final txtControllerNumberOfSocialSecurity = new TextEditingController();

final txtControllerAddressEC = new TextEditingController();
final txtControllerNoRwEC = new TextEditingController();
final txtControllerNoRtEc = new TextEditingController();
final txtControllerMunicipalEC = new TextEditingController();
final txtControllerDistrictEC = new TextEditingController();
final txtControllerCityEC = new TextEditingController();
final txtControllerProvinceEC = new TextEditingController();
final txtControllerZipCodeEC = new TextEditingController();

String userPasswordForgotPass;
String confirmUserPasswordForgotPass;

String tempUserName = "";
String newPassword;
String confirmNewPassword;
String newPIN;
String confirmNewPin;
String wac = "Y";
String wac2 = "N";

var date = formatDate(DateTime.now(), [yyyy, '-', mm, '-', dd]);
var time = formatDate(DateTime.now(), [HH, ':', nn, ':']);
var merge = "${date}T${time}00Z";
var tempTimestamp = merge;

var dropdownSelectedItemCP;
// var dropdownSelectedItemPOAO;
// var dropdownSelectedItemROF;
// var dropdownSelectedItemROE;
// var dropdownSelectedItemSOF;

// var dropdownSelectedItemBT;

// var dropdownSelectedItemCP2;
// var dropdownSelectedItemPOAO2;
// var dropdownSelectedItemROF2;
// var dropdownSelectedItemROE2;
// var dropdownSelectedItemSOF2;

String validateFirstName(String value) {
  final pattern = RegExp(r'^[a-zA]+$');
  if (value.isEmpty) {
    return "First Name Can't be Empty";
  } else if (!(pattern.hasMatch(value))) {
    return "First Name Can't Contain Special Character and Number";
  } else {
    return null;
  }
}

String validateLastName(String value) {
  final pattern = RegExp(r'^[a-zA]+$');
  if (value.isEmpty) {
    return "Last Name Can't be Empty";
  } else if (!(pattern.hasMatch(value))) {
    return "Last Name Can't Contain Special Character and Number";
  } else {
    return null;
  }
}

String validateMobileNumber(String value) {
  // String patttern = r'(^(?:[+]628)?[0-9]{10}$)';
  // String patttern = r'(^(?:[+]628)?[0-9]{9}$)';
  // RegExp regExp = new RegExp(patttern);
  // if (value.length == 0) {
  if (value.isEmpty) {
    return "Mobile Number Can't be Empty";
  }
  // else if (!regExp.hasMatch(value)) {
  // return 'Bukan Nomor Handphone Yang Valid';
  // }
  else {
    return null;
  }
}

String validateEmail(String value) {
  String pattern =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regExp = new RegExp(pattern);
  // if (value.length == 0) {
  if (value.isEmpty) {
    return "Email Can't be Empty";
  } else if (!regExp.hasMatch(value)) {
    return 'Not a Valid Email';
  } else {
    return null;
  }
}

String validatePassword(String value) {
  if (value.isEmpty) {
    return "Password Can't be Empty";
  } else if (value.length < 6) {
    return "Password too little";
  } else {
    return null;
  }
}

String validateOTP(String value) {
  String patttern = r'(^(?:[])?[0-9]{6}$)';
  RegExp regExp = new RegExp(patttern);
  if (value.length == 0) {
    return "OTP Code Can't be Empty";
  } else if (!regExp.hasMatch(value)) {
    return 'Not a Valid OTP Code';
  }
  return null;
}

String validatePIN(String value) {
  String patttern = r'(^(?:[])?[0-9]{6}$)';
  RegExp regExp = new RegExp(patttern);
  if (value.length == 0) {
    return "PIN Code Can't be Empty";
  } else if (value.length < 6) {
    return "PIN too little";
  } else if (!regExp.hasMatch(value)) {
    return 'Not a Valid PIN Code';
  }
  return null;
}

String validateNIK(String value) {
  if (value.isEmpty) {
    return "Your e-KTP Number Can't be Empty";
  }
  // else if (value.length < 6) {
  //   return "Password too little";
  // }
  else {
    return null;
  }
}

String validateKK(String value) {
  if (value.isEmpty) {
    return "Your KK Number Can't be Empty";
  }
  // else if (value.length < 6) {
  //   return "Password too little";
  // }
  else {
    return null;
  }
}

String validateMotherName(String value) {
  if (value.isEmpty) {
    return "Your Mother's Maiden Can't be Empty";
  }
  // else if (value.length < 6) {
  //   return "Password too little";
  // }
  else {
    return null;
  }
}

String validateAll(String value) {
  if (value.isEmpty) {
    return "Can't be Empty";
  }
  // else if (value.length < 6) {
  //   return "Password too little";
  // }
  else {
    return null;
  }
}

class Ektp {
  final String nomorKK;
  final String nomorEktp;
  final String namaLengkap;
  final String jenisKelamin;
  final String tempatLahir;
  final String tanggalLahir;
  final String alamat;
  final String nomorRt;
  final String nomorRw;
  final String kota;
  final String provinsi;
  final String kecamatan;
  final String kelurahan;
  final String agama;
  final String statusPernikahan;
  final String pekerjaan;
  final String namaIbu;
  final String pendidikan;
  final String nomorProvinsi;
  final String nomorKabupaten;

  Ektp(
      this.nomorKK,
      this.nomorEktp,
      this.namaLengkap,
      this.jenisKelamin,
      this.tempatLahir,
      this.tanggalLahir,
      this.alamat,
      this.nomorRt,
      this.nomorRw,
      this.kota,
      this.provinsi,
      this.kecamatan,
      this.kelurahan,
      this.agama,
      this.statusPernikahan,
      this.pekerjaan,
      this.namaIbu,
      this.pendidikan,
      this.nomorKabupaten,
      this.nomorProvinsi);
}

class EktpAboutYou {
  final String nomorKK;
  final String nomorEktp;
  final String namaLengkap;
  final String jenisKelamin;
  final String tempatLahir;
  final String tanggalLahir;
  final String alamat;
  final String nomorRt;
  final String nomorRw;
  final String kota;
  final String provinsi;
  final String kecamatan;
  final String kelurahan;
  final String agama;
  final String statusPernikahan;
  final String pekerjaan;
  final String namaIbu;
  final String pendidikan;
  final String nomorProvinsi;
  final String nomorKabupaten;

  EktpAboutYou(
      this.nomorKK,
      this.nomorEktp,
      this.namaLengkap,
      this.jenisKelamin,
      this.tempatLahir,
      this.tanggalLahir,
      this.alamat,
      this.nomorRt,
      this.nomorRw,
      this.kota,
      this.provinsi,
      this.kecamatan,
      this.kelurahan,
      this.agama,
      this.statusPernikahan,
      this.pekerjaan,
      this.namaIbu,
      this.pendidikan,
      this.nomorProvinsi,
      this.nomorKabupaten);
}

class EktpProduct {
  final String nomorKK;
  final String nomorEktp;
  final String namaLengkap;
  final String jenisKelamin;
  final String tempatLahir;
  final String tanggalLahir;
  final String alamat;
  final String nomorRt;
  final String nomorRw;
  final String kota;
  final String provinsi;
  final String kecamatan;
  final String kelurahan;
  final String agama;
  final String statusPernikahan;
  final String pekerjaan;
  final String namaIbu;
  final String pendidikan;
  final String nomorProvinsi;
  final String nomorKabupaten;

  EktpProduct(
      this.nomorKK,
      this.nomorEktp,
      this.namaLengkap,
      this.jenisKelamin,
      this.tempatLahir,
      this.tanggalLahir,
      this.alamat,
      this.nomorRt,
      this.nomorRw,
      this.kota,
      this.provinsi,
      this.kecamatan,
      this.kelurahan,
      this.agama,
      this.statusPernikahan,
      this.pekerjaan,
      this.namaIbu,
      this.pendidikan,
      this.nomorProvinsi,
      this.nomorKabupaten);
}

class EktpFinalOnboading {
  final String nomorKK;
  final String nomorEktp;
  final String namaLengkap;
  final String jenisKelamin;
  final String tempatLahir;
  final String tanggalLahir;
  final String alamat;
  final String nomorRt;
  final String nomorRw;
  final String kota;
  final String provinsi;
  final String kecamatan;
  final String kelurahan;
  final String agama;
  final String statusPernikahan;
  final String pekerjaan;
  final String namaIbu;
  final String pendidikan;
  final String nomorProvinsi;
  final String nomorKabupaten;

  EktpFinalOnboading(
      this.nomorKK,
      this.nomorEktp,
      this.namaLengkap,
      this.jenisKelamin,
      this.tempatLahir,
      this.tanggalLahir,
      this.alamat,
      this.nomorRt,
      this.nomorRw,
      this.kota,
      this.provinsi,
      this.kecamatan,
      this.kelurahan,
      this.agama,
      this.statusPernikahan,
      this.pekerjaan,
      this.namaIbu,
      this.pendidikan,
      this.nomorProvinsi,
      this.nomorKabupaten);
}

class EktpAgreement {
  final String nomorKK;
  final String nomorEktp;
  final String namaLengkap;
  final String jenisKelamin;
  final String tempatLahir;
  final String tanggalLahir;
  final String alamat;
  final String nomorRt;
  final String nomorRw;
  final String kota;
  final String provinsi;
  final String kecamatan;
  final String kelurahan;
  final String agama;
  final String statusPernikahan;
  final String pekerjaan;
  final String namaIbu;
  final String pendidikan;
  final String nomorProvinsi;
  final String nomorKabupaten;

  EktpAgreement(
      this.nomorKK,
      this.nomorEktp,
      this.namaLengkap,
      this.jenisKelamin,
      this.tempatLahir,
      this.tanggalLahir,
      this.alamat,
      this.nomorRt,
      this.nomorRw,
      this.kota,
      this.provinsi,
      this.kecamatan,
      this.kelurahan,
      this.agama,
      this.statusPernikahan,
      this.pekerjaan,
      this.namaIbu,
      this.pendidikan,
      this.nomorProvinsi,
      this.nomorKabupaten);
}

class CustomerIdHomeOnBoarding {
  final String customerId;

  CustomerIdHomeOnBoarding(this.customerId);
}

class CustomerIdOnBoardingPage {
  final String customerId;

  CustomerIdOnBoardingPage(this.customerId);
}

class CustomerIdAboutYou {
  final String customerId;

  CustomerIdAboutYou(this.customerId);
}

class CustomerIdProduct {
  final String customerId;

  CustomerIdProduct(this.customerId);
}

class CustomerIdYourIdentity {
  final String customerId;

  CustomerIdYourIdentity(this.customerId);
}

class CustomerIdFinalOnboarding {
  final String customerId;

  CustomerIdFinalOnboarding(this.customerId);
}

class SavingAccount {
  final String savingAc;
  final String lastTimeLogin;

  SavingAccount(this.savingAc, this.lastTimeLogin);
}

class SavingAccountHomepage {
  final String savingAc;
  final String lastTimeLogin;

  SavingAccountHomepage(this.savingAc, this.lastTimeLogin);
}

Pao selectedPAO;
List<Pao> purposeAccountOpening = <Pao>[
  const Pao(1, 'INVESTASI'),
  const Pao(2, 'OPERASIONAL'),
  const Pao(3, 'GAJI'),
  const Pao(4, 'LAIN-LAIN')
];

class Pao {
  final String name;
  final int id;

  const Pao(this.id, this.name);
}

Rof selectedROF;
List<Rof> rangeOfFund = <Rof>[
  const Rof(1, '< 1 JT'),
  const Rof(2, '1 JT s/d < 5 JT'),
  const Rof(3, '5 JT s/d < 10 JT'),
  const Rof(4, '10 JT s/d < 25 JT'),
  const Rof(5, '25 JT s/d < 50 JT'),
  const Rof(6, '50 JT s/d < 100 JT'),
  const Rof(7, '100 JT ke atas')
];

class Rof {
  final String name;
  final int id;

  const Rof(this.id, this.name);
}

Roe selectedROE;
List<Roe> rangeOfExpense = <Roe>[
  const Roe(1, '< 1 JT'),
  const Roe(2, '1 JT s/d < 5 JT'),
  const Roe(3, '5 JT s/d < 10 JT'),
  const Roe(4, '10 JT s/d < 25 JT'),
  const Roe(5, '25 JT s/d < 50 JT'),
  const Roe(6, '50 JT s/d < 100 JT'),
  const Roe(7, '100 JT ke atas')
];

class Roe {
  final String name;
  final int id;

  const Roe(this.id, this.name);
}

Sof selectedSOF;
List<Sof> sourceOfFund = <Sof>[
  const Sof(1, 'Usaha Sampingan (per bulan)'),
  const Sof(2, 'Omzet Usaha (per bulan)'),
  const Sof(3, 'Gaji (per bulan)'),
  const Sof(4, 'Warisan/Hibah/Hadiah (per bulan)'),
  const Sof(5, 'Usaha Group Perusahaan (per bulan)'),
  const Sof(6, 'Usaha Anak Perusahaan (per bulan)'),
  const Sof(7, 'Penjualan Aset'),
  const Sof(9, 'Lainnya')
];

class Sof {
  final String name;
  final int id;

  const Sof(this.id, this.name);
}

Bt selectedBT;
List<Bt> bussinesType = <Bt>[
  const Bt(1, 'Bidang Usaha Pertambangan'),
  const Bt(2, 'Industri Eletronika'),
  const Bt(3, 'Industri makanan/minuman'),
  const Bt(4, 'Industri Rokok'),
  const Bt(5, 'Jasa konstruksi'),
  const Bt(6, 'Banking'),
  const Bt(7, 'Conversion'),
  const Bt(8, 'Education'),
  const Bt(9, 'Entertainment'),
  const Bt(10, 'Dana pensiun & Pembiayaan / Finance'),
  const Bt(11, 'Government '),
  const Bt(12, 'Hospital '),
  const Bt(13, 'Hotel'),
  const Bt(14, 'Industry'),
  const Bt(15, 'Insurance'),
  const Bt(16, 'Jasa'),
  const Bt(17, 'Manufacturing'),
  const Bt(18, 'Tourism'),
  const Bt(19, 'Agen perjalanan / trading'),
  const Bt(20, 'Transportation'),
  const Bt(21, 'Ternak Ayam '),
  const Bt(22, 'Money Changer'),
  const Bt(23, 'Jasa Pengiriman uang'),
  const Bt(24, 'Akuntan, Pengacara, Notaris'),
  const Bt(25, 'Surveyor & Agen Real Estate'),
  const Bt(26, 'Pedagang Logam Mulia'),
  const Bt(27, 'Penjual Barang Antik & Mewah'),
  const Bt(28, 'Lembaga Swadaya masyarakat'),
  const Bt(29, 'Partai Politik'),
  const Bt(30, 'Perusahaan Asing'),
  const Bt(31, 'Kasino / Organisasi Permainan'),
  const Bt(32, 'Pelajar/ Mahasiswa'),
  const Bt(33, 'Ibu rumah tangga')
];

class Bt {
  final String name;
  final int id;

  const Bt(this.id, this.name);
}

Jt selectedJT;
List<Jt> jobType = <Jt>[
  const Jt(1, 'Pegawai Negeri/ PNS'),
  const Jt(2, 'TNI/ ABRI/ Militer'),
  const Jt(3, 'Pegawai Swasta/ Karyawan Swasta'),
  const Jt(4, 'Professional'),
  const Jt(5, 'Wiraswasta/ Wirausaha'),
  const Jt(6, 'Pensiun/ Pensiunan'),
  const Jt(7, 'Akuntan'),
  const Jt(8, 'Dokter'),
  const Jt(9, 'Engineer/ Teknisi'),
  const Jt(10, 'Guru/ Dosen'),
  const Jt(11, 'Ibu Rumah Tangga '),
  const Jt(12, 'Konsultan '),
  const Jt(13, 'Pegawai Koperasi'),
  const Jt(14, 'Mahasiswa/ Pelajar'),
  const Jt(15, 'Notaris'),
  const Jt(16, 'Pegawai BUMN/ BUMD'),
  const Jt(17, 'Pengacara'),
  const Jt(18, 'Pekerja Informal'),
  const Jt(19, 'Pekerja Seni'),
  const Jt(20, 'Pekerja Sosial/ LSM'),
  const Jt(21, 'Pejabat Negara'),
  const Jt(22, 'Polisi'),
  const Jt(23, 'Paramedis'),
  const Jt(24, 'Petani/ Nelayan'),
  const Jt(25, 'Wartawan'),
  const Jt(26, 'Pegawai Yayasan')
];

class Jt {
  final String name;
  final int id;

  const Jt(this.id, this.name);
}

Jp selectedJP;
List<Jp> jobPosition = <Jp>[
  const Jp(1, 'Non Staff'),
  const Jp(2, 'Staff'),
  const Jp(3, 'Senior Staff'),
  const Jp(4, 'Supervisor'),
  const Jp(5, 'Assistant Manager'),
  const Jp(6, 'Manager'),
  const Jp(7, 'Senior Manager'),
  const Jp(8, 'General Manager'),
  const Jp(9, 'Wakil Direktur'),
  const Jp(10, 'Direktur'),
  const Jp(11, 'Komisaris'),
  const Jp(12, 'Hakim Agung & Anggota Mahkamah Agung'),
  const Jp(13, 'Hakim Konstitusi & Komisi Yudisial'),
  const Jp(14, 'Auditor Pajak'),
  const Jp(15, 'Pengurus Partai Politik'),
  const Jp(16, 'Anggota Partai Politik'),
  const Jp(17, 'Menteri'),
  const Jp(18, 'Eselon 1 & 2'),
  const Jp(19, 'Eselon 3 - 5'),
  const Jp(20, 'Perwira Tinggi POLRI/TNI'),
  const Jp(21, 'Perwira Menengah POLRI/TNI'),
  const Jp(22, 'Perwira Pertama POLRI/TNI'),
  const Jp(23, 'Bintara Tinggi POLRI/TNI'),
  const Jp(24, 'Bintara POLRI/TNI'),
  const Jp(25, 'Tamtama Kepala POLRI/TNI'),
  const Jp(26, 'Tamtama POLRI/TNI'),
  const Jp(27, 'Pimpinan dan Anggota KPK'),
  const Jp(28, 'Duta Besar/Konsulat Jenderal'),
  const Jp(29, 'Lain - lain')
];

class Jp {
  final String name;
  final int id;

  const Jp(this.id, this.name);
}
